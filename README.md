# IOT - Sentinel
Sistema de monitorización web que permite gestionar datos generados por dispositivos de medición automáticos.

El sistema esta dividido en dos componentes principales:

- Una aplicación web (frontend) la cual permite a los usuarios gestionar todos los dispositivos y sus variables, como así tambien monitorizar todos los datos registrados por estos.  El frontend fué desarrollado con el framework ReactJS.

- Una API REST (backend), que permite por un lado la carga de datos sensados por dispositivos medición a través de peticiones HTTP los cuales se almacenan en una base de datos y por otro lado interactúa con la aplicación web que contiene la interfaz de usuario. El backend contiene un proyecto Django que utiliza el módulo Django Rest Framework para alojar la API REST.

 
Los entornos de desarrollo y de producción se encuentran completamente pre-configurados en un sistema de imagenes y contenedores virtuales denominado Docker. El motor de Docker es una tecnología open source de containerización para construir y containerizar aplicaciones. Docker es una tecnología que simplifica el proceso de gestionar los procesos de aplicaciones en contenedores. Los contenedores permiten ejecutar aplicaciones en procesos aislados de recursos. Se parece a las máquinas virtuales, sin embargo, los contenedores son más portátiles, tienen más recursos y son más independientes del sistema operativo host.

Una instancia de Docker permite crear y configurar de a un contenedor a la vez y resulta un poco engorroso manejar la comunicación entre dos o más contenedores. Para facilitar ésto se utiliza docker-compose que permite orquestar todo el mecanismo de configuración y comunicación entre los distinos contenedores de una manera muy sencilla en un unico archivo de configuración.

## Tecnologías y entornos

Existen dos entornos preconfigurados para la creación de los contenedores que alojan el sistema: desarrollo y producción.

- **Entorno de desarrollo:**
    - Backend:
        - Django / Django Rest Framework
    - Database: 
        - SqlLite 
    - Frontend 
        - React
        - Redux (Manejador de estados de la app) 
        - Antd (Diseño de las vistas ) 

</br>


- **Entorno de producción:**
    - Servidor web: 
        - Nginx    
    - Database: 
        - Postgresql         
    - Backend (Python 3.6):
        - Django / Django Rest Framework
    - Frontend  (NodeJS 10.15 y NPM)
        - React
        - Redux (Manejador de estados) 
        - Antd (Diseño de las vistas) 



# Guía de instalación  


**Requisitos**

1.  Instalar Docker(se utilizó la version 18.09.3). Guías:
    -   https://docs.docker.com/engine/install/ubuntu/
    -   https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es

</br>

2.  Instalar docker-compose (se utilizó version 1.25.4)
    -   https://docs.docker.com/compose/install/

</br>

3.  Abrir una consola y clonar el proyecto:   
`git clone https://gitlab.com/ceneha/sentinel-iot.git`   

</br>

4. Modificar usuarios propietarios y permisos al directorio del proyecto:   
`sudo chown -R $USER:$USER /directorio_proyecto`
`sudo chmod -R 755 /directorio_proyecto`

</br>

5. Agregar en el archivo /etc/environments las siguientes variables de entorno:   
`DOCKER_CLIENT_TIMEOUT=240`
`COMPOSE_HTTP_TIMEOUT=240`

</br></br>

## Configuración de entornos

**Aclaración paraa configurar los entornos de trabajo:**

Docker-compose crea 2 o 4 servicios dependiendo el entorno que se pretenda activar: 

**Entorno de  Desarrollo:**

-   Backend  (python + django)  
-   Frontend (node + reactjs) 


**Entorno de Producción:**

-   Backend  (python + django) 
-   Frontend (node + reactjs) 
-   Postgress 
-   Webserver (nginx)

 Docker-compose inicia los hilos de ejecución de los servicios mencionados en forma paralela; cómo algunos servicios dependen de otros puede que aparezcan errores hasta que los servicios requeridos terminan de activarse. Una vez que se encuentren completamente activos todos los servicios, será posible acceder al sistema.

 Esperar hasta que se terminen de  activar y verificar en la consola que no se hayan producido errores al intentar montar los contenedores. Debería verse algo asi:

![alt text](readmes/img/server_started_ok1.png "Ejemplo de salida de la consola tras iniciar la activación de los entornos")

Estos servicios permanecen siempre activos a excepción del frontend en producción que finaliza tras compilar el código de la aplicación y pasarselo a la aplicación del backend que luego lo sirve como archivos estáticos.

**Configuración del entorno de desarrollo** 

1.  Posicionarse en la carpeta .infraestructure-dev y ejecutar el script que inicia los contenedores:

    `cd .infraestructure-dev && ./start-app.sh`

2.  Verificar en la consola que no se hayan producido errores al intentar montar los contenedores.

3.  Abrir el navegador y comprobar que funcionen las direcciones:

    Backend: http://127.0.0.1:8000/admin/

    Frontend: http://127.0.0.1:3000/


4.  Abrir otra terminal y posicionarse en la carpeta _.infraestructure-dev_ , ejecutar el script que permite acceder al contenedor del backend y crear una nueva cuenta de usuario con permisos de administrador:

    `./open-terminal-backend.sh`

    
    `python manage.py createsuperuser`   
    

5.  Ir al navegador y acceder al sistema (backend o frontend) e iniciar sesión con el usuario creado.
            

**Configuración del entorno de producción** 
 

1.  Posicionarse en la carpeta .infraestructure-prod y ejecutar el script que inicia los contenedores:

    `cd .infraestructure-prod && ./start-app.sh`

2.  Verificar en la consola que no se hayan producido errores al intentar montar los contenedores.

3.  Abrir el navegador y comprobar que funcionen las direcciones:

    Backend: http://127.0.0.1:8000/admin/

    Frontend: http://127.0.0.1:1300/

4.  Abrir otra terminal y posicionarse en la carpeta _.infraestructure-prod_ , ejecutar el script que permite acceder al contenedor del backend y crear una nueva cuenta de usuario con permisos de administrador:

    `./open-terminal-backend.sh`

    
    `python manage.py createsuperuser`   
    

5.  Ir al navegador y acceder al sistema (backend o frontend) e iniciar sesión con el usuario creado.


# Comandos útiles

## Comandos de docker-compose:

-   Construir  las imágenes y activar los contenedores(servicios) del sistema:


    Desarrollo:   
    `cd .infraestructure-dev`

    `sudo docker-compose up --build`

    Producción:   
    `cd .infraestructure-prod`

    `sudo docker-compose up --build`    

    Esto es especialmente útil cuando:
    -   Se hayan modificado archivos de configuracion del proyecto.
    -   Con el entorno de producción activo se haya modificado el código del frontend y deba recompilarse la aplicación del frontend.


-   Activar los contenedores ya creados de la aplicación (ejecutar cuando no haya habido cambios en el código):

    Desarrollo:   
    `cd .infraestructure-dev`

    `sudo docker-compose up`

    Producción:   
    `cd .infraestructure-prod`

    `sudo docker-compose up`  

    Este comando es especialmente útil cuando por el motivo que fuese se hayan detenido los servicios de los contenedores y se requiera reactivarlos.


## Comandos de docker:

Los siguientes comandos son propios de docker y pueden resultar útiles para manipular los contenedores de la aplicación de forma directa.

-   Listar todos los procesos y estados (activos/inactivos) de los contenedores creados:

    `sudo docker ps -a`

-   Activar contenedor con imagen ya creada:
     
     `docker start nombre_contenedor`

    Ejemplo backend modo desarrollo:   
    `docker start app_backend_dev` 

-   Detener todos los contenedores que se encuentran activos:

    `sudo docker stop $(sudo docker ps -aq)`

-   Eliminar todas las imágenes y contenedores activos:

    `sudo docker rm -f $(sudo docker ps -aq)`

    `sudo docker rmi -f $(sudo docker images -aq)`

-   Acceder a un contenedor activo:   
    `docker exec  -i -t nombre-del-contenedor /bin/bash`

    Ejemplo:    
    `docker exec  -i -t app_backend_prod /bin/bash`

## Comandos útiles dentro del contenedor del Backend:

Antes de ejecutar cualquier comando propio de Python/django es necesario activar y acceder al contenedor del backend. 

Si el contenedor no se encuentra activo, se puede activar de dos formas:

1. Posicionarse en la carpeta _.infraestructure-dev_ o _.infraestructure-prod_ (dependiendo el entorno en el que se esté trabajandp) y ejecutar:   
    `./start-app.sh`

2. Activarlo mediante comando de docker:  
   `docker start nombre_contenedor`

   Ejemplo desarrollo:   
   `docker start app_backend_dev`

   Ejemplo produccion:   
   `docker start app_backend_prod`



Una vez que el contenedor se encuentra activo,  se puede acceder al mismo de dos formas:

1. Desde la consola posicionarse en la carpeta .infraestructure-dev o .infraestructure-prod (dependiendo el entorno al que se quiera acceder) y ejecutar: 

    `./open-terminal-backend.sh`

2. Si el contenedor se encuentra activo, se puede acceder directamente con el comando docker mencionado previamente (sino activarlo):  
  `docker exec -i -t nombre_del_contenedor /bin/bash`
   
Ejemplo para desarrollo:   
  `docker exec -i -t app_backend_dev /bin/bash`

Ejemplo para produccion:   
  `docker exec -i -t app_backend_prod /bin/bash`

**Comandos:**

Una vez dentro del contenedor:

-   Crear super usuario:  
  `python manage.py createsuperuser`
-   Activar servidor en modo desarrollo:     
 `python manage.py runserver`


## Comandos útiles para el contenedor del Frontend:
Antes de ejecutar cualquier comando propio de nodejs o npm es necesario activar y acceder al contenedor del frontend.

Si el contenedor no se encuentra activo, se puede activar de dos formas:

1. Posicionarse en la carpeta _.infraestructure-dev_ y ejecutar (solo sirve para desarrollo):   
    `./start-app.sh`

2. Activarlo mediante comando de docker:  
   `docker start nombre_contenedor`

   Ejemplo desarrollo:   
   `docker start app_frontend_dev`

   Ejemplo produccion:   
   `docker start app_frontend_prod`

Una vez que el contenedor se encuentra activo,  se puede acceder al mismo de dos formas:

1. Desde la consola posicionarse en la carpeta .infraestructure-dev (solo sirve para el servidor de desarrollo) y ejecutar: 

    `./open-terminal-frontend.sh`


2. Si el contenedor se encuentra activo, se puede acceder directamente con el comando docker mencionado previamente:  

    `docker exec -i -t nombre_del_contenedor /bin/bash`
   
Ejemplo para desarrollo:   
    `docker exec -i -t app_backend_dev /bin/bash`

Ejemplo para produccion:   
     `docker exec -i -t app_backend_prod /bin/bash`

**Comandos**

Una vez dentro del contenedor:

-   Instalar todas las librerias javascript necesarias para poder ejecutar la aplicación:    
`npm install`

-   Inicia un servidor en modo desarrollo para poder probar la aplicación:   
    `npm start`

-   Construir una version compilada y minificada de la aplicación para ser utilizada en producción:   
    `npm run build`

## Acceso al contenedor del servidor web (Nginx):
En caso de que se requiera modificar la configuración del servidor web es necesario que el mismo se encuentre activo. 

Si el contenedor no se encuentra activo, se puede activar de dos formas:

1. Posicionarse en la carpeta _.infraestructure-prod_ y ejecutar (solo sirve para producción y activa todos los contenedores):   
    `./start-app.sh`

2. Activarlo mediante comando de docker:  
   `docker start nombre_contenedor`

   Ejemplo produccion:   
   `docker start app_nginx_prod`

Una vez que el contenedor se encuentra activo,  se puede acceder al mismo de dos formas:

1. Desde la consola posicionarse en la carpeta .infraestructure-prod (solo sirve para el servidor de desarrollo) y ejecutar: 

    `./open-terminal-nginx.sh`

2. Si el contenedor se encuentra activo, se puede acceder directamente con el comando docker mencionado previamente:  

    `docker exec -i -t nombre_del_contenedor /bin/bash`
   
   Ejemplo para produccion:   
     `docker exec -i -t app_nginx_prod /bin/bash`

Para modificar la configuración del servidor web:

   - Abrir el archivo: _nginx/nginx.conf_   
   - Modificarlo según se requiera.

## Acceso al contenedor del que maneja la base de datos (Postregsql):
En caso de que se requiera modificar la configuración de la base de datos de forma directa es necesario que el servicio que lo maneja se encuentre activo.

Si el servicio no se encuentra activo, se puede activar de dos formas:

1. Posicionarse en la carpeta _.infraestructure-prod_ y ejecutar (solo sirve para producción y activa todos los contenedores):   
    `./start-app.sh`

2. Activarlo mediante comando de docker:  
   `docker start nombre_contenedor`

   Ejemplo produccion:   
   `docker start app_postres_prod`


Una vez que el contenedor se encuentra activo,  se puede acceder al mismo de dos formas:

1. Desde la consola posicionarse en la carpeta .infraestructure-prod (solo sirve para el servidor de desarrollo) y ejecutar: 

    `./open-terminal-postgres.sh`


2. Si el contenedor se encuentra activo, se puede acceder directamente con el comando docker mencionado previamente:  

    `docker exec -i -t nombre_del_contenedor /bin/bash`
   
   Ejemplo para produccion:   
     `docker exec -i -t app_postrges_prod /bin/bash`


Una vez dentro del contenedor es posible manipular las bases de datos de forma directa accediendo a ellas como cualquier base de datos postgres.

 

# Guía de uso de la aplicación

## Documentación de la API (Falta completar..)

Se utilizó la librería drf-yasg para documentar la API de forma automática:

Enlaces:
    - Documentación via ReDoc: http://localhost:8000/redoc/
    - Documentación via Swagger: http://localhost:8000/swagger/

Referencias: 
    - https://www.django-rest-framework.org/topics/documenting-your-api/
    - https://github.com/axnsan12/drf-yasg/#code-generation



## Enviar petición a la API para registrar nuevos datos

Estos pasos permiten enviar una petición Http al sistema para el registro de nuevos datos. Estas peticiones pueden enviarse desde un gestor de peticiones http, un script, mediante linea de comandos o desde una petición a través de un dispositivo electrónico de medición programable como un arduino. 

1.  Crear Token:    
    - Acceder al panel de administración del backend:  
   producción:  http://127.0.0.1:1300/admin/  
   desarrollo: http://127.0.0.1:8000/admin/
    - Crear un nuevo Token para el usuario registrado y copiarlo.  
   Ejemplo: _fe5de20bc0b6aecacab77d0e40bd12401b17e4da_ 


2. Crear nuevo dispositivo y variable:
   -  Acceder al frontend:   
desarrollo: http://127.0.0.1:3000/    
producción: http://127.0.0.1:1300/      


   - Iniciar sesión con una cuenta de administrador.
   - Dar de alta un nuevo dispositivo.
   - Dar de alta una variable para para el dispositivo creado.
   - Copiar el ID de la variable (ID_VARIABLE, ej: 1)

3.  Enviar una petición http con los siguientes parámetros:   

    - URL: http://127.0.0.1:1300/api/datos/
    -  Headers:
        - Content-Type: application/json
        - Authorization: 'Token fe5de20bc0b6aecacab77d0e40bd12401b17e4da (token creado en el paso 1)' 
    -  Body:

           `[{
                "valor": "valor flotante registrado",  
                "timestamp": datetime con formato 'aaaa-MM-dd HH:mm:ss',
                "variable": 'id de la variable'
            }] `

        Ejemplo:  

            `[{
                "valor": "11.3",
                "timestamp": "2020-01-16 15:03:38",
                "variable": 1
            }] `

4.  Verificar desde la consola y desde la aplicación que el dato haya llegado correctamente.

## Script de simulación de envío de datos registrados por sensores a la API

Se creo un script que permite simular el envío de datos registrados a la api por sensores de un dispositivo. El script envía 50 peticiones para grabar datos registrados por 3 sensores con un delay de 3 segundos entre una petición y otra.

Tener en cuenta:

-  Las peticiones estan harcodeadas para grabar datos en las variables: 63, 67 y 68
-  Se envían solo 50 peticiones.

Para ejecutarlo (en modo desarrollo):

1. Posicionarse en la siguiente carpeta: `cd backend\.infraestructure-dev`

2. Iniciar aplicacion:  `./start-app.sh`

3. Ingresar al contenedor del backend: `./open-terminal-backend.sh`

4. Posicionarse en la siguiente carpeta: `cd api_scripts`

5. Ejecutar el script: `python post_data_multivar.py `

6. Posicionarse en la vista del dispositivo de las variables(45 en este caso) y visualizar como se actualiza el grafico en tiempo real.

   ![alt text](readmes/img/variables_real_time.png "Grafica de datos registrados por una variable en tiempo real")

# Configuración Telegram API Bot

Completar....

# Configuración inicio automático de docker-compose al iniciar sistema

Para configurar el inicio automático de los contenedores de docker-compose es necesario utilizar un gestor de servicios del sistema. Esto es especialmente en un entorno de producción donde se requiera que el sistema se encuentre activo de forma permanente. Los siguientes pasos están basados utilizando el gestor **systemd** (Ubuntu16.04):

1.  Abrir una consola y ejecutar:   
` cd /etc/systemd/system/ `

2.  Ejecutar en consola:   
` sudo touch docker-compose-iot.service `

3. Abrir el archivo anterior y copiar las siguientes líneas: 

**IMPORTANTE:** Reemplazar el parámetro _DIRECTORIO_DEL_PROYECTO_, por el directorio donde se encuentra el proyecto.

```
#/etc/systemd/system/docker-compose-iot.service > 

[Unit]
Description=Docker Compose Application Service
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/DIRECTORIO_DEL_PROYECTO
ExecStart=/usr/local/bin/docker-compose up -d
ExecStop=/usr/local/bin/docker-compose down
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
```

4. Habilitar el servicio para que se inicie automáticamente. Ejecutar en consola:   
`sudo systemctl enable docker-compose-iot`


5. Reiniciar la pc y verificar que los servicios se iniciaron automáticamente de forma correcta:   
`systemctl status docker-compose-iot.service`

   Salida correcta:

   ![alt text](readmes/img/check_automatic_up_services.png "Title Text")


6. Para obtener los logs en tiempo real de los contenedores, ejecutar en una terminal:   
`docker-compose logs -f -t`

-  Referencias:
    
    -  [How to run docker-compose up -d at system start up:](https://stackoverflow.com/questions/43671482/how-to-run-docker-compose-up-d-at-system-start-up)




# Mecanismo de trabajo con el repositorio

El repositorio de GitLab cuenta con dos ramas activas: `master` y `dev`. 

La rama dev es un espejo de la rama de desarrollo, por lo que cualquier nueva funcionalidad debe desarrolarse sobre la misma.

La rama master debería ser un espejo del código que se encuentra en producción. Cuando se decida desplegar el proyecto a producción, 

1. Commitear cualquier cambio en la rama de desarrollo:   
`git add -- .`

   `git commit -m "descripcion de commit"`


2. Moverse a la rama _master_:   
`git checkout master`

3.  mergear con _dev_:   
`git merge dev`

4. Pushear los cambios a master:   
`git push origin master`







## Referencias

- https://stackoverflow.com/questions/28610372/reactjs-with-django-real-usage#28646223
- https://hackernoon.com/creating-websites-using-react-and-django-rest-framework-b14c066087c7
- https://docs.google.com/presentation/d/1TRgDA-fImXTj88eeNW37iokt4ehuK8Q6Y-7hZnBqW4g/edit#slide=id.g1559307c6d_0_371

- https://help.pythonanywhere.com/pages/UsingMySQL
- Telegram Api Bot: https://www.guguweb.com/2019/12/09/automate-your-telegram-channel-with-a-django-telegram-bot/


