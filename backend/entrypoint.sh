#!/bin/sh


echo "Initializing postgres db..."

while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
  sleep 1
done

echo "postgres database has initialized successfully"
fi

exec "$@"