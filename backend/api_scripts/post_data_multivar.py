#Este script permite simular el envío de datos registrados por las variables 63, 67 y 68
#Se debe ejecutar desde dentro del contenedor del backend:
# cd api_scripts/ && python ./post_data_multivar.py


# Documentacion 
# https://docs.python-requests.org/en/master/user/quickstart/#post-a-multipart-encoded-file

import requests, json, random, time
from datetime import datetime, timedelta

# url = "http://127.0.0.1:8000/api/crear-datos-multisensores/"
url = "http://web3.unl.edu.ar:8000/api/crear-datos-multisensores/"
headers = {'Content-Type': 'application/json', 'Authorization': 'Token 68b8661a9be62d0505c840cd0cdb5dac9a9d4e28'}



v1 = round(random.uniform(1,50), 3)
v2 = round(random.uniform(20,80), 3)
v3 = round(random.uniform(60,70), 3)

for i in range(50):

    v1 += round(random.uniform(-3,3), 3)
    v2 += round(random.uniform(-4,4), 3)
    v3 += round(random.uniform(-5,5), 3)

    payload = {
        "timestamp": str(datetime.now()),
        "datos": [
            {
                "valor": str(round(v1,3)),
                "variable": 1
            },
            
            {
                "valor": str(round(v2,3)),
                "variable": 2
            },
            {
                "valor": str(round(v3,3)),
                "variable": 3
            }
        ]
    }

    r = requests.post(url, headers = headers, data = json.dumps(payload))

    time.sleep(1)

    data = r.json()
    print(v1,v2,v3)
    print(data)