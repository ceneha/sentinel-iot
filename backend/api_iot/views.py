
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt


from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.parsers import JSONParser
from api_iot.models import Dato
from rest_framework import serializers, permissions
from rest_framework.authentication import TokenAuthentication

from pprint import pprint
from rest_framework import viewsets, status
from rest_framework.response import Response
from .ApiAccessPolicy import ApiAccessPolicy
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import PageNumberPagination
from django.utils.timezone import now
from django.utils import timezone
from django.conf import settings
from django.template.loader import render_to_string
from datetime import datetime
from dateutil.parser import *
import pytz
from django.contrib.auth import get_user_model

# from django.contrib.auth.models import User
from rest_framework import generics
from .filters import DatoFilter
from .serializers import DispositivoSerializer, VariableSerializer, DatoSerializer, DispositivoVariableSerializer, UserSerializer, DatosMultisensoresSerializer, DatosVariablesDispositivoSerilizer
from .models import Dispositivo, Variable, Dato, Alerta

import telegram  # this is from python-telegram-bot package


utc = pytz.UTC

User = get_user_model()


# class UserList(generics.ListAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend]
    pagination_class = None
    ordering = ['username']


class DispositivoViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    permission_classes = (ApiAccessPolicy, )
    serializer_class = DispositivoSerializer
    queryset = Dispositivo.objects.all()
    filter_backends = [DjangoFilterBackend]

    # Helper property here to make get_queryset logic
    # more explicit
    @property
    def access_policy(self):
        return self.permission_classes[0]

    # Ensure that current user can only see the models
    # they are allowed to see
    # Si es administrador puede ver todos los dispositivos
    # Si no es administrador, solo puede ver los dispositivos para los que está habilitado
    def get_queryset(self):

        if getattr(self, "swagger_fake_view", False):
            return Dispositivo.objects.none()


        if self.request.user.is_staff:
            dispositivos = Dispositivo.objects.all()
        else:
            dispositivos = Dispositivo.objects.all().filter(
                users_enabled__id__icontains=self.request.user.id)

        return self.access_policy.dispositivos_scope_queryset(
            self.request, dispositivos)


class VariableViewSet(viewsets.ModelViewSet):
    permission_classes = (ApiAccessPolicy, )
    serializer_class = DispositivoVariableSerializer
    queryset = Variable.objects.all()
    filter_backends = [DjangoFilterBackend]

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Dispositivo.objects.none()

        return Variable.objects.filter(dispositivo=self.kwargs['dispositivo_pk'])

    @property
    def access_policy(self):
        return self.permission_classes[0]





class DatoViewSet(viewsets.ModelViewSet):
    permission_classes = (ApiAccessPolicy, )
    queryset = Dato.objects.all()
    serializer_class = DatoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = DatoFilter

    @property
    def access_policy(self):
        return self.permission_classes[0]


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, many=isinstance(request.data, list))

        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        # Actualizo la fecha de ultima actividad del dispositivo correspondiente (si es mayor a la actual)

        data = request.data[0]
        dispositivo = Variable.objects.get(id=data['variable']).dispositivo

        timestamp = parse(
            serializer.data[0]['timestamp'])

        if(dispositivo.ultima_actividad is None):
            dispositivo.ultima_actividad = data['timestamp']
        else:
            if(timestamp > dispositivo.ultima_actividad):
                dispositivo.ultima_actividad = data['timestamp']

        dispositivo.save()

        # Se setea id del dispositivo en el dato registrado
        request.data[0]['dispositivo'] = dispositivo.id

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    """
    Override list method and do the pagination only when page query param is provided. Otherwise return all.
    """

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        if 'page' in request.query_params:
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


    # def validate_alert(self):

    #     asd = 4

    # def post_event_on_telegram(data):

    #     message_html = 'Alerta: el dispositivo X , '

    #     telegram_settings = settings.TELEGRAM
    #     bot = telegram.Bot(token=telegram_settings['bot_token'])
    #     bot.send_message(chat_id="@%s" % telegram_settings['channel_name'],
    #                      text=message_html, parse_mode=telegram.ParseMode.HTML)


class AlertaViewSet(viewsets.ModelViewSet):
    permission_classes = (ApiAccessPolicy, )
    # serializer_class = DispositivoVariableSerializer
    queryset = Alerta.objects.all()
    filter_backends = [DjangoFilterBackend]

    def get_queryset(self):
        return Alerta.objects.filter(variable=self.kwargs['variable_pk'])

    @property
    def access_policy(self):
        return self.permission_classes[0]




@api_view(['POST'])
@permission_classes((ApiAccessPolicy,))
@csrf_exempt
def create_datos_multisensores(request):
    """
        Crea nuevos datos en base a la siguiente estructura recibida:
        Ejemplo estructura recibida: 
            data = {
                "timestamp": "2021-08-03 14:35:59",
                "datos": [ { "valor": "50.99", "variable": 63 }, { "valor": "52.8",   "variable": 63 } ]
                }
    """
    data = JSONParser().parse(request)

    #Utilizo DatosMultisensoresSerializer solo para validar que la estructura de datos recibida sea correcta
    serializer_tmp = DatosMultisensoresSerializer(data=data)
    if serializer_tmp.is_valid():

        # Reestructuro el formato del objeto json(diccionario) recibido
        # según el formato que admite DatoSerializer
        # Ejemplo estructura transformada para que la procese DatoSerializer
        # data = {
        #     "datos": [ {"timestamp": "2021-08-03 14:35:59", "valor": "50.99", "variable": 63 }, 
        #                 { "timestamp": "2021-08-03 14:35:59","valor": "52.8",   "variable": 63 } ]
        #     }

        for datoparcial in data['datos']:
            datoparcial.update({'timestamp': data['timestamp']})

        #Borro key timestamp del diccionario
        del data['timestamp']    

        #Paso el array de datos reestructurado a DatoSerializer
        serializer = DatoSerializer(data=data['datos'], many=isinstance(data['datos'], list))
    
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False, status=201)
        return JsonResponse(serializer.errors, safe=False, status=400)

    return JsonResponse(serializer_tmp.errors, safe=False, status=400)


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_datos_multisensores(request, id_dispositivo):
    """
        Ejempl estructura RETORNADA: 
            data = {[
                        {"variable": "63",
                        "nombre": "temperatura"
                        "datos": [ { "valor": "50.99", "timestamp": 2021-08-03 14:35:59 }, { "valor": "52.8",   "variable": 2021-08-03 14:36:59 } ]
                        },
                        {"variable": "15",
                        "nombre": "humedad"
                        "datos": [ { "valor": "10.5", "timestamp": 2021-08-03 14:38:59 }, { "valor": "10.8",   "variable": 2021-08-03 14:39:30  } ]
                        }
                    ]}
    """

    try:
        variables = Variable.objects.filter(dispositivo=id_dispositivo) # Todas las variables del dispositivo 
    except Dispositivo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND) 
        
    serialized = DatosVariablesDispositivoSerilizer(variables, many = True)

    return JsonResponse(serialized.data, status=status.HTTP_200_OK, safe=False)