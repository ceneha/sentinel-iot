from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
import datetime
from datetime import datetime, timedelta
from django.utils import timezone
from django.utils.timezone import now
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext as _
from rest_framework import serializers

ESTADO_CHOICES = (
    (True, 'Habilitado'),
    (False, 'No Habilitado')
)

VISIBILIDAD_CHOICES = (
    (False, 'Sólo administradores'),
    (True, 'Todos los usuarios'),
)


class Dispositivo(models.Model):

    etiqueta = models.CharField(
        max_length=50, unique=True, blank=False)
    descripcion = models.TextField(max_length=200)
    estado = models.BooleanField(
        choices=ESTADO_CHOICES, default=True, blank=False)
    visibilidad = models.BooleanField(
        choices=VISIBILIDAD_CHOICES, default=False, blank=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    ultima_actividad = models.DateTimeField(null=True, blank=True)
    latitud = models.FloatField(_('Latitude'), blank=True, null=True)
    longitud = models.FloatField(_('Longitude'), blank=True, null=True)
    users_enabled = models.ManyToManyField(
        get_user_model(), blank=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.etiqueta)
        # return "%s" % (self.etiqueta)

    class Meta:
        ordering = ['id']


# class Propiedad(models.Model):
#     nombre = models.CharField(max_length=25)
#     descripcion = models.TextField(max_length=200)
#     dispositivo = models.ForeignKey(Dispositivo, on_delete=models.CASCADE)

#     def __str__(self):
#         return self.nombre


class Variable(models.Model):
    nombre = models.CharField(max_length=25)
    etiqueta = models.CharField(
        max_length=50, unique=True)  # Ver si quitar o no
    descripcion = models.TextField(max_length=200)
    unidad = models.CharField(max_length=25)
    valor_min_permitido = models.DecimalField(
        max_digits=12, decimal_places=3, null=True, blank=True,)
    valor_max_permitido = models.DecimalField(
        max_digits=12, decimal_places=3, null=True, blank=True,)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    dispositivo = models.ForeignKey(
        Dispositivo, related_name='variables', on_delete=models.CASCADE)

    class Meta:
        unique_together = ['dispositivo', 'etiqueta']
        ordering = ['id']


    '''
    Retorna los ultimos 100 datos registrados en las ultimas 4 horas por la variable y ordenados por timestamp.
    No se muestran los datos registrados con un timestamp mayor al datetime actual
    '''
    def get_data_realtime(self):
        time_now = timezone.now()
        time_threshold = timezone.now() - timedelta(hours=1)
        return Dato.objects.filter(variable=self, timestamp__gt=time_threshold, timestamp__lt=time_now).order_by('-timestamp')[:150][::-1]

    # algooo se rompia si se agregan los __str__, por las dudas no descomentar
    # def __str__(self):
    #     return self.nombre

    # def __str__(self):
    #     return "%s - %s" % (self.id, self.nombre)


class Alerta(models.Model):
    variable = models.ForeignKey(
        Variable, related_name='alertas', on_delete=models.CASCADE)
    min_alerta = models.DecimalField(
        max_digits=12, decimal_places=3, blank=True, null=True)
    max_alerta = models.DecimalField(
        max_digits=12, decimal_places=3, blank=True, null=True)
    phone = PhoneNumberField(null=False, blank=False, unique=True)
    descripcion = models.TextField(max_length=200, blank=False)
    ultimo_envio = models.DateTimeField(blank=False, null=True)

    def __str__(self):
        return self.descripcion

# https://stackoverflow.com/questions/14666199/how-do-i-create-multiple-model-instances-with-django-rest-framework/40862505#40862505


class Dato(models.Model):
    valor = models.DecimalField(max_digits=12, decimal_places=3)
    timestamp = models.DateTimeField(null=False)
    timestamp_servidor = models.DateTimeField(null=False, default= timezone.now())
    variable = models.ForeignKey(
        Variable, related_name='data', on_delete=models.CASCADE)
    dispositivo = models.ForeignKey(
        Dispositivo, related_name='data', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        ordering = ['timestamp']

    """
    Se redefine el .save del modelo para autoguardar el dispositivo en función de la variable recibida.
    """

    def save(self, *args, **kwargs):
        if not self.dispositivo:
            self.dispositivo = self.variable.dispositivo
        super().save(*args, **kwargs)


    def __str__(self):
        # return "Disp(%s) - Var(%s) - Val(%s) " % (self.dispositivo.id, self.variable.id, self.valor)
        return "Id(%s) - Dispositivo(%s) - Variable(%s) - Valor(%s) - Timestamp (%s) - TimestampServidor (%s)" % (self.id, self.dispositivo.id, self.variable.id, self.valor, self.timestamp, self.timestamp_servidor)


# class Event(models.Model):
#     title = models.CharField(max_length=255)
#     description = models.TextField(blank=True)
#     start_date = models.DateTime(null=True)
