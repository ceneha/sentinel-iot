from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from rest_framework_nested import routers
from .views import DispositivoViewSet, VariableViewSet, DatoViewSet, UserViewSet  # UserList
from rest_framework.urlpatterns import format_suffix_patterns

from api_iot import views

# from rest_framework.schemas import openapi
# from rest_framework import permissions

# Nested Routing
router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'dispositivos', DispositivoViewSet)

dispositivos_router = routers.NestedSimpleRouter(
    router, r'dispositivos', lookup='dispositivo')
dispositivos_router.register(
    r'variables', VariableViewSet, basename='dispositivo-variables')


router.register('datos', DatoViewSet, basename='dato')
# router.register('datos-multi-var-model', DatosMultiVariableViewSet, base_name='datos-multi-var-model')



urlpatterns = [
    re_path(r'', include(router.urls)),
    re_path(r'', include(dispositivos_router.urls)),
    # path('datos-x-dispositivo/', views.get_datos_x_dispositivo_list),
    path('crear-datos-multisensores/', views.create_datos_multisensores),
    # path('get-datos-multisensores/dispositivo', views.get_datos_multisensores),
    path('get-datos-multisensores/dispositivo/<int:id_dispositivo>', views.get_datos_multisensores),
]
urlpatterns = format_suffix_patterns(urlpatterns,
                                     allowed=['json', 'csv'])


