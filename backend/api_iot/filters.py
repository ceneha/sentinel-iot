from rest_framework import generics
from django_filters import rest_framework as filters
from .models import Dato


# class DispositivoFilter(filters.FilterSet):

class DatoFilter(filters.FilterSet):
    start_date = filters.DateTimeFilter(
        field_name="timestamp", lookup_expr='gte')
    end_date = filters.DateTimeFilter(
        field_name="timestamp", lookup_expr='lte')

    class Meta:
        model = Dato
        fields = ['dispositivo','variable', 'start_date', 'end_date']
