from django.urls import include, path, reverse


from rest_framework import status
from django.test import TestCase, Client
from rest_framework.authtoken.models import Token

from rest_framework.test import APITestCase, URLPatternsTestCase, APIClient
from django.contrib.auth.models import User
from .models import Dispositivo, Variable, Dato
from .serializers import DispositivoSerializer
import json

from pprint import pprint
# https://dev.to/viktorvillalobos/testeando-drf-la-forma-correcta-40pf
# https://iheanyi.com/journal/user-registration-authentication-with-django-django-rest-framework-react-and-redux/
# https://www.django-rest-framework.org/api-guide/testing/#headers-authentication_1
# https://realpython.com/test-driven-development-of-a-django-restful-api/
# https://stackoverflow.com/questions/55391404/how-to-test-api-that-uses-django-rest-auth-token-authentication

# initialize the APIClient app
# user = User.objects.get(username='nboutet')


client = APIClient()


# client.force_authenticate(user=user)

# client.login(username='test', password='test')

# token = Token.objects.get(user__username='nahuboutet')
# client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)


# client.force_authenticate(user=user)


class DispositivoTests(APITestCase, URLPatternsTestCase):

    urlpatterns = [
        path('api/', include('api_iot.urls')),
        path('rest-auth/', include('rest_auth.urls')),
        path('rest-auth/registration/', include('rest_auth.registration.urls')),
    ]

    def setUp(self):
        # Create Admin user.
        user = User.objects.create_user(
            username='username',
            email='username@gmail.com',
            password='password123-',
            **{"is_superuser": True, "is_staff": True}
        )

        token, created = Token.objects.get_or_create(user=user)

        # client = APIClient(HTTP_AUTHORIZATION='Token ' +
        #                    token.key, **{'Authorization': token.key})

        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_create_dispositivo(self):
        """
        Caso de prueba 01: Ensure we can create a new dispositivo object.
        """
        url = reverse('dispositivo-list')
        data = {
            "etiqueta": "dev2",
            "descripcion": "descripcion",
            "estado": True,
            "visibilidad": False,
            "latitud": 5.3,
            "longitud": 2.4
        }

        response = client.post(url, data, format='json',
                               **{'Authorization': ''})

        expectedData = {'descripcion': 'descripcion',
                        'estado': True,
                        'etiqueta': 'dev2',
                        'fecha_creacion': response.data['fecha_creacion'],
                        'id': 1,
                        'latitud': 5.3,
                        'longitud': 2.4,
                        'ultima_actividad': None,
                        'url': 'http://testserver/api/dispositivos/1/',
                        'users_enabled': [],
                        'variables': [],
                        'visibilidad': False}

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_get_dispositivo(self):
        """
        Caso de prueba 02: Ensure we can get a dispositivo object.
        """

        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=20,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        url = reverse('dispositivo-detail', args={'pk': 1})
        url = "/api/dispositivos/20/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.get(url, format='json',
                              **{'Authorization': ''})
        response.render()
        # print("-----------response-------------------")

        # pprint(vars(response))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = {
            'id': 20,
            'etiqueta': 'DEV2',
            'descripcion': 'descrip',
            'estado': True,
            'visibilidad': False,
            'fecha_creacion': response.data['fecha_creacion'],
            'ultima_actividad': None,
            'latitud': 2,
            'longitud': 3,
            'users_enabled': [],
            'url': 'http://testserver/api/dispositivos/20/',
            'variables': []
        }

        # print("--------expectedData--------")
        # print(json.dumps(expectedData))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_get_list_dispositivos(self):
        """
        Caso de prueba 03: Ensure we can list all dispositivos object.
        """

        # Create Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            etiqueta="DEV1",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        # url = reverse('dispositivo-list', args={'pk': 1})
        url = reverse('dispositivo-list')

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.get(url, format='json',
                              **{'Authorization': ''})
        response.render()
        # print("-----------response-------------------")
        # pprint(vars(response.content))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = {'count': 1, 'next': None, 'previous': None,
                        'results': [{
                            'id': 1,
                            'etiqueta': 'DEV1',
                            'descripcion': 'descrip',
                            'estado': True,
                            'visibilidad': False,
                            'fecha_creacion': response.data['results'][0]['fecha_creacion'],
                            'ultima_actividad': None,
                            'latitud': 2.0,
                            'longitud': 3.0,
                            'users_enabled': [],
                            'url': 'http://testserver/api/dispositivos/1/',
                            'variables': []
                        }]
                        }

        # print("--------expectedData--------")
        # print(json.dumps(expectedData))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_delete_dispositivo(self):
        """
        Caso de prueba 04: Ensure we can delete a dispositivo object.
        """

        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=30,
            etiqueta="DEV",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        # url = reverse('dispositivo-detail', args={'pk': 1})
        url = "/api/dispositivos/30/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.delete(url, format='json',
                                 **{'Authorization': ''})
        response.render()
        # print("-----------response-------------------")
        # pprint(vars(response))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = None

        # print("--------expectedData--------")
        # print(json.dumps(expectedData))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_update_dispositivo(self):
        """
        Caso de prueba 05: Ensure we can update a dispositivo object.
        """

        id = 40
        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=id,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        # Modifico campos
        data = {
            "etiqueta": "dev_modif",
            "descripcion": "descrip_modif",
            "estado": False,
            "visibilidad": True,
            "latitud": 99,
            "longitud": 99
        }

        url = "/api/dispositivos/" + str(id) + "/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.put(url, data, format='json',
                              **{'Authorization': ''})
        response.render()
        # print("-----------response-------------------")

        # pprint(vars(response))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = {
            'id': id,
            'etiqueta': 'dev_modif',
            'descripcion': 'descrip_modif',
            'estado': False,
            'visibilidad': True,
            'fecha_creacion': response.data['fecha_creacion'],
            'ultima_actividad': None,
            'latitud': 99.0,
            'longitud': 99.0,
            'users_enabled': [],
            'url': 'http://testserver/api/dispositivos/' + str(id) + '/',
            'variables': []
        }

        # print("--------expectedData--------")
        # print(json.dumps(expectedData))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_create_variable(self):
        """
        Caso de prueba 06: Ensure we can create a new variable object.
        """

        id = 60
        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=id,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        # Variable
        data = {
            "nombre": "temperatura",
            "etiqueta": "TMP",
            "descripcion": "descrip",
            "unidad": "Celsius",
            "valor_min_permitido": "-50",
            "valor_max_permitido": "500",
            "dispositivo": id,
        }

        # url = reverse('dispositivo-list')
        url = "/api/dispositivos/" + str(id) + "/variables/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.post(url, data, format='json',
                               **{'Authorization': ''})

        # print("-----------response-------------------")
        # pprint(vars(response))

        expectedData = {
            'id': 1,
            "dispositivo": id,
            "nombre": "temperatura",
            "etiqueta": "TMP",
            "descripcion": "descrip",
            "unidad": "Celsius",
            "valor_min_permitido": "-50.000",
            "valor_max_permitido": "500.000",
            'fecha_creacion': response.data['fecha_creacion'],
            'url': 'http://testserver/api/dispositivos/60/variables/1/'
        }

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_get_variable(self):
        """
        Caso de prueba 07: Ensure we can GET a new variable object.
        """

        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=70,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        variable = Variable.objects.create(
            id=10,
            nombre="temperatura",
            etiqueta="TMP",
            descripcion="descrip",
            unidad="Celsius",
            valor_min_permitido="-50",
            valor_max_permitido="500",
            dispositivo=dispositivo,
        )

        # url = reverse('dispositivo-list')
        url = "/api/dispositivos/" + \
            str(dispositivo.id) + "/variables/" + str(variable.id) + '/'

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.get(url, format='json',
                              **{'Authorization': ''})

        # print("-----------response-------------------")
        # pprint(vars(response))

        expectedData = {
            'id': variable.id,
            "dispositivo": dispositivo.id,
            "nombre": "temperatura",
            "etiqueta": "TMP",
            "descripcion": "descrip",
            "unidad": "Celsius",
            "valor_min_permitido": "-50.000",
            "valor_max_permitido": "500.000",
            'fecha_creacion': response.data['fecha_creacion'],
            'url': 'http://testserver/api/dispositivos/70/variables/10/'
        }

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_get_list_variables(self):
        """
        Caso de prueba 08: Ensure we can list all variables object from a device.
        """

        # Create Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            etiqueta="DEV1",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        variable = Variable.objects.create(
            id=10,
            nombre="temperatura",
            etiqueta="TMP",
            descripcion="descrip",
            unidad="Celsius",
            valor_min_permitido="-50",
            valor_max_permitido="500",
            dispositivo=dispositivo,
        )

        # url = reverse('dispositivo-list', args={'pk': 1})
        # url = reverse('dispositivo-list')
        url = "/api/dispositivos/" + str(dispositivo.id) + "/variables/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.get(url, format='json',
                              **{'Authorization': ''})
        response.render()

        # print("-----------response-------------------x")
        # pprint(vars(response))
        # pprint(vars(response.content))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = {'count': 1, 'next': None, 'previous': None,
                        'results': [{
                            'id': 10,
                            'nombre': "temperatura",
                            'etiqueta': "TMP",
                            'descripcion': "descrip",
                            'unidad': "Celsius",
                            'fecha_creacion': response.data['results'][0]['fecha_creacion'],
                            'valor_min_permitido': "-50.000",
                            'valor_max_permitido': "500.000",
                            'dispositivo': dispositivo.id,
                            'url': 'http://testserver/api/dispositivos/1/variables/10/'
                        }]
                        }

        # # print("--------expectedData--------")
        # # print(json.dumps(expectedData))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_delete_variable(self):
        """
        Caso de prueba 04: Ensure we can delete a variable object.
        """

        # Create Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            etiqueta="DEV1",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        variable = Variable.objects.create(
            id=10,
            nombre="temperatura",
            etiqueta="TMP",
            descripcion="descrip",
            unidad="Celsius",
            valor_min_permitido="-50",
            valor_max_permitido="500",
            dispositivo=dispositivo,
        )

        # url = reverse('dispositivo-list', args={'pk': 1})
        # url = reverse('dispositivo-list')
        url = "/api/dispositivos/" + str(dispositivo.id) + "/variables/10/"

        # url = reverse('dispositivo-detail', args={'pk': 1})
        # url = "/api/dispositivos/30/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.delete(url, format='json',
                                 **{'Authorization': ''})
        response.render()
        # print("-----------response-------------------")
        # pprint(vars(response))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = None

        # print("--------expectedData--------")
        # print(json.dumps(expectedData))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_update_variable(self):
        """
        Caso de prueba 10: Ensure we can update a variable object.
        """

        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=1,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        variable = Variable.objects.create(
            id=10,
            nombre="temperatura",
            etiqueta="TMP",
            descripcion="descrip",
            unidad="Celsius",
            valor_min_permitido="-50",
            valor_max_permitido="500",
            dispositivo=dispositivo,
        )

        # Variable
        data = {
            "nombre": "humedad",
            "etiqueta": "HUM",
            "descripcion": "decrip_modif",
            "unidad": "Hps",
            "valor_min_permitido": "-50",
            "valor_max_permitido": "500",
            "dispositivo": 1
        }

        url = "/api/dispositivos/" + \
            str(dispositivo.id) + "/variables/" + str(variable.id) + "/"

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.put(url, data, format='json',
                              **{'Authorization': ''})
        response.render()
        # print("-----------response-------------------")

        # pprint(vars(response))
        # jsonResponse = json.dumps(response.data)
        # print(response.data['results'][0]['fecha_creacion'])

        expectedData = {
            'id': variable.id,
            "dispositivo": dispositivo.id,
            "nombre": "humedad",
            "etiqueta": "HUM",
            "descripcion": "decrip_modif",
            "unidad": "Hps",
            "valor_min_permitido": "-50.000",
            "valor_max_permitido": "500.000",
            'fecha_creacion': response.data['fecha_creacion'],
            'url': 'http://testserver/api/dispositivos/' + str(dispositivo.id) + '/variables/10/'
        }

        # # print("--------expectedData--------")
        # # print(json.dumps(expectedData))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_create_dato(self):
        """
        Caso de prueba 11: Ensure we can create a new dato object.
        """

        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=5,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        # Create Variable
        variable = Variable.objects.create(
            id=5,
            nombre="temperatura",
            etiqueta="TMP",
            descripcion="descrip",
            unidad="Celsius",
            valor_min_permitido="-50",
            valor_max_permitido="500",
            dispositivo=dispositivo,
        )

        # url = reverse('dispositivo-list')
        url = "/api/datos/"

        postData = [
                        {
                            "valor": "0.5",
                            "timestamp": "2006-10-25 14:30:59",
                            "variable": variable.id
                        }
                    ]


        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.post(url, postData, format='json',
                               **{'Authorization': ''})

        response.render()
        # print("-----------response-------------------")
        # pprint((json.dumps(response.data[0].timestamp_servidor)))
        # print(response.data[0]['timestamp_servidor'])

        expectedData = [{
                        "id": 1, 
                        "timestamp": response.data[0]['timestamp'], 
                        "timestamp_servidor": response.data[0]['timestamp_servidor'],
                        "valor": "0.500",
                        "variable": variable.id, 
                        "dispositivo": dispositivo.id
                        }]

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_get_dato_rango_fechas(self):
        """
        Caso de prueba 12: Ensure we can get a dato object.
        """

        # Crea Dispositivos en la BD
        dispositivo = Dispositivo.objects.create(
            id=5,
            etiqueta="DEV2",
            descripcion="descrip",
            estado=True,
            fecha_creacion="2019-08-01T21:54:31.541375Z",
            ultima_actividad=None,
            latitud=2,
            longitud=3
        )

        # Create Variable
        variable = Variable.objects.create(
            id=8,
            nombre="temperatura",
            etiqueta="TMP",
            descripcion="descrip",
            unidad="Celsius",
            valor_min_permitido="-50",
            valor_max_permitido="500",
            dispositivo=dispositivo,
        )

        dato = Dato.objects.create(
            valor="10.5",
            timestamp="2019-10-25 14:30:59",
            variable=variable
        )

        # url = reverse('dispositivo-list')
        start_date = "2019-10-24%2017:47:16"
        end_date = "2019-10-26%2017:47:16"

        url = "/api/datos/?variable=" + str(variable.id) + "&start_date=" + start_date + "&end_date=" + end_date

        # print("--------------client----------------")
        # pprint(vars(client))
        response = client.get(url, format='json', **{'Authorization': ''})

        response.render()
        # print("-----------response-------------------")
        # pprint(vars(response))
        # pprint((json.dumps(response.data[0].timestamp_servidor)))
        # print(response.data[0]['timestamp_servidor'])

        expectedData = [{
                        "id": 1, 
                        "timestamp": response.data[0]['timestamp'], 
                        "timestamp_servidor": response.data[0]['timestamp_servidor'],
                        "valor": "10.500",
                        "variable": variable.id, 
                        "dispositivo": dispositivo.id
                        }]

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(json.dumps(response.data),
                             json.dumps(expectedData))

    def test_get_dato_ultimos_100(self):
            """
            Caso de prueba 13: Ensure we can get last 100 datos object.
            """

            # Crea Dispositivos en la BD
            dispositivo = Dispositivo.objects.create(
                id=5,
                etiqueta="DEV2",
                descripcion="descrip",
                estado=True,
                fecha_creacion="2019-08-01T21:54:31.541375Z",
                ultima_actividad=None,
                latitud=2,
                longitud=3
            )

            # Create Variable
            variable = Variable.objects.create(
                id=8,
                nombre="temperatura",
                etiqueta="TMP",
                descripcion="descrip",
                unidad="Celsius",
                valor_min_permitido="-50",
                valor_max_permitido="500",
                dispositivo=dispositivo,
            )

            dato0 = Dato.objects.create(
                valor="10.5",
                timestamp="2019-10-25 14:30:59",
                variable=variable
            )
            dato1 = Dato.objects.create(
                valor="11.2",
                timestamp="2019-10-25 14:30:02",
                variable=variable
            )

            url = "/api/datos/?page=last&variable=" + str(variable.id)
                    
            # print("--------------client----------------")
            # pprint(vars(client))
            response = client.get(url, format='json', **{'Authorization': ''})

            response.render()
            # print("-----------response-------------------")
            # pprint(vars(response))
            # pprint((json.dumps(response.data[0].timestamp_servidor)))
            # print(response.data[0]['timestamp_servidor'])


            expectedData = {'count': 2, 'next': None, 'previous': None,
                            'results': [
                                        {
                                            "id": 2, 
                                            "timestamp": response.data['results'][0]['timestamp'], 
                                            "timestamp_servidor": response.data['results'][0]['timestamp_servidor'],
                                            "valor": "11.200",
                                            "variable": variable.id, 
                                            "dispositivo": dispositivo.id
                                        },
                                        {
                                            "id": 1, 
                                            "timestamp": response.data['results'][1]['timestamp'], 
                                            "timestamp_servidor": response.data['results'][1]['timestamp_servidor'],
                                            "valor": "10.500",
                                            "variable": variable.id, 
                                            "dispositivo": dispositivo.id
                                        }
                                        ]
                            }

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertJSONEqual(json.dumps(response.data),
                                 json.dumps(expectedData))

    def test_register_user(self):
                """
                Caso de prueba 15: Ensure we can register new user.
                """

                dataPost = {
                                "username": "username2",
                                "email": "email2@email.com",
                                "password1": "password123-",
                                "password2": "password123-"
                            }
                url = "/rest-auth/registration/"
                        
                # print("--------------client----------------")
                # pprint(vars(client))
                response = client.post(url, dataPost, format='json', **{'Authorization': ''})

                response.render()
                # print("-----------response-------------------")
                # print(response)
                # pprint(vars(response))
                # pprint((json.dumps(response.data[0].timestamp_servidor)))
                # print(response.data[0]['timestamp_servidor'])

                expectedData = {                    
                        "key": response.data['key'],
                        "user": {
                            "username": "username2",
                            "email": "email2@email.com",
                            "is_staff": False
                        }                    
                }

                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
                self.assertJSONEqual(json.dumps(response.data),
                                     json.dumps(expectedData))

    def test_login_user(self):
                """
                Caso de prueba 16: Ensure we can login last 100 datos object.
                """

                dataPost = {
                                "username": "username",
                                "password": "password123-",
                            }
                url = "/rest-auth/login/"
                        
                # print("--------------client----------------")
                # pprint(vars(client))
                response = client.post(url, dataPost, format='json', **{'Authorization': ''})

                response.render()
                print("-----------response-------------------")
                # print(response)
                # pprint(vars(response.data['key']))
                # print((response.data['key']))
                # pprint((json.dumps(response.data[0].timestamp_servidor)))
                # print(response.data[0]['timestamp_servidor'])

                expectedData = {                    
                                "key": response.data['key'],
                                "user": {
                                    "email": "username@gmail.com",
                                    "is_staff": True,
                                    "username": "username"
                                    }             
                                }

                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertJSONEqual(json.dumps(response.data),
                                     json.dumps(expectedData))