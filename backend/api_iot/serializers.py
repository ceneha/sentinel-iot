from rest_framework import serializers
from .models import Dispositivo, Variable, Dato, Alerta
from rest_framework_nested.relations import NestedHyperlinkedRelatedField, NestedHyperlinkedIdentityField
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer
from collections import OrderedDict
from django.contrib.auth import get_user_model
from django.conf import settings
from django.template.loader import render_to_string
from datetime import datetime, time, timedelta
import pytz
from decimal import *

import telegram  # this is from python-telegram-bot package


from pprint import pprint


User = get_user_model()


#  ------------------------------ TUTORIAL SERIALIZADORES ---------------------------------------------
#  https://medium.com/geekculture/introduction-to-serializers-in-the-django-rest-framework-8e0f20e1cb10
# 
#  ---------------------------------------------------------------------------------------------------

"""
    Un Serializer es ...completar...

    Serializers allow complex data such as querysets and model instances to be converted to 
    native Python datatypes that can then be easily rendered into JSON, XML or other content types. 
    Serializers also provide deserialization, allowing parsed data to be converted back into complex types, 
    after first validating the incoming data.

    Se utilizaron 3 clases de Serializers

    1- `ModelSerializer`: es un` Serializer` normal, excepto que:

        * Un conjunto de campos predeterminados se rellena automáticamente.
        * Se completa automáticamente un conjunto de validadores predeterminados.
        * Se proporcionan implementaciones predeterminadas `.create ()` y `.update ()`.

        El proceso de determinar automáticamente un conjunto de campos de serializador
        basado en los campos del modelo es razonablemente complejo, pero prácticamente
        no es necesario profundizar en la implementación.

        Si la clase `ModelSerializer` * no * genera el conjunto de campos que
        necesita declarar explícitamente los campos adicionales / diferentes en
        la clase serializador, o simplemente use una clase `Serializer`.

    2- 'HiperLinkedModelSerializer': Es un tipo de `ModelSerializer` que define relaciones de
        los modelos a través de hiperlinks en vez de relaciones a través de primary keys.
        Específicamente:

        * Se incluye un campo 'url' en lugar del campo 'id'.
        * Las relaciones con otras instancias son hipervínculos, en lugar de claves primarias.

    3-  'NestedHyperlinkedModelSerializer': Es un tipo de `ModelSerializer` que utiliza relaciones de modelos
        hipervinculadas con claves COMPUESTAS (Anidadas) en  lugar de relaciones a través de primary keys.
        Específicamente:

        * Se incluye un campo 'url' en lugar del campo 'id'.
        * Las relaciones con otras instancias son hipervínculos, en lugar de claves primarias.

"""



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


# class UsersEnabledListingField(serializers.RelatedField):
#     def to_representation(self, value):
#         user_enabled =

#         return super().to_representation(value)

# class DispositivoSerializer(serializers.HyperlinkedModelSerializer):




class DatoSerializer(serializers.ModelSerializer):

    # dispositivo = serializers.PrimaryKeyRelatedField(
    #     many=False, read_only=False, queryset=Dispositivo.objects.all())

    # variable = serializers.PrimaryKeyRelatedField(
    #     many=False, read_only=False, queryset=Variable.objects.select_related(id=dispositivo))

    class Meta:
        model = Dato
        fields = ('id', 'timestamp',
                  'timestamp_servidor', 'valor', 'variable', 'dispositivo')

    # dispositivo = serializers.SerializerMethodField()

    # """
    # This is a read-only field. It gets its value by calling a method on the serializer class it is attached to.
    # It can be used to add any sort of data to the serialized representation of your object.
    # -Si el valor del dispositivo no viaja en la request, se inserta automáticamente en base
    # al dispositivo de la variable enviada.
    # -Quien llama a éste método hace un loop sobre el arreglo de datos enviados en la request,
    # por lo que se actualizan todos los datos registrados.
    # """

    # def get_dispositivo(self, obj):
    #     return obj.variable.dispositivo.id

    """
    Validations on specific fields
    """
    def validate(self, data):

        if(not isinstance(data, OrderedDict)):
            raise serializers.ValidationError(
                "Formato inválido. Formato correcto: List de Datos: [{" + "valor: x, variable: z" + "},]")

        data = dict(data)

        if data['variable'].dispositivo.estado is False:
            raise serializers.ValidationError(
                "El dispositivo se encuentra deshabilitado. Los datos enviados no serán registrados")

        # Valida el envío de alerta
        self.validate_sending_alert(data)

        minVal = data['variable'].valor_min_permitido
        maxVal = data['variable'].valor_max_permitido

        if minVal is not None and data['valor'] < minVal:
            raise serializers.ValidationError(
                "Valor recibido inferior al mínimo permitido")
        elif maxVal is not None and data['valor'] > maxVal:
            raise serializers.ValidationError(
                "Valor recibido superior al máximo permitido")

        return data

    # Utiliza un la API de telegram para enviar una notificación al canal 'SentinelIot' si
    # se detectaron valores fuera del rango de valores de alertas programados.
    # Nota: No se envía más de 1 mensaje por hora si durante esa misma hora se detectan 2  o mas ocurrencias de la alerta.
    def validate_sending_alert(self, data):
        alertas = Alerta.objects.filter(variable=data['variable'])
        for alerta in alertas:

            if (alerta.min_alerta is not None and data['valor'] < alerta.min_alerta) or (alerta.max_alerta is not None and alerta.max_alerta < data['valor']):

                # solo envia si pasaron mas de 30 min desde el ultimo envio
                expiracion_bloqueo = alerta.ultimo_envio + \
                    timedelta(minutes=30)
                current_datetime = datetime.now()

                expiracion_bloqueo_envio_tmp = expiracion_bloqueo.replace(
                    tzinfo=pytz.utc)
                current_datetime_tmp = current_datetime.replace(
                    tzinfo=pytz.utc)

                if(expiracion_bloqueo_envio_tmp < current_datetime_tmp):

                    message =   "<b>Alerta !!</b> " + \
                                "\r\n<b>Origen:</b> <i>Dispositivo: " +data['variable'].dispositivo.etiqueta + " (ID: " + str(data['variable'].dispositivo.id) + ")" +  \
                                " - Sensor: " + data['variable'].nombre + " (ID: " + str(data['variable'].id) + ").</i>" \
                                "\r\n<b>Motivo: </b> <i>Dato registrado en el rango de alertas.</i>" + \
                                "\r\n<b>Dato:</b> <i>{ timestamp: " + str(data['timestamp']) +", valor:" + str(data['valor']) + "}</i>" + \
                                "\r\n<b>Rango de alertas:</b> <i>(" + str(alerta.min_alerta) + ";" + str(alerta.max_alerta) + ")</i> " 

                    telegram_settings = settings.TELEGRAM
                    bot = telegram.Bot(token=telegram_settings['bot_token'])
                    bot.send_message(chat_id="@%s" % telegram_settings['channel_name'],
                                     text=message, parse_mode='HTML')

                    alerta.ultimo_envio = current_datetime
                    alerta.save()

# class VariableSerializer(serializers.HyperlinkedModelSerializer):

"""
Serializer del modelo 'Variable'.
Además de los campos del propio modelo, se define:
* Una URL de cada Variable


URLs:
    List: < http://127.0.0.1:8000/api/dispositivos/1/variables/>
    Detail, Update, Get, Delete: < http://127.0.0.1:8000/api/dispositivos/1/variables/1/ >

Estructura:

"""
class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = ('id', 'nombre', 'descripcion', 'etiqueta', 'unidad',
                  'valor_min_permitido', 'valor_max_permitido', 'fecha_creacion', 'dispositivo', 'url')

        url = serializers.HyperlinkedIdentityField(
            read_only=True,
            view_name='variables-detail',
            # parent_lookup_kwargs={'pk': 'pk'}
        )



class DispositivoVariableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Variable
        fields = ('id', 'nombre', 'descripcion', 'etiqueta', 'unidad',
                  'valor_min_permitido', 'valor_max_permitido', 'fecha_creacion', 'dispositivo', 'url')

    url = NestedHyperlinkedIdentityField(
        read_only=True,
        view_name='dispositivo-variables-detail',
        parent_lookup_kwargs={'dispositivo_pk': 'dispositivo__pk'}
    )

    # dispositivoUrl = NestedHyperlinkedRelatedField(
    #     read_only=True,
    #     view_name='dispositivo',
    #     parent_lookup_kwargs={'dispositivo_pk': 'dispositivo__pk'}
    # )





"""
Serializer basado en el modelo 'Dispositivo'.
Además de los campos del propio modelo, se define:
  * Una URL de cada dispositivo específico
  * Por cada dispositivo, un arreglo de variables "anidadas" a través de 'DispositivoVariableSerializer'

URLs:
    List: < http://127.0.0.1:8000/api/dispositivos/ >
    Detail, Update, Get, Delete: < http://127.0.0.1:8000/api/dispositivos/1/ >

Estructura:
    [{
        "id": 1,
        "etiqueta": "EstacionX",
        ...,
        "url": "http://127.0.0.1:8000/api/dispositivos/1/",
        "variables": [
            {
                "id": 1,
                "nombre": "Temperatura",
                ...,
                "url": "http://127.0.0.1:8000/api/dispositivos/1/variables/1/"
            }
        ]
    }]
"""
class DispositivoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dispositivo
        fields = ('id', 'etiqueta', 'descripcion', 'estado', 'visibilidad',
                  'fecha_creacion', 'ultima_actividad', 'latitud', 'longitud', 'users_enabled', 'url', 'variables')

    variables = DispositivoVariableSerializer(
        many=True,
        read_only=True)

#  users_enabled =

    # url = serializers.HyperlinkedIdentityField(
    #     read_only=True,
    #     view_name='dispositivo-detail',
    # )

    url = NestedHyperlinkedIdentityField(
        read_only=True,
        view_name='dispositivo-detail',
        parent_lookup_kwargs={'pk': 'pk'}
    )


class DatosMultisensoresSerializer(serializers.Serializer):
    timestamp = serializers.DateTimeField(input_formats=None)
    datos = serializers.ListField(child=serializers.DictField(), allow_empty=False)




class DatosVariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dato
        fields = ['timestamp', 'valor']



class DatosVariablesDispositivoSerilizer(serializers.ModelSerializer):

    # Serializa el resultado del metodo "get_data_realtime" definido en el modelo Dato.
    # El campo "data" está definido en el modelo Dato, en la foreign_key de su Variable "madre": related_name='data'.
    # De otro modo DRF no puede reconocer la relacion entre Dato y los datos de la variable.
    # Ref: https://newbedev.com/how-do-you-filter-a-nested-serializer-in-django-rest-framework
    data = DatosVariableSerializer(many=True, read_only=True, source='get_data_realtime') 

    class Meta:
        model = Variable
        fields = ['id','nombre', 'data']
