from rest_access_policy import AccessPolicy


"""
En ésta clase se definen todas las politicas de acceso a los recursos del sistema.

En ésta clase se definen definen y gestionan las politicas de acceso a l

Para detalles de implementación consultar la documentación de la librería:
https://github.com/rsinger86/drf-access-policy
"""


class ApiAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": "*",
            "effect": "allow",
            "condition": "is_staff"
        },
        {
            "action": ["list", "retrieve", "options"],
            "principal": "*",
            "effect": "allow",
            "condition": "is_not_staff"
        },
        {
            "action": ["create_datos_multisensores"],
            "principal": ["group:is_staff"],
            "effect": "allow"            
        }

        # {
        #     "action": ["list", "retrieve", "options"],
        #     "principal": "*",
        #     "effect": "allow",
        #     "condition": "*"
        # },



        # {
        #     "action": ["list", "retrieve"],
        #     "principal": "*",
        #     "effect": "allow"
        # },
        # {
        #     "action": ["publish", "unpublish"],
        #     "principal": ["group:editor"],
        #     "effect": "allow"
        # },
        # {
        #     "action": ["delete"],
        #     "principal": ["*"],
        #     "effect": "allow",
        #     "condition": "is_author"
        # },
        # {
        #     "action": ["*"],
        #     "principal": ["*"],
        #     "effect": "deny",
        #     "condition": "is_happy_hour"
        # }
    ]

    def is_staff(self, request, view, action) -> bool:
        return request.user.is_staff

    def is_not_staff(self, request, view, action) -> bool:
        return not request.user.is_staff

    """
        Un usuario administrador (is_staff = True) puede consultar por todos los dispositivos.
        Los usuarios visitantes (is_staff = False) solo pueden consultar los dispositivos con visibilidad = True
    """
    @classmethod
    def dispositivos_scope_queryset(cls, request, queryset):
        return queryset
