from django.apps import AppConfig


class ApiIotConfig(AppConfig):
    name = 'api_iot'
