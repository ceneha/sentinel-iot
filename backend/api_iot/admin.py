from django.contrib import admin

# Register your models here.
from .models import Dispositivo, Variable, Dato, Alerta
from django.contrib.auth import get_user_model

User = get_user_model()

admin.site.register(Dispositivo)
admin.site.register(Variable)
admin.site.register(Alerta)
admin.site.register(Dato)
