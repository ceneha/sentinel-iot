// https://stackoverflow.com/questions/45367298/could-not-proxy-request-pusher-auth-from-localhost3000-to-http-localhost500

const proxy = require("http-proxy-middleware");
module.exports = function(app) {
  app.use(proxy("/**", { // https://github.com/chimurai/http-proxy-middleware
    target: "http://app_backend_prod:8000",
    secure: false,
    changeOrigin: true
  }));
};

