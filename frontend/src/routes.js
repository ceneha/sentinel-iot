import React from 'react';
import { Route, Switch } from 'react-router-dom';

import DispositivosList from './containers/DispositivosListView';
import DispositivoDetail from './containers/DispositivoDetailView';
import VariableDetail from './containers/VariableDetailView';
import Login from './containers/Login';
import Signup from './containers/Signup';


//export const ServerDomain = 'https://iot-REACT_APP_HOST_IP_ADDRESS.herokuapp.com';
// export const ServerDomain = 'http://127.0.0.1:80';
// export const ServerDomain = 'http://0.0.0.0:1337';

// Read the API_URL from .env file


// export const ServerDomain = process.env.REACT_APP_HOST_IP_ADDRESS;
// export const ServerDomain = window.location.origin;

// export const ServerDomain = 'http://127.0.0.1:8000';
//export const ServerDomain = 'http://web3.unl.edu.ar:8000';
export const ServerDomain = 'http://' + window.location.hostname + ':8000';

const BaseRouter = () => (
    <div>
        {console.log(ServerDomain)}
        <Switch>
            <Route exact path='/' component={DispositivosList} /> {" "}
            <Route exact path='/dispositivos/:dispositivoID/' component={DispositivoDetail} /> {" "}
            <Route exact path='/dispositivos/:dispositivoID/variables/:variableID/' component={VariableDetail} /> {" "}
            <Route exact path='/login/' component={Login} /> {" "}
            <Route exact path='/signup/' component={Signup} /> {" "}
        </Switch>
    </div>
)

export default BaseRouter;