import React from 'react';
import { connect } from 'react-redux';
import Permissions from "react-redux-permissions"
import { Descriptions, Badge, Divider, List } from 'antd';
import axios from 'axios';


import * as actions from '../store/actions/dispositivos';
import * as selectors from '../store/selectors/dispositivos';

import { ServerDomain } from '../routes';
import DispositivoForm from '../components/DispositivoForm';
import VariablesList from './VariablesListView';
import CsvUploadDatos from '../components/CsvUploadDatos';


export class DispositivoDetail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dispositivoID: this.props.match.params.dispositivoID,
            users: [],
            usuariosHabilitados: [],
            usernamesEnabled: []
        }
    }


    //Fetch inicial del dispositivo en el componente
    componentDidMount() {

        const dispositivoID = this.props.match.params.dispositivoID;

        if (dispositivoID) {
            this.props.onFetchDispositivo(this.props.token, dispositivoID);
        }

        if (this.props.token) {

            axios.defaults.headers = {
                "Authorization": `Token ${this.props.token}`,
            }

            return axios.get(`${ServerDomain}/api/users/`)
                .then(response => {

                    if (response.status === 201 || response.status === 200) { //Fetched OK
                        this.setState({ users: response.data });
                    }
                })
                .catch(error => {
                    console.log("err DispositivoForm", error);
                    // if (error.response) {
                    //   // The request was made and the server responded with a status code
                    //   // that falls out of the range of 2xx
                    //   console.log("err.response", err.response);
                    // }
                })
        }
    }

    async componentWillReceiveProps(newProps) {

        //arma el listado de usuarios habilitados en el detalle del dispositivo
        if (newProps && newProps.dispositivo && newProps.dispositivo !== this.props.dispositivo && this.state.users !== []) {

            const usernamesEnabled = this.state.users.map(user => {
                const usersDispositivo = newProps.dispositivo.users_enabled;

                for (var i = 0; i < usersDispositivo.length; i++) {
                    if (usersDispositivo[i].toString() === user.id.toString()) {
                        return user.username;
                    }
                }
            })

            var filteredUsernames = usernamesEnabled.filter(function (item) {
                return item != null;
            })

            await this.setState({ usernamesEnabled: filteredUsernames });
        }
    }


    render() {
        return (

            <div>
                <Permissions allowed={["admin"]}>
                    <DispositivoForm
                        {...this.props}
                        requestType="put"
                        btnText="Modificar Dispositivo"
                        users={this.state.users}
                    />
                </Permissions>

                <Descriptions title="Dispositivo" layout="vertical" bordered size="small">
                    <Descriptions.Item label="Dispositivo" span={1}>{this.props.dispositivo ? this.props.dispositivo.etiqueta : ""}</Descriptions.Item>
                    <Descriptions.Item label="Estado" span={1}>
                        {this.props.dispositivo && this.props.dispositivo.estado === true ?
                            <Badge status="success" text="Habilitado" /> :
                            <Badge status="error" text="Deshabilitado - Fuera de linea" />
                        }
                    </Descriptions.Item>


                    {this.props.permissions === 'admin' ?
                        <Descriptions.Item label="Usuarios habilitados" span={2}>
                            <Permissions allowed={["admin"]}>
                                <List
                                    // header={<b>Usuarios habilitados</b>}
                                    size="small"
                                    dataSource={this.state.usernamesEnabled}
                                    renderItem={item => <List.Item><Badge color='green' text={item} /></List.Item>}
                                />
                            </Permissions>
                        </Descriptions.Item>
                        :
                        <div></div>
                    }

                    {this.props.dispositivo ?
                        <Descriptions.Item label="Localización" span={1} >{'(' + this.props.dispositivo.latitud + ' ; ' + this.props.dispositivo.longitud + ')'}</Descriptions.Item>
                        :
                        <Descriptions.Item label="Localización" span={1} >{'-'}</Descriptions.Item>
                    }

                    <Descriptions.Item label="Fecha Creación:" span={2}> {this.props.dispositivo ? this.props.dispositivo.fecha_creacion : ""} </Descriptions.Item>


                    <Descriptions.Item label="Descripción" span={3}>
                        {this.props.dispositivo ? this.props.dispositivo.descripcion : ""}
                    </Descriptions.Item>

                </Descriptions>

                <Divider />

                <CsvUploadDatos token={this.props.token} />
                <Divider />

                <VariablesList {...this.props} dispositivoID={this.state.dispositivoID} className='forms' />
            </div >
        );
    }
}

// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {
    // console.log("DetailStateToProps", state.authentication.token);

    return {
        permissions: state.permissions.slice(-1).pop(),
        token: state.authentication.token,
        dispositivo: selectors.selectCurrentDispositivo(state.entities.dispositivos.data, state.entities.dispositivos.currentId),
    }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        // handleRedirect: () => dispatch(push('/')),
        onFetchDispositivo: (token, dispositivoID) => dispatch(actions.fetchDispositivo(token, dispositivoID)),
    }
}

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DispositivoDetail));
export default connect(mapStateToProps, mapDispatchToProps)(DispositivoDetail);