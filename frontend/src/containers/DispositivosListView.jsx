import React from 'react';
import { connect } from 'react-redux';
import Permissions from "react-redux-permissions"
import { Divider, Typography } from 'antd';
import axios from 'axios';

import { ServerDomain } from '../routes';
import * as actions from '../store/actions/dispositivos';
import * as selectors from '../store/selectors/dispositivos';

import DispositivosTable from '../components/DispositivosTable';
import DispositivoForm from '../components/DispositivoForm';

import DispositivosMap from './MapViewPort';

const { Title } = Typography;

export class DispositivosList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentDidMount() {

        if (this.props.token) {
            this.props.dispatchFetchDispositivos(this.props.token);

            if (this.props.permissions === 'admin') {
                axios.defaults.headers = {
                    "Authorization": `Token ${this.props.token}`,
                }

                return axios.get(`${ServerDomain}/api/users/`)
                    .then(response => {
                        if (response.status === 201 || response.status === 200) { //Fetched OK

                            // console.log("RESPONSE.DATA", response.data)
                            const users = response.data;
                            this.setState({ users: users });
                        }
                    })
                    .catch(error => {
                        // console.log("err DispositivoForm", error);

                        // if (error.response) {
                        //   // The request was made and the server responded with a status code
                        //   // that falls out of the range of 2xx
                        //   console.log("err.response", err.response);
                        // }
                    })
            }
        }
    }

    /**
    Se ejecuta siempre que nuevas propiedades "lleguen" al componente.
    componentDidMount no sirve en éste caso porque se ejecuta SOLO cuando se monta el componente
    y el token aún sería null pues el login y las actions que modifican el estado del token se ejecutan
    posteriormente.
*/
    componentWillReceiveProps(newProps) {
        if (newProps.token && newProps.token !== this.props.token) {
            this.props.dispatchFetchDispositivos(newProps.token);
        }
    }

    render() {
        return (
            <div>
                <Permissions allowed={["admin", "visitor"]}>

                    <Title level={4}>Dispositivos</Title>

                    <div className="leaflet-container">
                        <DispositivosMap dispositivos={this.props.dispositivos} />
                    </div>

                    <br></br>
                    <Permissions allowed={["admin"]}>
                        <DispositivoForm
                            {...this.props}
                            requestType="post"
                            btnText="Nuevo Dispositivo"
                            users={this.state.users}
                        />
                    </Permissions>

                    <Divider />

                    <DispositivosTable token={this.props.token} dispositivos={this.props.dispositivos} />
                </Permissions>
            </div>
        )
    }
}


// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivosList'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {
    return {
        permissions: state.permissions.slice(-1).pop(),
        token: state.authentication.token,
        dispositivos: selectors.objectToArray(state.entities.dispositivos.data),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchFetchDispositivos: (token) => dispatch(actions.fetchDispositivos(token)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(DispositivosList);