import React from 'react';
import { connect } from 'react-redux';
import Permissions from "react-redux-permissions"
import { Divider, Typography } from 'antd';

import * as actions from '../store/actions/variables';
import * as selectors from '../store/selectors/variables'

import VariablesTable from '../components/VariablesTable';
import VariableForm from '../components/VariableForm';
import GraficaMultiVariableRealTime from '../components/GraficaMultiVariableRealTime';

const { Title } = Typography;

export class VariablesList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            dispositivoID: this.props.dispositivoID,
        }
    }

    componentDidMount() {
        if (this.props.token && this.props.dispositivoID) {
            this.props.dispatchFetchVariables(this.props.token, this.props.dispositivoID);
        }
    }

    /**
     * Se ejecuta siempre que nuevas propiedades "lleguen" al componente.
     * Permite mantener actualizado el state del componente tras cada  dispatch de un action que 
     * modifque las props del componente: dispatchFetchVariables
     */
    componentWillReceiveProps(newProps) {

        // Cuando el componente se monta el ID del dispositivo es null:
        // Cuando se termine de actualizar el state del componente y se tenga el ID del dispositivo 
        // se realiza un fetch de las variables. 
        if (this.props.token && this.props.dispositivoID && newProps.dispositivoID !== this.props.dispositivoID) {
            this.props.dispatchFetchVariables(this.props.token, newProps.dispositivoID);
        }
    }


    render() {
        return (
            <div>
                <Title level={4}>Variables</Title>

                <GraficaMultiVariableRealTime dispositivoID={this.state.dispositivoID} token={this.props.token}/>

                <Permissions allowed={["admin"]}>
                    <VariableForm {...this.props} requestType="post" btnText="Crear Variable" dispositivoID={this.props.dispositivoID} variableID={null} />
                </Permissions>

                <Divider type="horizontal" />

                <Permissions allowed={["admin", "visitor"]}>
                    <VariablesTable token={this.props.token} dispositivoID={this.props.dispositivoID} variables={this.props.variables} />
                </Permissions>
            </div>
        )
    }
}


// Mapea token y variables almacenados en el state del Store de la app para que puedan ser utilizados
// como una propiedades en el componente 'VariablesList'.
// Ej: Ahora en el componente puedo acceder al valor del token a través de: this.props.token
const mapStateToProps = state => {

    return {
        token: state.authentication.token,
        variables: selectors.objectToArray(state.entities.variables.data),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchFetchVariables: (token, dispositivoID) => dispatch(actions.fetchVariables(token, dispositivoID)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VariablesList);
//export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VariablesList));