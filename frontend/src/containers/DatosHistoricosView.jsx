import React from 'react';
import { connect } from 'react-redux';
import Permissions from "react-redux-permissions"
import { Typography } from 'antd';

import DatosHistoricosChart from '../components/DatosHistoricosChart';
import DataForm from '../components/DataForm';
import * as actions from '../store/actions/datos';


const { Title } = Typography;

export class DatosHistoricos extends React.Component {
    // constructor(props) {
    //     super(props);
    // }


    // convert Date to Date with  format YYYY-mm-dd HH-MM-SS
    DateTo_YYYYmmdd_HHMMSS = (d) => {
        d = new Date(d.getTime() - 3000000);
        return d.getFullYear().toString() + "-" + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : "0" + (d.getMonth() + 1).toString()) + "-" + (d.getDate().toString().length === 2 ? d.getDate().toString() : "0" + d.getDate().toString()) + " " + (d.getHours().toString().length === 2 ? d.getHours().toString() : "0" + d.getHours().toString()) + ":" + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : "0" + (parseInt(d.getMinutes() / 5) * 5).toString()) + ":00";
    }


    UNSAFE_componentWillMount() {
        //Reseteo del store los datos historicos almacenados, 
        //por si quedaron datos almacenados de una vieja consulta
        this.props.onResetDatosHistoricos();

        const token = this.props.token;


        //Fetch inicial de datos historicos del ultimo mes
        var d = new Date();
        var today = this.DateTo_YYYYmmdd_HHMMSS(d);

        var ma = new Date();
        ma.setMonth(ma.getMonth() - 1);
        var monthAgo = this.DateTo_YYYYmmdd_HHMMSS(ma);

        const parameters = {
            variableID: this.props.variableID,
            start_date: monthAgo,
            end_date: today
        }

        this.props.fetchFirstDatosHistoricos(token, parameters);
    }


    render() {
        return (
            <div className='forms'>
                <Permissions allowed={["admin", "visitor"]}>
                    <Title level={4}>Registros históricos</Title>

                    <DatosHistoricosChart />
                    <DataForm token={this.props.token} />
                </Permissions>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onResetDatosHistoricos: () => dispatch(actions.resetDatosHistoricos()),
        fetchFirstDatosHistoricos: (token, parameters) => dispatch(actions.fetchDatosHistoricos(token, parameters)),
    }
}

export default connect(null, mapDispatchToProps)(DatosHistoricos);