import React from 'react';

import { connect } from 'react-redux';
import Permissions from "react-redux-permissions";
import { Descriptions, Spin, Divider } from 'antd';

import * as actions from '../store/actions/variables';
import * as selectors from '../store/selectors/variables';

import VariableForm from '../components/VariableForm';
import VariableRealTimeChart2 from '../components/VariableRealTimeChart2';
import DatosHistoricos from './DatosHistoricosView';



export class VariableDetail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dispositivoID: this.props.match.params.dispositivoID,
            variableID: this.props.match.params.variableID
        }
    }

    //Fetch inicial del dispositivo en el componente
    componentDidMount() {
        if (this.props.token && this.state.dispositivoID && this.state.variableID) {
            this.props.onFetchVariable(this.props.token, this.state.dispositivoID, this.state.variableID);
        }
    }



    render() {
        return (

            <div>

                {this.props.variableObj ?

                    <div>
                        <Permissions allowed={["admin"]}>
                            <VariableForm
                                // {...this.props}
                                requestType="put"
                                dispositivoID={this.state.dispositivoID}
                                variableID={this.state.variableID}
                                variableObj={this.props.variableObj}
                                btnText="Editar"
                            />
                        </Permissions>

                        <Descriptions title="Variable" layout="vertical" bordered size="small">
                            <Descriptions.Item label="Nombre" span={1}>{this.props.variableObj.nombre}</Descriptions.Item>
                            <Descriptions.Item label="Etiqueta" span={1}>{this.props.variableObj.etiqueta}</Descriptions.Item>
                            <Descriptions.Item label="Dispositivo" span={1}>{this.props.variableObj.dispositivo}</Descriptions.Item>


                            <Descriptions.Item label="Unidad" span={1}>{this.props.variableObj.unidad}</Descriptions.Item>
                            <Descriptions.Item label="Valor mínimo permitido" span={1} >{this.props.variableObj.valor_min_permitido}</Descriptions.Item>
                            <Descriptions.Item label="Fecha máximo permitido" span={1}> {this.props.variableObj.valor_max_permitido} </Descriptions.Item>


                            <Descriptions.Item label="Descripción" span={3}>
                                {this.props.variableObj.descripcion}
                            </Descriptions.Item>

                        </Descriptions>

                        <Divider type="horizontal" />

                        <VariableRealTimeChart2 token={this.props.token} variableID={this.state.variableID} variableNombre={this.props.variableObj.nombre} />

                        <Divider type="horizontal" />

                        <DatosHistoricos token={this.props.token} variableID={this.state.variableID} variableNombre={this.props.variableObj.nombre} />

                    </div>

                    :

                    <div className="spin-loading">
                        <Spin tip="Cargando..." />
                    </div>

                }
            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.authentication.token,
        variableObj: selectors.selectCurrentVariable(state.entities.variables.data, state.entities.variables.currentId),
    }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        onFetchVariable: (token, dispositivoID, variableID) => dispatch(actions.fetchVariable(token, dispositivoID, variableID)),
    }
}

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VariableDetail));
export default connect(mapStateToProps, mapDispatchToProps)(VariableDetail);


