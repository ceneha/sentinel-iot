import React from 'react'
import { Badge } from 'antd';
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';


class DispositivosMap extends React.Component {

    render() {
        let santa_fe_coords = ['-31.6179774', '-60.7762977'];


        return (
            <LeafletMap
                center={santa_fe_coords}
                zoom={6}
                maxZoom={10}
                attributionControl={true}
                zoomControl={true}
                doubleClickZoom={true}
                scrollWheelZoom={true}
                dragging={true}
                animate={true}
                easeLinearity={0.35}
            >
                <TileLayer
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />

                {
                    this.props.dispositivos ?
                        <div>
                            {this.props.dispositivos.map((item, index) =>
                                <div key={index}>
                                    <Marker position={[item.latitud, item.longitud]}>
                                        <Popup>
                                            <b>Dispositivo: </b> {item.etiqueta} <br />
                                            <b>Estado: </b> {item.estado === true ?
                                                <Badge status="success" text="Habilitado" /> :
                                                <Badge status="error" text="Deshabilitado - Fuera de linea" />} <br />
                                            <b>Última Actividad: </b> {Date(item.ultima_actividad)}
                                        </Popup>
                                    </Marker>

                                </div>
                            )}
                        </div>
                        :
                        null

                }

            </LeafletMap>
        );
    }
}

export default DispositivosMap;