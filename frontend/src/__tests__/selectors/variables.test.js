import { selectCurrentVariable, objectToArray } from '../../store/selectors/variables';
import expect from 'expect';


describe('variables selector', () => {


    it('selectCurrentVariable OK', () => {
        const variablesListMock = [
            {
                "id": 63,
                "nombre": "VAR1",
                "descripcion": "tu variable",
                "etiqueta": "var1",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            },
            {
                "id": 65,
                "nombre": "VAR65",
                "descripcion": "tu variable",
                "etiqueta": "var65",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            }
        ]

        const currentVar = {
            "id": 63,
            "nombre": "VAR1",
            "descripcion": "tu variable",
            "etiqueta": "var1",
            "unidad": "farenheti",
            "valor_min_permitido": "-5.000",
            "valor_max_permitido": "98.000",
            "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
            "dispositivo": 45,
            "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
        }


        expect(selectCurrentVariable(variablesListMock, 63)).toEqual(currentVar);
    })

    it('selectCurrentVariable NULL', () => {
        const variablesListMock = [];

        expect(selectCurrentVariable(variablesListMock, 45)).toEqual(null);
    });


    it('objectToArray ', () => {
        const variablesObjectsListMock = {
            0: {
                "id": 63,
                "nombre": "VAR1",
                "descripcion": "tu variable",
                "etiqueta": "var1",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            },
            1: {
                "id": 65,
                "nombre": "VAR65",
                "descripcion": "tu variable",
                "etiqueta": "var65",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            }
        };

        const variablesArrayListMock = [
            {
                "id": 63,
                "nombre": "VAR1",
                "descripcion": "tu variable",
                "etiqueta": "var1",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            },
            {
                "id": 65,
                "nombre": "VAR65",
                "descripcion": "tu variable",
                "etiqueta": "var65",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            }
        ]

        expect(objectToArray(variablesObjectsListMock, 45)).toEqual(variablesArrayListMock);
    })

});