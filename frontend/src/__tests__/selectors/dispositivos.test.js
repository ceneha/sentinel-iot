import { selectCurrentDispositivo, objectToArray } from '../../store/selectors/dispositivos';
import * as actionTypes from '../../store/actions/actionTypes';
import * as mocks from '../../mocks/datosMocks';
import expect from 'expect';


describe('dispositivos selector', () => {


    it('selectCurrentDispositivo OK', () => {
        const dispositivosListMock = [
            {
                "id": 45,
                "etiqueta": "dev1",
                "descripcion": "asd",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 5.3,
                "longitud": 2.4,
                "users_enabled": [
                    7
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/45/"
            },
            {
                "id": 46,
                "etiqueta": "dev1",
                "descripcion": "test46",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 80,
                "longitud": 25,
                "users_enabled": [
                    5
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/46/"
            }
        ];

        const currentDev = {
            "id": 45,
            "etiqueta": "dev1",
            "descripcion": "asd",
            "estado": true,
            "visibilidad": false,
            "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
            "ultima_actividad": "2006-10-25T14:30:59-03:00",
            "latitud": 5.3,
            "longitud": 2.4,
            "users_enabled": [
                7
            ],
            "url": "http://127.0.0.1:8000/api/dispositivos/45/"
        };


        expect(selectCurrentDispositivo(dispositivosListMock, 45)).toEqual(currentDev);
    })

    it('selectCurrentDispositivo NULL', () => {
        const dispositivosListMock = [];

        expect(selectCurrentDispositivo(dispositivosListMock, 45)).toEqual(null);
    })


    it('objectToArray', () => {
        const dispositivosObjectsListMock = {
            0: {
                "id": 45,
                "etiqueta": "dev1",
                "descripcion": "asd",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 5.3,
                "longitud": 2.4,
                "users_enabled": [
                    7
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/45/"
            },
            1: {
                "id": 46,
                "etiqueta": "dev1",
                "descripcion": "test46",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 80,
                "longitud": 25,
                "users_enabled": [
                    5
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/46/"
            }
        };

        const dispositivosArrayListMock = [
            {
                "id": 45,
                "etiqueta": "dev1",
                "descripcion": "asd",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 5.3,
                "longitud": 2.4,
                "users_enabled": [
                    7
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/45/"
            },
            {
                "id": 46,
                "etiqueta": "dev1",
                "descripcion": "test46",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 80,
                "longitud": 25,
                "users_enabled": [
                    5
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/46/"
            }
        ];

        expect(objectToArray(dispositivosObjectsListMock, 45)).toEqual(dispositivosArrayListMock);
    })

});