import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios';


import * as actions from '../../store/actions/dispositivos';
import * as types from '../../store/actions/actionTypes'

import expect from 'expect' // You can use any testing library

// for some reason i need to do this to fix those reducer keys undefined errors..
// jest.mock('../../store/configureStore.js')


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Test from redux actions: /store/actions/dispositivos.jsx', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });


    describe('fetchDispositivos(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";

        const responseMock = {
            results: [
                {
                    "id": 45,
                    "etiqueta": "dev1",
                    "descripcion": "asd",
                    "estado": true,
                    "visibilidad": false,
                    "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                    "ultima_actividad": "2006-10-25T14:30:59-03:00",
                    "latitud": 5.3,
                    "longitud": 2.4,
                    "users_enabled": [
                        7
                    ],
                    "url": "http://127.0.0.1:8000/api/dispositivos/45/"
                }
            ]
        };



        it('FETCH_DISPOSITIVOS_SUCCEEDED after successfuly fetching Dispositivos', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        results: [
                            {
                                "id": 45,
                                "etiqueta": "dev1",
                                "descripcion": "asd",
                                "estado": true,
                                "visibilidad": false,
                                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                                "latitud": 5.3,
                                "longitud": 2.4,
                                "users_enabled": [
                                    7
                                ],
                                "url": "http://127.0.0.1:8000/api/dispositivos/45/"
                            }
                        ]
                    }
                });
            });

            const expectedActions = [
                { type: types.FETCH_DISPOSITIVOS_STARTED },
                { type: types.FETCH_DISPOSITIVOS_SUCCEEDED, payload: responseMock.results },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchDispositivos(token)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });

        });



        it('FETCH_DISPOSITIVOS_FAILED after failed fetching listo of dispositivos', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.FETCH_DISPOSITIVOS_STARTED },
                { type: types.FETCH_DISPOSITIVOS_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchDispositivos(token)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });







    describe('fetchDispositivo(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const dispositivoID = '45';

        const responseMock = {
            data: {
                "id": 45,
                "etiqueta": "dev1",
                "descripcion": "asd",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 5.3,
                "longitud": 2.4,
                "users_enabled": [
                    7
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/45/"
            }
        }



        it('FETCH_DISPOSITIVO_SUCCEEDED after successfuly fetching Dispositivo', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        "id": 45,
                        "etiqueta": "dev1",
                        "descripcion": "asd",
                        "estado": true,
                        "visibilidad": false,
                        "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                        "ultima_actividad": "2006-10-25T14:30:59-03:00",
                        "latitud": 5.3,
                        "longitud": 2.4,
                        "users_enabled": [
                            7
                        ],
                        "url": "http://127.0.0.1:8000/api/dispositivos/45/"
                    }
                });
            });

            const expectedActions = [
                { type: types.FETCH_DISPOSITIVO_STARTED },
                { type: types.FETCH_DISPOSITIVO_SUCCEEDED, payload: responseMock.data },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchDispositivo(token, dispositivoID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });



        it('FETCH_DISPOSITIVO_FAILED after failed fetching Datos', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.FETCH_DISPOSITIVO_STARTED },
                { type: types.FETCH_DISPOSITIVO_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchDispositivo(token, dispositivoID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });


    describe('handleSubmit(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const requestType = 'post';
        const dispositivo = {
            "id": 45,
            "etiqueta": "dev1",
            "descripcion": "asd",
            "estado": true,
            "visibilidad": false,
            "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
            "ultima_actividad": "2006-10-25T14:30:59-03:00",
            "latitud": 5.3,
            "longitud": 2.4,
            "users_enabled": [
                7
            ],
            "url": "http://127.0.0.1:8000/api/dispositivos/45/"
        };
        const postObj = {
            etiqueta: 'etiqueta',
            descripcion: 'descripcion',
            estado: true,
            latitud: '28.3',
            longitud: '30.1',
            users_enabled: [7]
        }

        const responseMock = {
            data: {
                "id": 45,
                "etiqueta": "dev1",
                "descripcion": "asd",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 5.3,
                "longitud": 2.4,
                "users_enabled": [
                    7
                ],
                "url": "http://127.0.0.1:8000/api/dispositivos/45/"
            }
        }


        it('CREATE_DISPOSITIVO_STARTED after successfuly POST Dispositivo', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 201,
                    response: {
                        "id": 45,
                        "etiqueta": "dev1",
                        "descripcion": "asd",
                        "estado": true,
                        "visibilidad": false,
                        "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                        "ultima_actividad": "2006-10-25T14:30:59-03:00",
                        "latitud": 5.3,
                        "longitud": 2.4,
                        "users_enabled": [
                            7
                        ],
                        "url": "http://127.0.0.1:8000/api/dispositivos/45/"
                    }
                });
            });

            const expectedActions = [
                { type: types.CREATE_DISPOSITIVO_STARTED },
                { type: types.CREATE_DISPOSITIVO_SUCCEEDED, payload: responseMock.data },
            ];

            const store = mockStore({ payload: {} });

            const requestType = 'post';
            return store.dispatch(actions.handleSubmit(token, postObj, requestType, dispositivo)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });



        it('CREATE_DISPOSITIVO_FAILED after trying to POST new Dispositivo', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.CREATE_DISPOSITIVO_STARTED },
                { type: types.CREATE_DISPOSITIVO_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });
            const requestType = 'post';
            return store.dispatch(actions.handleSubmit(token, postObj, requestType, dispositivo)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });


        it('UPDATE_DISPOSITIVO_STARTED after successfuly PUT Dispositivo', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        "id": 45,
                        "etiqueta": "dev1",
                        "descripcion": "asd",
                        "estado": true,
                        "visibilidad": false,
                        "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                        "ultima_actividad": "2006-10-25T14:30:59-03:00",
                        "latitud": 5.3,
                        "longitud": 2.4,
                        "users_enabled": [
                            7
                        ],
                        "url": "http://127.0.0.1:8000/api/dispositivos/45/"
                    }
                });
            });

            const expectedActions = [
                { type: types.UPDATE_DISPOSITIVO_STARTED },
                { type: types.UPDATE_DISPOSITIVO_SUCCEEDED, payload: responseMock.data },
            ];

            const store = mockStore({ payload: {} });

            const requestType = 'put';
            return store.dispatch(actions.handleSubmit(token, postObj, requestType, dispositivo)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('UPDATE_DISPOSITIVO_FAILED after trying to PUT  Dispositivo', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.UPDATE_DISPOSITIVO_STARTED },
                { type: types.UPDATE_DISPOSITIVO_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });
            const requestType = 'put';
            return store.dispatch(actions.handleSubmit(token, postObj, requestType, dispositivo)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });


        it('UPDATE_DISPOSITIVO_FAILED after trying to PUT  Dispositivo: Metodo invalido', () => {

            const expectedActions = [
                { type: types.UPDATE_DISPOSITIVO_FAILED, error: "Error al procesar la solicitud - Metodo de la Request inválido" },
            ];

            const store = mockStore({ payload: {} });
            const requestType = 'INVALID_METHOD';
            store.dispatch(actions.handleSubmit(token, postObj, requestType, dispositivo));
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);

        });

    });

    describe('handleDeleteDispositivo(): ', () => {
        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const dispositivoID = '45';

        it('DELETE_DISPOSITIVO_SUCCEDED after trying to Delete  Dispositivo', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 204,
                    response: { message: "No content" },
                });
            });



            const expectedActions = [
                { type: types.DELETE_DISPOSITIVO_STARTED },
                { type: types.DELETE_DISPOSITIVO_SUCCEEDED, payload: dispositivoID },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.handleDeleteDispositivo(token, dispositivoID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('DELETE_DISPOSITIVO_FAILED after trying to Delete  Dispositivo', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.DELETE_DISPOSITIVO_STARTED },
                { type: types.DELETE_DISPOSITIVO_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.handleDeleteDispositivo(token, dispositivoID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    })
});