import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios';


import * as actions from '../../store/actions/variables';
import * as types from '../../store/actions/actionTypes'

import expect from 'expect' // You can use any testing library

// for some reason i need to do this to fix those reducer keys undefined errors..
// jest.mock('../../store/configureStore.js')


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Test from redux actions: /store/actions/variables.jsx', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });

    describe('fetchVariables(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const dispositivoID = '45';

        const responseMock = {
            results: [
                {
                    "id": 63,
                    "nombre": "VAR1",
                    "descripcion": "tu variable",
                    "etiqueta": "var1",
                    "unidad": "farenheti",
                    "valor_min_permitido": "-5.000",
                    "valor_max_permitido": "98.000",
                    "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                    "dispositivo": 45,
                    "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
                }
            ]
        };



        it('FETCH_VARIABLES_SUCCEEDED after successfuly fetching Variables', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        results: [
                            {
                                "id": 63,
                                "nombre": "VAR1",
                                "descripcion": "tu variable",
                                "etiqueta": "var1",
                                "unidad": "farenheti",
                                "valor_min_permitido": "-5.000",
                                "valor_max_permitido": "98.000",
                                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                                "dispositivo": 45,
                                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
                            }
                        ]
                    }
                });
            });

            const expectedActions = [
                { type: types.FETCH_VARIABLES_STARTED },
                { type: types.FETCH_VARIABLES_SUCCEEDED, payload: responseMock.results },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchVariables(token, dispositivoID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });

        });



        it('FETCH_VARIABLES_FAILED after failed fetching list of variables', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.FETCH_VARIABLES_STARTED },
                { type: types.FETCH_VARIABLES_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchVariables(token, dispositivoID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('fetchVariable(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const dispositivoID = '45';
        const variableID = '45';

        const responseMock = {
            data: {
                "id": 63,
                "nombre": "VAR1",
                "descripcion": "tu variable",
                "etiqueta": "var1",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            }
        }



        it('FETCH_VARIABLE_SUCCEEDED after successfuly fetching Variable', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        "id": 63,
                        "nombre": "VAR1",
                        "descripcion": "tu variable",
                        "etiqueta": "var1",
                        "unidad": "farenheti",
                        "valor_min_permitido": "-5.000",
                        "valor_max_permitido": "98.000",
                        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                        "dispositivo": 45,
                        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
                    }
                });
            });

            const expectedActions = [
                { type: types.FETCH_VARIABLE_STARTED },
                { type: types.FETCH_VARIABLE_SUCCEEDED, payload: responseMock.data },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchVariable(token, dispositivoID, variableID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });



        it('FETCH_VARIABLE_FAILED after failed fetching Datos', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.FETCH_VARIABLE_STARTED },
                { type: types.FETCH_VARIABLE_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchVariable(token, dispositivoID, variableID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('handleSubmit(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const data = {
            requestType: 'post',
            dispositivoID: 45,
            variableID: 63,
            postObj: {
                "nombre": "VAR1",
                "descripcion": "tu variable",
                "etiqueta": "var1",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "dispositivo": 45,
            }
        }

        const responseMock = {
            data: {
                "id": 63,
                "nombre": "VAR1",
                "descripcion": "tu variable",
                "etiqueta": "var1",
                "unidad": "farenheti",
                "valor_min_permitido": "-5.000",
                "valor_max_permitido": "98.000",
                "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                "dispositivo": 45,
                "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
            }
        }


        it('CREATE_VARIABLE_STARTED after successfuly POST Variable', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 201,
                    response: {
                        "id": 63,
                        "nombre": "VAR1",
                        "descripcion": "tu variable",
                        "etiqueta": "var1",
                        "unidad": "farenheti",
                        "valor_min_permitido": "-5.000",
                        "valor_max_permitido": "98.000",
                        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                        "dispositivo": 45,
                        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
                    }
                });
            });

            const expectedActions = [
                { type: types.CREATE_VARIABLE_STARTED },
                { type: types.CREATE_VARIABLE_SUCCEEDED, payload: responseMock.data },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.handleSubmit(token, data)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });


        it('CREATE_VARIABLE_FAILED after trying to POST new Variable', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.CREATE_VARIABLE_STARTED },
                { type: types.CREATE_VARIABLE_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });
            data.requestType = 'post';
            return store.dispatch(actions.handleSubmit(token, data)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });



        it('UPDATE_VARIABLE_STARTED after successfuly PUT Dispositivo', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        "id": 63,
                        "nombre": "VAR1",
                        "descripcion": "tu variable",
                        "etiqueta": "var1",
                        "unidad": "farenheti",
                        "valor_min_permitido": "-5.000",
                        "valor_max_permitido": "98.000",
                        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
                        "dispositivo": 45,
                        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
                    }
                });
            });

            const expectedActions = [
                { type: types.UPDATE_VARIABLE_STARTED },
                { type: types.UPDATE_VARIABLE_SUCCEEDED, payload: responseMock.data },
            ];

            const store = mockStore({ payload: {} });
            data.requestType = 'put';

            return store.dispatch(actions.handleSubmit(token, data)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('UPDATE_VARIABLE_FAILED after trying to PUT  Variable', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.UPDATE_VARIABLE_STARTED },
                { type: types.UPDATE_VARIABLE_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });
            data.requestType = 'put';
            return store.dispatch(actions.handleSubmit(token, data)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });


        it('UPDATE_VARIABLE_FAILED after trying to PUT Variable: Metodo invalido', () => {

            const expectedActions = [
                { type: types.CREATE_VARIABLE_FAILED, error: "Error: La solicitud no se pudo procesar. Tipo: submit variable" },
            ];

            const store = mockStore({ payload: {} });
            data.requestType = 'INVALID_METHOD';
            store.dispatch(actions.handleSubmit(token, data));
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);

        });

        it('CREATE_VARIABLE_FAILED after trying to POST Variable: dispositivoID invalido', () => {

            const expectedActions = [
                { type: types.CREATE_VARIABLE_STARTED },
                { type: types.CREATE_VARIABLE_FAILED, error: "Error al procesar la solicitud- El dispositivo no pudo ser creado: dispositivoID inválido." },
            ];

            const store = mockStore({ payload: {} });
            data.requestType = 'post';
            data.dispositivoID = undefined;
            store.dispatch(actions.handleSubmit(token, data));
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);

        });

        it('UPDATE_VARIABLE_FAILED after trying to POST Variable: dispositivoID invalido', () => {

            const expectedActions = [
                { type: types.UPDATE_VARIABLE_STARTED },
                { type: types.UPDATE_VARIABLE_FAILED, error: "Error al procesar la solicitud- El dispositivo no pudo ser actualizado: dispositivoID inválido." },
            ];

            const store = mockStore({ payload: {} });
            data.requestType = 'put';
            data.dispositivoID = undefined;
            data.variableID = undefined;
            store.dispatch(actions.handleSubmit(token, data));
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);

        });
    });

    describe('handleDeleteVariable(): ', () => {
        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const dispositivoID = '45';
        const variableID = '63';

        it('DELETE_VARIABLE_SUCCEDED after trying to Delete  Variable', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 204,
                    response: { message: "No content" },
                });
            });

            const expectedActions = [
                { type: types.DELETE_VARIABLE_STARTED },
                { type: types.DELETE_VARIABLE_SUCCEEDED, payload: variableID },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.handleDeleteVariable(token, dispositivoID, variableID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('DELETE_VARIABLE_FAILED after trying to Delete Variable', () => {
            const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
            const dispositivoID = '45';
            const variableID = '63';

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.DELETE_VARIABLE_STARTED },
                { type: types.DELETE_VARIABLE_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.handleDeleteVariable(token, dispositivoID, variableID)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('DELETE_VARIABLE_FAILED after trying to Delete Variable: dispositivoID o variableID inválidos', () => {
            const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
            const dispositivoID = undefined;
            const variableID = undefined;

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.DELETE_VARIABLE_STARTED },
                { type: types.DELETE_VARIABLE_FAILED, error: "Error al procesar la solicitud: La variable no pudo ser eliminada: dispositivoID o variableID inválidos." },
            ];

            const store = mockStore({ payload: {} });

            store.dispatch(actions.handleDeleteVariable(token, dispositivoID, variableID));
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);
        });

    })

});