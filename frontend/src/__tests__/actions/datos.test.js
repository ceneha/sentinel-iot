import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios';


import * as actions from '../../store/actions/datos';
import * as types from '../../store/actions/actionTypes'

import expect from 'expect' // You can use any testing library

// for some reason i need to do this to fix those reducer keys undefined errors..
// jest.mock('../../store/configureStore.js')


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Test from redux actions: /store/actions/datos.jsx', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });

    // afterEach(() => {
    //     fetchMock.restore()
    // })


    // it('should create an action when fetching Datos start.', () => {
    //     const expectedAction = {
    //         type: types.FETCH_DATOS_STARTED
    //     }

    //     expect(actions.fetchDatosStarted()).toEqual(expectedAction)
    // });

    // it('should create an action when fetching Datos Succeded.', () => {
    //     const payload = [
    //         {
    //             "id": 88,
    //             "timestamp": "2006-10-25T14:30:59-03:00",
    //             "timestamp_servidor": "2019-11-30T10:58:08.524936-03:00",
    //             "valor": "0.500",
    //             "variable": 63,
    //             "dispositivo": 45
    //         }];

    //     const expectedAction = {
    //         type: types.FETCH_DATOS_SUCCEEDED,
    //         payload: payload
    //     }

    //     expect(actions.fetchDatosSucceeded(payload)).toEqual(expectedAction)
    // });

    // it('should create an action when fetching Datos failed.', () => {
    //     const error = 'Error Msg'
    //     const expectedAction = {
    //         type: types.FETCH_DATOS_FAILED,
    //         error: error
    //     }

    //     expect(actions.fetchDatosFailed(error)).toEqual(expectedAction)
    // });





    // it('should create an action when fetching Datos Historicos starts.', () => {
    //     const expectedAction = {
    //         type: types.FETCH_DATOS_HISTORICOS_STARTED,
    //     }

    //     expect(actions.fetchDatosHistoricosStarted()).toEqual(expectedAction);
    // });

    // it('should create an action when fetching Datos Historicos succeded.', () => {
    //     const payload = [
    //         {
    //             "id": 88,
    //             "timestamp": "2006-10-25T14:30:59-03:00",
    //             "timestamp_servidor": "2019-11-30T10:58:08.524936-03:00",
    //             "valor": "0.500",
    //             "variable": 63,
    //             "dispositivo": 45
    //         }];

    //     const expectedAction = {
    //         type: types.FETCH_DATOS_HISTORICOS_SUCCEEDED,
    //         payload: payload
    //     }

    //     expect(actions.fetchDatosHistoricosSucceeded(payload)).toEqual(expectedAction);
    // });


    // it('should create an action when fetching Datos Historicos fails.', () => {
    //     const error = "error msg";
    //     const expectedAction = {
    //         type: types.FETCH_DATOS_HISTORICOS_FAILED,
    //         error: error
    //     }

    //     expect(actions.fetchDatosHistoricosFailed(error)).toEqual(expectedAction);
    // });


    it('should create an action when reseting datos historicos from store.', () => {
        const expectedAction = {
            type: types.RESET_DATOS_HISTORICOS
        }

        expect(actions.resetDatosHistoricos()).toEqual(expectedAction);
    });


    describe('fetchDatos(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";

        const responseMock = {
            results: [
                {
                    "id": 88,
                    "timestamp": "2006-10-25T14:30:59-03:00",
                    "timestamp_servidor": "2019-11-30T10:58:08.524936-03:00",
                    "valor": "0.500",
                    "variable": 63,
                    "dispositivo": 45
                }
            ]
        };



        it('FETCH_DATOS_SUCCEEDED after successfuly fetching Datos', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        results: [
                            {
                                "id": 88,
                                "timestamp": "2006-10-25T14:30:59-03:00",
                                "timestamp_servidor": "2019-11-30T10:58:08.524936-03:00",
                                "valor": "0.500",
                                "variable": 63,
                                "dispositivo": 45
                            }
                        ]
                    },
                });
            });

            const expectedActions = [
                { type: types.FETCH_DATOS_STARTED },
                { type: types.FETCH_DATOS_SUCCEEDED, payload: responseMock.results },
            ];



            const store = mockStore({ payload: {} })

            return store.dispatch(actions.fetchDatos(token)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });

        });



        it('FETCH_DATOS_FAILED after trying to fetch Datos', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.FETCH_DATOS_STARTED },
                { type: types.FETCH_DATOS_FAILED, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.fetchDatos(token)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });


        });

        it('FETCH_DATOS_FAILED after trying to fetch Datos: Empty token', () => {

            const expectedActions = [
                { type: types.FETCH_DATOS_STARTED },
                { type: types.FETCH_DATOS_FAILED, error: "Empty Token when trying to fetch Datos." },
            ];

            const store = mockStore({ payload: {} })

            store.dispatch(actions.fetchDatos(undefined));

            expect(store.getActions()).toEqual(expectedActions);

        });

    });



    describe('fetchDatosHistoricos(): ', () => {

        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const parameters = {
            variableID: 63,
            start_date: '2019-12-09%2017:47:16',
            end_date: '019-12-11%2017:47:16'
        }


        const responseMock = {
            data: [
                {
                    "id": 88,
                    "timestamp": "2006-10-25T14:30:59-03:00",
                    "timestamp_servidor": "2019-11-30T10:58:08.524936-03:00",
                    "valor": "0.500",
                    "variable": 63,
                    "dispositivo": 45
                }
            ]
        };



        it('FETCH_DATOS_HISTORICOS_SUCCEEDED after successfuly fetching Datos', () => {
            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: [
                        {
                            "id": 88,
                            "timestamp": "2006-10-25T14:30:59-03:00",
                            "timestamp_servidor": "2019-11-30T10:58:08.524936-03:00",
                            "valor": "0.500",
                            "variable": 63,
                            "dispositivo": 45
                        }
                    ]
                    ,
                });
            });

            const expectedActions = [
                { type: types.FETCH_DATOS_HISTORICOS_STARTED },
                { type: types.FETCH_DATOS_HISTORICOS_SUCCEEDED, payload: responseMock.data },
            ];


            const store = mockStore({ payload: {} })

            return store.dispatch(actions.fetchDatosHistoricos(token, parameters)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });

        });



        it('FETCH_DATOS_HISTORICOS_FAILED after trying to fetch Datos', () => {


            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.FETCH_DATOS_HISTORICOS_STARTED },
                { type: types.FETCH_DATOS_HISTORICOS_FAILED, error: "Request failed with status code 301" },
            ];



            const store = mockStore({ payload: {} })

            return store.dispatch(actions.fetchDatosHistoricos(token, parameters)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });


        });

        it('FETCH_DATOS_HISTORICOS_FAILED after trying to fetch Datos: Empty token', () => {

            const expectedActions = [
                { type: types.FETCH_DATOS_HISTORICOS_STARTED },
                { type: types.FETCH_DATOS_HISTORICOS_FAILED, error: "Empty Token when trying to fetch Datos Historicos." },
            ];

            const store = mockStore({ payload: {} })

            store.dispatch(actions.fetchDatosHistoricos(undefined, parameters));

            expect(store.getActions()).toEqual(expectedActions);

        });

    });





});