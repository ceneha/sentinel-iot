import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios';


import * as actions from '../../store/actions/auth';
import * as types from '../../store/actions/actionTypes'

import expect from 'expect' // You can use any testing library

// for some reason i need to do this to fix those reducer keys undefined errors..
// jest.mock('../../store/configureStore.js')


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Test from redux actions: /store/actions/auth.jsx', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });

    it('authStart()', () => {
        const expectedAction = {
            type: types.AUTH_START,
        }

        expect(actions.authStart()).toEqual(expectedAction)
    });

    it('authSuccess()', () => {
        const token = "Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc";
        const user = "fulanito";

        const expectedAction = {
            type: types.AUTH_SUCCESS, token: token, user: user
        }

        expect(actions.authSuccess(user, token)).toEqual(expectedAction)
    });

    it('authFail()', () => {
        const error = "Fallo la autenticacion";

        const expectedAction = {
            type: types.AUTH_FAIL, error: error
        }

        expect(actions.authFail(error)).toEqual(expectedAction)
    });

    it('authLogout()', () => {
        const expectedAction = {
            type: types.AUTH_LOGOUT
        }

        expect(actions.authLogout()).toEqual(expectedAction)
    });

    it('reset()', () => {
        const expectedAction = {
            type: types.RESET
        }

        expect(actions.reset()).toEqual(expectedAction)
    });



    describe('authLogin(): ', () => {

        const username = "nahu";
        const password = '12344566-';

        const responseMock = {
            "key": "2e01a4c1b160a193876fd77dab08b512f5ccbffc",
            "user": {
                "username": "nahu",
                "email": "nahuboutet@gmail.com",
                "is_staff": true
            }
        };

        it('AUTH_SUCCESS after successfuly login', () => {

            moxios.wait(() => {
                let request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 200,
                    response: {
                        "key": "2e01a4c1b160a193876fd77dab08b512f5ccbffc",
                        "user": {
                            "username": "nahu",
                            "email": "nahuboutet@gmail.com",
                            "is_staff": true
                        }
                    }
                });
            });

            const expectedActions = [
                { type: types.AUTH_START },
                { type: types.AUTH_SUCCESS, token: responseMock.key, user: responseMock.user },
                { type: "RRP_ADD_PERMISSION", roles: ["admin"] },
                { type: "@@router/CALL_HISTORY_METHOD", payload: { args: ["/"], method: "push", } }
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.authLogin(username, password)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });

        });


        it('AUTH_FAIL after tring to login 1', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.AUTH_START },
                { type: types.AUTH_FAIL, error: "Request failed with status code 301 - " },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.authLogin(username, password)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

    });


    describe('logout()', () => {
        it('AUTH_LOGOUT succesfuul', () => {

            const expectedActions = [
                { type: types.AUTH_LOGOUT },
                { type: 'RESET' },
                { type: 'RRP_CLEAR' },
                { type: '@@router/CALL_HISTORY_METHOD', payload: { args: ["/"], method: "push", } }
            ];

            const store = mockStore({ payload: {} });
            store.dispatch(actions.logout());
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions);
        });
    });


    describe('authSignup(): ', () => {

        const username = "nahu";
        const password1 = '12344566-';
        const password2 = '12344566-';
        const email = "email@gmail.com";

        const responseMock = {
            "key": "606f5fc89b80a0dfbe7a03b2a320d09cbe460038",
            "user": {
                "username": "sabalerito",
                "email": "sabalerito@gmail.com",
                "is_staff": false
            }
        };

        it('AUTH_SIGNUP after successfuly sign up', () => {

            moxios.wait(() => {
                let request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 201,
                    response: {
                        "key": "606f5fc89b80a0dfbe7a03b2a320d09cbe460038",
                        "user": {
                            "username": "sabalerito",
                            "email": "sabalerito@gmail.com",
                            "is_staff": false
                        }
                    }
                });
            });

            const expectedActions = [
                { type: types.AUTH_START },
                { type: types.AUTH_SUCCESS, token: responseMock.key, user: responseMock.user },
                { type: 'RRP_ADD_PERMISSION', roles: ['visitor'] },
                { type: "@@router/CALL_HISTORY_METHOD", payload: { args: ["/"], method: "push", } }
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.authSignup(username, email, password1, password2)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });

        });


        it('AUTH_FAIL fail after trying  to sign up', () => {

            moxios.wait(() => {
                const request = moxios.requests.mostRecent();
                request.respondWith({
                    status: 301,
                    response: { message: "Request failed with status code 301" },
                });
            });

            const expectedActions = [
                { type: types.AUTH_START },
                { type: types.AUTH_FAIL, error: "Request failed with status code 301" },
            ];

            const store = mockStore({ payload: {} });

            return store.dispatch(actions.authSignup(username, email, password1, password2)).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

    });


});

