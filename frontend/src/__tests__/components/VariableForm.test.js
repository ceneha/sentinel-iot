
import React from 'react'
// import "jsdom-global/register"
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import VariableFormConnected, { VariableForm } from '../../components/VariableForm'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';

import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })



// https://binarapps.com/blog/test-ajax-calls-in-react-component-lifecycle/
// https://github.com/ctimmerm/axios-mock-adapter/issues/58
// https://stackoverflow.com/questions/49413937/why-typeerror-axios-create-is-not-a-function-when-testing-axios-get

// describe('>>> Signup  -- ISOLATED COMPONENT - Shallow Render REACT COMPONENTS', () => {
//     let enzymeWrapper
//     const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';
//     const dispositivoID = '43';

//     beforeEach(() => {
//         enzymeWrapper = mount(<SignupConnected onAuth={jest.fn()} />)
//     })

//     it('+++ render the DUMB component', () => {
//         chaiExpect(enzymeWrapper.length).equal(1)
//     });

//     it('+++ contains Variables Title', () => {
//         // expect(wrapper.find('input#dynamic_form_item_timestampColumnName.ant.input').prop('value')).toEqual(output)
//         chaiExpect(enzymeWrapper.contains(<Title level={4}>Variables</Title>)).to.be.equal(true)
//     });

// });


const mockStore = configureMockStore([thunk])
const mockReduxState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r',
        loading: false,
        error: "error"
    },
}

// // https://redux.js.org/recipes/writing-tests/
// // https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
// jest.unmock('axios');
describe('<VariableFormConnected/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {



    // const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';

    // const props = {
    //     form: {
    //         validateFieldsAndScroll: jest.fn(opts => c => c),
    //         getFieldDecorator: jest.fn(opts => c => c),
    //         handleConfirmBlur: jest.fn(opts => c => c),
    //         validateToNextPassword: jest.fn(opts => c => c),
    //         getFieldValue: jest.fn(opts => c => c)
    //     }
    // }


    const props = {
        dispositivoID: '45'
    }
    // state: {
    //     authentication: {
    //         token: 'Token 5skdhai89399129bdakjsd91fdgasj84r'
    //     }
    // }


    let store = mockStore(mockReduxState)

    let enzymeWrapper = mount(
        <Provider store={store}>
            <VariableFormConnected {...props} onSubmit={jest.fn()} _eventListeners={jest.fn()} />
        </Provider>
    )

    it('+++ render the DUMB component', () => {
        //console.log(enzymeWrapper.debug())
        chaiExpect(enzymeWrapper.length).equal(1)
    });

    it('+++ render the DUMB component correctly', () => {
        //   console.log(enzymeWrapper.debug())
        // const newFunction = jest.fn();

        let wrapper = mount(
            <Provider store={store}>
                <VariableFormConnected {...props} onSubmit={jest.fn()} _eventListeners={jest.fn()} />
            </Provider>
        )

        expect(wrapper).toMatchSnapshot();
    })



    it('+++ Submit Variable form correctly', () => {
        //   console.log(enzymeWrapper.debug())
        const onSubmit = jest.fn();

        let wrapper = mount(
            <Provider store={store}>
                <VariableFormConnected {...props} onSubmit={jest.fn()} _eventListeners={jest.fn()} />
            </Provider>
        )

        wrapper.find('#btnShowDrawer').at(0).simulate('click');

        wrapper.find('input#nombre.ant-input').simulate('change', { target: { value: 'nombre' } });
        wrapper.find('input#etiqueta.ant-input').simulate('change', { target: { value: 'etiqueta' } });
        wrapper.find('input#unidad.ant-input').simulate('change', { target: { value: 'unidad' } });
        wrapper.find('input#valMin.ant-input').simulate('change', { target: { value: '8' } });
        wrapper.find('input#valMax.ant-input').simulate('change', { target: { value: '85' } });
        wrapper.find('textarea#descripcion.ant-input').simulate('change', { target: { value: 'descripcion' } });

        const mockedEvent = {
            preventDefault: jest.fn(),
            target: {
                elements: {
                    nombre: { value: 'nombre' },
                    etiqueta: { value: 'descripcion' },
                    descripcion: { value: 'descrip' },
                    unidad: { value: 'C°' },
                    valMin: { value: '8' },
                    valMax: { value: '55' },
                }
            }
        }

        wrapper.find("#submitVariableBtn", { preventDefault() { return { err: false, value: false } } }).at(0).simulate('submit', mockedEvent);

        setTimeout(() => {
            expect(onSubmit.mock.calls.length).to.equal(1);
        }, 0);

    });


});

