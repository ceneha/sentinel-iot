
import React from 'react'
// import "jsdom-global/register"
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import DataForm from '../../components/UploadDataForm'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';

import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })


const mockStore = configureMockStore([thunk])
const mockReduxState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r',
        loading: false,
        error: "error"
    },
    entities: {
        dispositivos: {
            data: mocksDispositivos.dispositivosListMock
        },
        variables: {
            data: mocksVariables.variablesListMock
        },
        datos: {
            historicos: {
                data: {
                    0: { id: 5, timestamp: '2019-05-05 10:10:10', valor: '25' },
                }
            }
        }
    }
}

// // https://redux.js.org/recipes/writing-tests/
// // https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
// jest.unmock('axios');
describe('<DataFormConnected/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {


    const props = {
        token: "Token 5skdhai89399129bdakjsd91fdgasj84r",
        match: {
            params: {
                variableID: '32'
            }
        },
        unmountMe: jest.fn(),
        uploadedData: [
            { "timestamp": "2019-10-08T12:10:03Z", "62": 58.5 },
            { "timestamp": "2019-10-08T12:10:03Z", "54": 60.3 }
        ]
    };

    // form: {
    //     validateFields: jest.fn(opts => c => c)
    // }


    //let store = mockStore(mockReduxState)

    let enzymeWrapper = shallow(
        <DataForm {...props} _eventListeners={jest.fn()} />
    )

    // it('+++ render the DUMB component', () => {
    //     //console.log(enzymeWrapper.debug())
    //     chaiExpect(enzymeWrapper.length).equal(1)
    // });

    // it('+++ render the DUMB component correctly', () => {

    //     let wrapper = shallow(
    //         <DataForm {...props} _eventListeners={jest.fn()} />
    //     )
    //     expect(wrapper).toMatchSnapshot();
    // });



    it('+++ Submit Dispositivo form correctly', () => {

        const onSubmit = jest.fn();

        var fieldsValues = [];
        fieldsValues['range-time-picker'] = '2019-05-05 10:10:10';

        // const form = {
        //     getFieldDecorator: jest.fn(opts => c => c),
        //     validateFields: jest.fn((err, fieldsValues) => { err: "err", fieldsValues })
        // }

        let wrapper = shallow(
            <DataForm {...props} permissions='admin' _eventListeners={jest.fn()} />
        )
        console.log(wrapper.debug())
        // wrapper.find('input#ant-calendar-range-picker-input').at(0).simulate('change', { target: { value: '2019-02-02 10:10:10' } });
        // wrapper.find('input#ant-calendar-range-picker-input').at(1).simulate('change', { target: { value: '2019-02-02 10:10:10' } });

        //     const mockedEvent = {
        //         preventDefault: jest.fn(),
        //         target: {
        //             elements: {
        //                 // etiqueta: { value: 'descripcion' },
        //                 // descripcion: { value: 'descrip' },
        //                 // latitud: { value: '8' },
        //                 // longitud: { value: '8' },
        //                 // valMax: { value: '55' },
        //             }
        //         }
        //     }

        //     wrapper.find("#btnAceptarUploadData", { preventDefault() { return { err: false, value: false } } }).at(0).simulate('submit', mockedEvent);

        //     setTimeout(() => {
        //         expect(onSubmit.mock.calls.length).to.equal(1);
        //     }, 0);

    });


});

