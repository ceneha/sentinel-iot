
import React from 'react'
// import "jsdom-global/register"
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import VariablesTableConnected, { VariablesTable } from '../../components/VariablesTable'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';

import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })



// https://binarapps.com/blog/test-ajax-calls-in-react-component-lifecycle/
// https://github.com/ctimmerm/axios-mock-adapter/issues/58
// https://stackoverflow.com/questions/49413937/why-typeerror-axios-create-is-not-a-function-when-testing-axios-get

// describe('>>> Signup  -- ISOLATED COMPONENT - Shallow Render REACT COMPONENTS', () => {
//     let enzymeWrapper
//     const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';
//     const dispositivoID = '43';

//     beforeEach(() => {
//         enzymeWrapper = mount(<SignupConnected onAuth={jest.fn()} />)
//     })

//     it('+++ render the DUMB component', () => {
//         chaiExpect(enzymeWrapper.length).equal(1)
//     });

//     it('+++ contains Variables Title', () => {
//         // expect(wrapper.find('input#dynamic_form_item_timestampColumnName.ant.input').prop('value')).toEqual(output)
//         chaiExpect(enzymeWrapper.contains(<Title level={4}>Variables</Title>)).to.be.equal(true)
//     });

// });


const mockStore = configureMockStore([thunk])
const mockReduxState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r',
        loading: false,
        error: "error"
    },
    permissions: ["admin"],
    entities: {
        dispositivos: {
            data: mocksDispositivos.DispositivosArrayOfObjectsMock
        },
        variables: {
            data: mocksVariables.VariablesArrayOfObjectsMock
        }
    },
}

// // https://redux.js.org/recipes/writing-tests/
// // https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
// jest.unmock('axios');
describe('<VariablesTableConnected/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {




    // const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';

    // const props = {
    //     form: {
    //         validateFieldsAndScroll: jest.fn(opts => c => c),
    //         getFieldDecorator: jest.fn(opts => c => c),
    //         handleConfirmBlur: jest.fn(opts => c => c),
    //         validateToNextPassword: jest.fn(opts => c => c),
    //         getFieldValue: jest.fn(opts => c => c)
    //     }
    // }


    const props = {
        token: 'Token asjhdhasdh9h9asddnoasnd',
        dispositivoID: '45',
        variables: mocksVariables.VariablesArrayOfObjectsMock,

        onDelete: jest.fn(),
        onRedirect: jest.fn(),
        onFetchVariables: jest.fn(),
    }


    let store = mockStore(mockReduxState)



    it('+++ render the DUMB component', () => {
        let wrapper = mount(
            <Provider store={store}>
                <VariablesTableConnected {...props} />
            </Provider>
        )

        chaiExpect(wrapper.length).equal(1)
    });

    it('+++ render the DUMB component correctly', () => {

        let wrapper = mount(
            <Provider store={store}>
                <VariablesTableConnected {...props} />
            </Provider>
        )

        expect(wrapper).toMatchSnapshot();
    })




    it('+++ Click on ver variables form correctly', () => {

        let wrapper = mount(
            <Provider store={store} >
                <VariablesTableConnected {...props} />
            </Provider>
        );

        wrapper.setProps({ token: 'Token adgkasdbu279rfblsdbhlajd' })

        //Simulate click on "Ver variable"
        wrapper.find('#ver-variable-btn').at(0).simulate('click')
        const urlVariable = "/dispositivos/45/variables/63/";
        const expectedActions = [{ type: '@@router/CALL_HISTORY_METHOD', payload: { method: 'push', args: [urlVariable] } }];
        expect(store.getActions()).toEqual(expectedActions);

    })


    it('+++ Click on delete variables form correctly', () => {

        let wrapper = mount(
            <Provider store={store} >
                <VariablesTableConnected {...props} />
            </Provider>
        );

        //Simulate click on "eliminar variable"
        wrapper.find('#eliminar-variable-btn').at(0).simulate('click')
        wrapper.find('button.ant-btn.ant-btn-primary.ant-btn-sm').at(0).simulate('click');

        const expectedActions = [
            { payload: { args: ["/dispositivos/45/variables/63/",], "method": "push" }, type: "@@router/CALL_HISTORY_METHOD" },
            { type: 'DELETE_VARIABLE_STARTED' },];
        expect(store.getActions()).toEqual(expectedActions);
    })


    it('+++ Click on cancel delete variables form correctly', () => {

        let wrapper = mount(
            <Provider store={store} >
                <VariablesTableConnected {...props} />
            </Provider>
        );

        //Simulate click on "eliminar variable"
        wrapper.find('#eliminar-variable-btn').at(0).simulate('click');
        wrapper.find('button.ant-btn.ant-btn-sm').at(0).simulate('click');

        const expectedActions = [
            { payload: { args: ["/dispositivos/45/variables/63/",], "method": "push" }, type: "@@router/CALL_HISTORY_METHOD" },
            { type: 'DELETE_VARIABLE_STARTED' },];
        expect(store.getActions()).toEqual(expectedActions);
    })



});
