
import React from 'react'
// import "jsdom-global/register"
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import { expect as chaiExpect } from 'chai'

import sinon from 'sinon';

import CsvUploadDatosConnected from '../../components/CsvUploadDatos'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';


jest.unmock('axios');



Enzyme.configure({ adapter: new Adapter() })


const mockStore = configureMockStore([thunk])
const mockReduxState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r'
    },
    entities: {
        dispositivos: {
            data: mocksDispositivos.DispositivosArrayOfObjectsMock
        },
        variables: {
            data: mocksVariables.VariablesArrayOfObjectsMock
        },
        // variableObj: mocksVariables.fetchedVariableSuccedeedMock
    },
    permissions: ["admin"]
}

describe('<CsvUploadDatosConnected/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {

    const props = {
        dispositivoID: '45'
    }

    let store = mockStore(mockReduxState)

    let enzymeWrapper = mount(
        <Provider store={store}>
            <CsvUploadDatosConnected {...props} onSubmit={jest.fn()} _eventListeners={jest.fn()} />
        </Provider>
    )

    it('+++ render the DUMB component', () => {
        //console.log(enzymeWrapper.debug())
        chaiExpect(enzymeWrapper.length).equal(1)
    });

    it('+++ render the DUMB component correctly', () => {

        let wrapper = mount(
            <Provider store={store}>
                <CsvUploadDatosConnected {...props} onSubmit={jest.fn()} _eventListeners={jest.fn()} />
            </Provider>
        )

        expect(wrapper).toMatchSnapshot();
    })




});

