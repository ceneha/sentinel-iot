import { reducer } from '../../store/reducers/auth';
import * as actionTypes from '../../store/actions/actionTypes';

import expect from 'expect';

describe('auth reducer', () => {

    it('should return the initial state', () => {
        expect(reducer({}, {})).toEqual({});
    });

    it('should handle AUTH_START', () => {
        const startAction = { type: actionTypes.AUTH_START };
        const endState = { error: null, loading: true };

        expect(reducer({}, startAction)).toEqual(endState);
    });

    it('should handle AUTH_FAIL', () => {

        const authFailAction = { type: actionTypes.AUTH_FAIL, error: 'erroMsg' };
        const endState = { error: 'erroMsg', loading: false };

        expect(reducer({}, authFailAction)).toEqual(endState);
    });

    it('should handle AUTH_SUCCESS', () => {
        const authSuccessAction = { type: actionTypes.AUTH_SUCCESS, token: 'Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc', user: 'user' };
        const endState = { token: 'Token 2e01a4c1b160a193876fd77dab08b512f5ccbffc', user: 'user', error: null, loading: false };

        expect(reducer({}, authSuccessAction)).toEqual(endState);
    });

    it('should handle AUTH_LOGOUT', () => {
        const authLogoutAction = { type: actionTypes.AUTH_LOGOUT };
        const endState = { token: null, user: null };

        expect(reducer({}, authLogoutAction)).toEqual(endState);
    });
});