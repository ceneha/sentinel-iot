import { reducer } from '../../store/reducers/variables';

import * as actionTypes from '../../store/actions/actionTypes';

import { updateList } from '../../store/utility';

import * as mocks from '../../mocks/variablesMocks';

import expect from 'expect';

describe('variables reducer', () => {
    let initialState = {
        data: [],     // Lista de variables
        currentId: null,    // ID de una variable seleccionada
        loading: false,
        error: false,
    }

    it('should return the initial state', () => {
        expect(reducer({}, {})).toEqual({});
    });

    it('should handle FETCH_VARIABLES_STARTED', () => {
        const fetchVariablesStartedAction = { type: actionTypes.FETCH_VARIABLES_STARTED };
        const endState = { ...initialState, loading: true };

        expect(reducer(initialState, fetchVariablesStartedAction)).toEqual(endState);
    });

    it('should handle FETCH_VARIABLES_SUCCEEDED', () => {
        const fetchVariablesSucceededAction = { type: actionTypes.FETCH_VARIABLES_SUCCEEDED, payload: mocks.variablesListMock };
        const endState = {
            data: mocks.variablesListMock,
            error: false,
            currentId: null,
            loading: false
        };

        expect(reducer({}, fetchVariablesSucceededAction)).toEqual(endState);
    });

    it('should handle FETCH_VARIABLES_FAILED', () => {
        const fetchVariablesFailedAction = { type: actionTypes.FETCH_VARIABLES_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            data: [],
            currentId: null,
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, fetchVariablesFailedAction)).toEqual(endState);
    });

    it('should handle UPDATE_VARIABLE_STARTED', () => {
        const updateVariableStartedAction = { type: actionTypes.UPDATE_VARIABLE_STARTED, };
        const endState = { ...initialState, loading: true };

        expect(reducer(initialState, updateVariableStartedAction)).toEqual(endState);
    });

    it('should handle UPDATE_VARIABLE_SUCCEEDED', () => {

        const initialState = { data: mocks.variablesListMock };
        const updateVariableSucceededAction = { type: actionTypes.UPDATE_VARIABLE_SUCCEEDED, payload: mocks.variableMock };
        const endState = {
            data: updateList(mocks.variablesListMock, mocks.variableMock),
            currentItem: { ...mocks.variableMock.id },
            loading: false
        };

        expect(reducer(initialState, updateVariableSucceededAction)).toEqual(endState);
    });

    it('should handle UPDATE_VARIABLE_FAILED', () => {

        const updateVariableFailedAction = { type: actionTypes.UPDATE_VARIABLE_FAILED, error: 'errorMsg' };
        const endState = {
            error: 'errorMsg',
            loading: false
        };

        expect(reducer({}, updateVariableFailedAction)).toEqual(endState);
    });

    it('should handle CREATE_VARIABLE_STARTED', () => {

        const createVariableStartedAction = { type: actionTypes.CREATE_VARIABLE_STARTED };
        const endState = { ...initialState, loading: true };

        expect(reducer(initialState, createVariableStartedAction)).toEqual(endState);
    });

    it('should handle CREATE_VARIABLE_SUCCEEDED', () => {
        const createDispositivoSucceededAction = { type: actionTypes.CREATE_VARIABLE_SUCCEEDED, payload: mocks.variableMock };
        const endState = { ...initialState, data: updateList(initialState.data, mocks.variableMock), loading: true };

        expect(reducer(initialState, createDispositivoSucceededAction)).toEqual(endState);
    });


    it('should handle CREATE_VARIABLE_FAILED', () => {
        const createVariableFailedAction = { type: actionTypes.CREATE_VARIABLE_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, createVariableFailedAction)).toEqual(endState);
    });


    it('should handle DELETE_VARIABLE_STARTED', () => {
        const deleteVariableStartedAction = { type: actionTypes.DELETE_VARIABLE_STARTED };
        const endState = {
            ...initialState,
            loading: true
        };

        expect(reducer(initialState, deleteVariableStartedAction)).toEqual(endState);
    });

    it('should handle DELETE_VARIABLE_SUCCEEDED', () => {
        const initialStateX = { ...initialState, data: mocks.variablesListMock };
        const deleteVariableSucceededAction = { type: actionTypes.DELETE_VARIABLE_SUCCEEDED, payload: 63 };
        const endState = {
            ...initialState,
            data: mocks.variablesListDeletedDispositivoMock,
            loading: false
        };

        expect(reducer(initialStateX, deleteVariableSucceededAction)).toEqual(endState);
    });

    it('should handle DELETE_VARIABLE_FAILED', () => {

        const deleteVariableFailedAction = { type: actionTypes.DELETE_VARIABLE_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, deleteVariableFailedAction)).toEqual(endState);
    });


    it('should handle FETCH_VARIABLE_STARTED', () => {

        const fetchVariableStartedAction = { type: actionTypes.FETCH_VARIABLE_STARTED };
        const endState = {
            ...initialState,
            loading: true
        };

        expect(reducer(initialState, fetchVariableStartedAction)).toEqual(endState);
    });

    it('should handle FETCH_VARIABLE_SUCCEEDED', () => {

        const initialStateX = { ...initialState, data: mocks.variablesListMock }
        const fetchVariableSucceededAction = { type: actionTypes.FETCH_VARIABLE_SUCCEEDED, payload: mocks.fetchedVariableSuccedeedMock };
        const endState = {
            ...initialState,
            data: updateList(mocks.variablesListMock, mocks.fetchedVariableSuccedeedMock),
            currentId: mocks.fetchedVariableSuccedeedMock.id,
            loading: false
        };

        expect(reducer(initialStateX, fetchVariableSucceededAction)).toEqual(endState);
    });


    it('should handle FETCH_VARIABLE_FAILED', () => {

        const fetchVariableFailedaction = { type: actionTypes.FETCH_VARIABLE_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            data: null,
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, fetchVariableFailedaction)).toEqual(endState);
    });


    it('should handle RESET', () => {

        const fetchVariableFailedaction = { type: actionTypes.RESET };
        const endState = { ...initialState };

        expect(reducer(initialState, fetchVariableFailedaction)).toEqual(endState);
    });

});