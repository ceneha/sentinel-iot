import { reducer } from '../../store/reducers/datos';

import * as actionTypes from '../../store/actions/actionTypes';

import * as mocks from '../../mocks/datosMocks';

import expect from 'expect';

describe('dispositivos reducer', () => {
    const initialState = {
        data: [],     // Lista de ultimos datos registrados
        loading: false,
        error: false,
    }

    it('should return the initial state', () => {
        expect(reducer({}, {})).toEqual({});
    });


    it('should handle FETCH_DATOS_STARTED', () => {
        const fetchDatosStartedAction = { type: actionTypes.FETCH_DATOS_STARTED };
        const endState = { ...initialState, loading: true };

        expect(reducer(initialState, fetchDatosStartedAction)).toEqual(endState);
    });

    it('should handle FETCH_DATOS_SUCCEEDED', () => {

        const fetchDatosSucceededAction = {
            type: actionTypes.FETCH_DATOS_SUCCEEDED,
            payload: mocks.datosListMock
        };

        const newData = mocks.datosListMock.map((item, index) => {
            return {
                date: new Date(Date.parse(item.timestamp)),
                Variable: item.valor
            };
        });

        const endState = { ...initialState, data: newData, loading: false };

        expect(reducer(initialState, fetchDatosSucceededAction)).toEqual(endState);
    });

    it('should handle FETCH_DATOS_FAILED', () => {

        const fetchDatosFailedAction = { type: actionTypes.FETCH_DATOS_FAILED, error: 'errorMsg' };
        const endState = { data: [], error: 'errorMsg', loading: false };

        expect(reducer({}, fetchDatosFailedAction)).toEqual(endState);
    });
});    