import { reducer } from '../../store/reducers/datosHistoricos';

import * as actionTypes from '../../store/actions/actionTypes';

import * as mocks from '../../mocks/datosMocks';

import expect from 'expect';

describe('dispositivos reducer', () => {
    const initialState = {
        data: [],     // Lista de ultimos datos registrados
        loading: false,
        error: false,
    }

    it('should return the initial state', () => {
        expect(reducer({}, {})).toEqual({});
    });


    it('should handle FETCH_DATOS_HISTORICOS_STARTED', () => {
        const fetchDatosHistoricosStartedAcction = { type: actionTypes.FETCH_DATOS_HISTORICOS_STARTED };
        const endState = { ...initialState, loading: true };

        expect(reducer(initialState, fetchDatosHistoricosStartedAcction)).toEqual(endState);
    });

    it('should handle FETCH_DATOS_HISTORICOS_SUCCEEDED', () => {

        const fetchDatosHistoricosSucceededAction = {
            type: actionTypes.FETCH_DATOS_HISTORICOS_SUCCEEDED,
            payload: mocks.datosListMock
        };

        const endState = { ...initialState, data: mocks.datosListMock, loading: false };

        expect(reducer(initialState, fetchDatosHistoricosSucceededAction)).toEqual(endState);
    });

    it('should handle FETCH_DATOS_HISTORICOS_FAILED', () => {

        const fetchDatosHistoricosFailedAction = { type: actionTypes.FETCH_DATOS_HISTORICOS_FAILED, error: 'errorMsg' };
        const endState = { data: [], error: 'errorMsg', loading: false };

        expect(reducer({}, fetchDatosHistoricosFailedAction)).toEqual(endState);
    });

    it('should handle RESET_DATOS_HISTORICOS', () => {

        const resetDatosHistoricosAction = { type: actionTypes.RESET_DATOS_HISTORICOS };
        const endState = { ...initialState };

        expect(reducer({}, resetDatosHistoricosAction)).toEqual(endState);
    });
});    