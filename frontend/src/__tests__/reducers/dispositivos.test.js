import { reducer } from '../../store/reducers/dispositivos';

import * as actionTypes from '../../store/actions/actionTypes';

import { updateList } from '../../store/utility';

import * as mocks from '../../mocks/dispositivosMocks';

import expect from 'expect';

describe('dispositivos reducer', () => {
    let initialState = {
        data: [],     // Lista de dispositivos
        currentId: null,    // ID de UN dispositivo
        loading: false,
        error: false,
    }

    it('should return the initial state', () => {
        expect(reducer({}, {})).toEqual({});
    });

    it('should handle FETCH_DISPOSITIVOS_STARTED', () => {
        const fetchDispositivosStartedAction = { type: actionTypes.FETCH_DISPOSITIVOS_STARTED };
        const endState = { loading: true };

        expect(reducer({}, fetchDispositivosStartedAction)).toEqual(endState);
    });

    it('should handle FETCH_DISPOSITIVOS_SUCCEEDED', () => {

        const dispositivosListMock = [
            {
                "id": 45,
                "etiqueta": "dev1",
                "descripcion": "dev",
                "estado": true,
                "visibilidad": false,
                "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
                "ultima_actividad": "2006-10-25T14:30:59-03:00",
                "latitud": 5.3,
                "longitud": 2.4,
            }
        ];

        // Estado Inicial
        const initialState = {
            data: [],           // Lista de dispositivos
            currentId: null,    // ID de un dispositivo seleccionado
            loading: false,
            error: false,
        }

        //Acción a ejecutar
        const fetchDispositivosSucceededAction = {
            type: actionTypes.FETCH_DISPOSITIVOS_SUCCEEDED,
            payload: dispositivosListMock
        };

        //Datos de Salida esperados
        const endState = { data: dispositivosListMock, currentId: null, loading: false, error: false };

        // Ejecución del test
        expect(reducer(initialState, fetchDispositivosSucceededAction)).toEqual(endState);
    });


    it('should handle FETCH_DISPOSITIVOS_FAILED', () => {

        const fetchDispositivosFailedAction = { type: actionTypes.FETCH_DISPOSITIVOS_FAILED, error: 'errorMsg' };
        const endState = { data: [], currentId: null, error: 'errorMsg', loading: false };

        expect(reducer({}, fetchDispositivosFailedAction)).toEqual(endState);
    });

    it('should handle UPDATE_DISPOSITIVO_STARTED', () => {

        const updateDispositivoStartedAction = { type: actionTypes.UPDATE_DISPOSITIVO_STARTED };
        const endState = { loading: true };

        expect(reducer({}, updateDispositivoStartedAction)).toEqual(endState);
    });

    it('should handle UPDATE_DISPOSITIVO_SUCCEEDED', () => {

        const initialState = { data: mocks.dispositivosListMock };
        const updateDispositivoSucceededAction = { type: actionTypes.UPDATE_DISPOSITIVO_SUCCEEDED, payload: mocks.dispositivoMock };
        const endState = {
            data: updateList(mocks.dispositivosListMock, mocks.dispositivoMock),
            currentItem: { ...mocks.dispositivoMock.id },
            loading: false
        };

        expect(reducer(initialState, updateDispositivoSucceededAction)).toEqual(endState);
    });


    it('should handle UPDATE_DISPOSITIVO_FAILED', () => {

        const updateDispositivoFailedAction = { type: actionTypes.UPDATE_DISPOSITIVO_FAILED, error: 'errorMsg' };
        const endState = {
            error: 'errorMsg',
            loading: false
        };

        expect(reducer({}, updateDispositivoFailedAction)).toEqual(endState);
    });


    it('should handle CREATE_DISPOSITIVO_STARTED', () => {

        const createDispositivoStartedAction = { type: actionTypes.CREATE_DISPOSITIVO_STARTED };
        const endState = { ...initialState, loading: true };

        expect(reducer(initialState, createDispositivoStartedAction)).toEqual(endState);
    });

    it('should handle CREATE_DISPOSITIVO_SUCCEEDED', () => {
        const createDispositivoSucceededAction = { type: actionTypes.CREATE_DISPOSITIVO_SUCCEEDED, payload: mocks.dispositivoMock };
        const endState = { ...initialState, data: updateList(initialState.data, mocks.dispositivoMock), loading: true };

        expect(reducer(initialState, createDispositivoSucceededAction)).toEqual(endState);
    });

    it('should handle CREATE_DISPOSITIVO_FAILED', () => {
        const createDispositivoFailedAction = { type: actionTypes.CREATE_DISPOSITIVO_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, createDispositivoFailedAction)).toEqual(endState);
    });

    it('should handle DELETE_DISPOSITIVO_STARTED', () => {
        const deleteDispositivoStartedAction = { type: actionTypes.DELETE_DISPOSITIVO_STARTED };
        const endState = {
            ...initialState,
            loading: true
        };

        expect(reducer(initialState, deleteDispositivoStartedAction)).toEqual(endState);
    });

    it('should handle DELETE_DISPOSITIVO_SUCCEEDED', () => {
        const initialStateX = { ...initialState, data: mocks.dispositivosListMock };
        const deleteDispositivoSucceededAction = { type: actionTypes.DELETE_DISPOSITIVO_SUCCEEDED, payload: 45 };
        const endState = {
            ...initialState,
            data: mocks.dispositivosListDeletedDispositivoMock,
            loading: false
        };

        expect(reducer(initialStateX, deleteDispositivoSucceededAction)).toEqual(endState);
    });

    it('should handle DELETE_DISPOSITIVO_FAILED', () => {

        const deleteDispositivoFailedAction = { type: actionTypes.DELETE_DISPOSITIVO_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, deleteDispositivoFailedAction)).toEqual(endState);
    });

    it('should handle FETCH_DISPOSITIVO_STARTED', () => {

        const fetchDispositivoStartedAction = { type: actionTypes.FETCH_DISPOSITIVO_STARTED };
        const endState = {
            ...initialState,
            loading: true
        };

        expect(reducer(initialState, fetchDispositivoStartedAction)).toEqual(endState);
    });

    it('should handle FETCH_DISPOSITIVO_SUCCEEDED', () => {

        const initialStateX = { ...initialState, data: mocks.dispositivosListMock }
        const fetchDispositivoSucceededAction = { type: actionTypes.FETCH_DISPOSITIVO_SUCCEEDED, payload: mocks.fetchedDispositivoSuccedeedMock };
        const endState = {
            ...initialState,
            data: updateList(mocks.dispositivosListMock, mocks.fetchedDispositivoSuccedeedMock),
            currentId: mocks.fetchedDispositivoSuccedeedMock.id,
            loading: false
        };

        expect(reducer(initialStateX, fetchDispositivoSucceededAction)).toEqual(endState);
    });

    it('should handle FETCH_DISPOSITIVO_FAILED', () => {

        const fetchDispositivoFailedaction = { type: actionTypes.FETCH_DISPOSITIVO_FAILED, error: 'errorMsg' };
        const endState = {
            ...initialState,
            data: [],
            error: 'errorMsg',
            loading: false
        };

        expect(reducer(initialState, fetchDispositivoFailedaction)).toEqual(endState);
    });

    it('should handle RESET', () => {

        const fetchDispositivoFailedaction = { type: actionTypes.RESET };
        const endState = { ...initialState };

        expect(reducer(initialState, fetchDispositivoFailedaction)).toEqual(endState);
    });

});