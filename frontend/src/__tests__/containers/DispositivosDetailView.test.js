
import React from 'react'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import DispositivoDetailConnected, { DispositivoDetail } from '../../containers/DispositivoDetailView'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';

import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })





// https://binarapps.com/blog/test-ajax-calls-in-react-component-lifecycle/
// https://github.com/ctimmerm/axios-mock-adapter/issues/58
// https://stackoverflow.com/questions/49413937/why-typeerror-axios-create-is-not-a-function-when-testing-axios-get

describe('>>> DispositivosDetailView  -- ISOLATED COMPONENT - Shallow Render REACT COMPONENTS', () => {
    let mockAdapter = new MockAdapter(axios);

    const props = {
        match: {
            params: {
                dispositivoID: '45'
            }
        }
    }

    const response = [
        {
            id: 1,
            username: 'username1'
        },
        {
            id: 2,
            username: 'username2'
        },
        {
            id: 7,
            username: 'username7'
        }]


    mockAdapter.onGet(`${ServerDomain}/api/users/`).reply(200, response);

    let enzymeWrapper
    const token = '5skdhai89399129bdakjsd91fdgasj84r';

    it('test if dispatch axios request of users list', () => {

        enzymeWrapper = shallow(<DispositivoDetail {...props} token={token} permissions='admin' handleRedirect={jest.fn()} onFetchDispositivo={jest.fn()} />)

        enzymeWrapper.instance().componentDidMount().then(() => {

            expect(axios.defaults.headers.Authorization).toBe('Token ' + token)
            // Falta comprobar el estado del componente 

            // State changes after ComponentDidMount:
            expect(enzymeWrapper.state('users')).toEqual(response);

            // Setting New props 
            enzymeWrapper.setProps({ dispositivo: mocksDispositivos.dispositivoMock })

            // State changes after ComponentWillReceiveProps
            expect(enzymeWrapper.state('usernamesEnabled')).toEqual(['username7']);
        })
    });

    it('+++capturing Snapshot of DispositivosList', () => {

        enzymeWrapper = shallow(<DispositivoDetail {...props} token={token} permissions='admin' handleRedirect={jest.fn()} onFetchDispositivo={jest.fn()} />)

        enzymeWrapper.instance().componentDidMount().then(() => {

            expect(enzymeWrapper).toMatchSnapshot();

        });
    });
});


const mockStore = configureMockStore([thunk])
const mockReduxState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r'
    },
    entities: {
        dispositivos: {
            data: mocksDispositivos.DispositivosArrayOfObjectsMock
        },
        variables: {
            data: mocksVariables.VariablesArrayOfObjectsMock
        }
    },
    permissions: ["admin"]
}

// // https://redux.js.org/recipes/writing-tests/
// // https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
// jest.unmock('axios');
// describe('<DispositivosListView/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {

//     describe("<DispositivosListView />'s Dispatching Props:", () => {

//         const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';

//         const props = {
//             match: {
//                 params: {
//                     dispositivoID: '45'
//                 }
//             }
//         }

//         let store = mockStore(mockReduxState)

//         // afterEach(() => {
//         //     store.clearActions()
//         // })

//         describe("#this.props.dispatchFetchDispositivo", () => {
//             it("dispatches the #FETCH_DISPOSITIVO action creator to the Redux store", () => {

//                 console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaa")
//                 let mockAdapter = new MockAdapter(axios);

//                 const response = [
//                     {
//                         id: 1,
//                         username: 'username1'
//                     },
//                     {
//                         id: 2,
//                         username: 'username2'
//                     },
//                     {
//                         id: 7,
//                         username: 'username7'
//                     }]

//                 mockAdapter.onGet(`${ServerDomain}/api/users/`).reply(200, response);

//                 let enzymeWrapper = mount(
//                     <Provider store={store}>
//                         <DispositivoDetail {...props} token={token} permissions='admin' handleRedirect={jest.fn()} onFetchDispositivo={jest.fn()} />
//                     </Provider>
//                 )
//                 // enzymeWrapper.find(VariablesList).instance().props.dispatchFetchVariables()
//                 enzymeWrapper.find(DispositivoDetail).instance().componentDidMount().then(() => {
//                     // console.log(enzymeWrapper.debug())

//                     const expectedActions = [{ "type": "FETCH_VARIABLES_STARTED" }];
//                     // chaiExpect(store.getActions()).to.have.deep.members(expectedActions)
//                     expect(store.getActions()).toEqual(expectedActions)

//                     // Setting New props 
//                     enzymeWrapper.setProps({ dispositivo: mocksDispositivos.dispositivoMock })
//                 })
//             })
//         })

//     });
// });

