
import React from 'react'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import DispositivosMap from '../../containers/MapViewPort'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';

import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })



// https://binarapps.com/blog/test-ajax-calls-in-react-component-lifecycle/
// https://github.com/ctimmerm/axios-mock-adapter/issues/58
// https://stackoverflow.com/questions/49413937/why-typeerror-axios-create-is-not-a-function-when-testing-axios-get

describe('>>> MapViewPort  -- ISOLATED/DISCONNECTED COMPONENT - Shallow Render REACT COMPONENTS', () => {
    let mockAdapter = new MockAdapter(axios);

    let enzymeWrapper
    const token = '5skdhai89399129bdakjsd91fdgasj84r';

    const props = {
        dispositivos: mocksDispositivos.dispositivosListMock,
        token: { token },
        permissions: 'admin'
    }

    it('+++capturing Snapshot of MapViewPort with spin loading true', () => {

        enzymeWrapper = shallow(<DispositivosMap {...props} />)

        expect(enzymeWrapper).toMatchSnapshot();

    });
});


// const mockStore = configureMockStore([thunk])
// const mockReduxState = {
//     authentication: {
//         token: 'Token 5skdhai89399129bdakjsd91fdgasj84r'
//     },
//     entities: {
//         dispositivos: {
//             data: mocksDispositivos.DispositivosArrayOfObjectsMock
//         },
//         variables: {
//             data: mocksVariables.VariablesArrayOfObjectsMock
//         },
//         // variableObj: mocksVariables.fetchedVariableSuccedeedMock
//     },
//     permissions: ["admin"]
// }

// // // https://redux.js.org/recipes/writing-tests/
// // // https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
// // jest.unmock('axios');
// describe('<VariableDetailiew/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {

//     describe("<VariableDetailView />'s Dispatching Props:", () => {

//         const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';

//         const props = {
//             match: {
//                 params: {
//                     dispositivoID: '45',
//                     variableID: '63'
//                 }
//             }
//         }

//         let store = mockStore(mockReduxState)

//         // afterEach(() => {
//         //     store.clearActions()
//         // })

//         describe("#this.props.onFetchVariable", () => {
//             it("dispatches the #FETCH_VARIABLE action creator to the Redux store", () => {

//                 let mockAdapter = new MockAdapter(axios);

//                 const response = mocksVariables.fetchedVariableSuccedeedMock

//                 mockAdapter.onGet(`${ServerDomain}/api/dispositivos/${props.match.params.dispositivoID}/variables/${props.match.params.variableID}/`).reply(200, response);

//                 let enzymeWrapper = mount(
//                     <Provider store={store}>
//                         <VariableDetailConnected {...props} token={token} permissions='admin' onFetchVariable={jest.fn()} />
//                     </Provider>
//                 )

//                 const expectedActions = [{ "type": "FETCH_VARIABLE_STARTED" }, { "type": "FETCH_VARIABLE_STARTED" }];

//                 enzymeWrapper.find(VariableDetail).instance().props.onFetchVariable()
//                 chaiExpect(store.getActions()).to.deep.equal(expectedActions)

//             })
//         })

//     });
// });

