import React from 'react'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import VariablesListConnected, { VariablesList } from '../../containers/VariablesListView'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocks from '../../mocks/dispositivosMocks';


const { Title } = Typography;

Enzyme.configure({ adapter: new Adapter() })

const mockStore = configureMockStore([thunk])
const mockState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r'
    },
    entities: {
        variables: {
            data: mocks.VariablesArrayOfObjectsMock
        }
    },
    permissions: ["admin"]
}


// https://medium.com/netscape/testing-a-react-redux-app-using-jest-and-enzyme-b349324803a9
// https://hackernoon.com/unit-testing-redux-connected-components-692fa3c4441c
// https://circleci.com/blog/continuously-testing-react-applications-with-jest-and-enzyme/

describe('>>> VariablesListView  -- ISOLATED COMPONENT - Shallow Render REACT COMPONENTS', () => {
    let enzymeWrapper
    const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';
    const dispositivoID = '43';

    beforeEach(() => {
        enzymeWrapper = shallow(<VariablesList token={token} dispositivoID={dispositivoID} dispatchFetchVariables={jest.fn()} />)
    })

    it('+++ render the DUMB component', () => {
        chaiExpect(enzymeWrapper.length).equal(1)
    });

    it('+++ contains Variables Title', () => {
        // expect(wrapper.find('input#dynamic_form_item_timestampColumnName.ant.input').prop('value')).toEqual(output)
        chaiExpect(enzymeWrapper.contains(<Title level={4}>Variables</Title>)).to.be.equal(true)
    });

});

// https://redux.js.org/recipes/writing-tests/
// https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
describe('<VariablesListView/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {
    describe("<VariablesListView />'s Dispatching Props:", () => {
        let enzymeWrapper
        const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';
        const dispositivoID = '43';
        let store = mockStore(mockState)

        beforeEach(() => {
            enzymeWrapper = mount(
                <Provider store={store}>
                    {/* <VariablesListConnected token={token} dispositivoID={dispositivoID} onFetchVariables={jest.fn()} /> */}
                    <VariablesListConnected token={token} dispositivoID={dispositivoID} />
                </Provider>
            )
        })

        afterEach(() => {
            store.clearActions()
        })

        describe("#this.props.dispatchFetchVariables", () => {
            it("dispatches the #FETCH_VARIABLES action creator to the Redux store", () => {

                const expectedActions = [
                    { type: 'FETCH_VARIABLES_STARTED' },
                    { type: 'FETCH_VARIABLES_STARTED' }]
                // console.log(wrapper.debug())
                enzymeWrapper.find(VariablesList).instance().props.dispatchFetchVariables()
                chaiExpect(store.getActions()).to.deep.equal(expectedActions)
            })
        })
    });
});

// https://medium.com/netscape/testing-a-react-redux-app-using-jest-and-enzyme-b349324803a9
// Snapshot for VariablesList React Component
describe('>>>VariablesList --- Snapshot', () => {

    const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';
    const dispositivoID = '43';
    let store = mockStore(mockState)

    it('+++capturing Snapshot of VariablesList', () => {
        const VariablesList =
            <Provider store={store}>
                <VariablesListConnected token={token} dispositivoID={dispositivoID} dispatchFetchVariables={jest.fn()} />
            </Provider>;
        const renderedValue = renderer.create(VariablesList).toJSON()
        expect(renderedValue).toMatchSnapshot();
    });
});
