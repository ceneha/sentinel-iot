
import React from 'react'
// import "jsdom-global/register"
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import SignupConnected, { RegistrationForm } from '../../containers/Signup'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocksDispositivos from '../../mocks/dispositivosMocks';
import * as mocksVariables from '../../mocks/variablesMocks';

import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })



// https://binarapps.com/blog/test-ajax-calls-in-react-component-lifecycle/
// https://github.com/ctimmerm/axios-mock-adapter/issues/58
// https://stackoverflow.com/questions/49413937/why-typeerror-axios-create-is-not-a-function-when-testing-axios-get

// describe('>>> Signup  -- ISOLATED COMPONENT - Shallow Render REACT COMPONENTS', () => {
//     let enzymeWrapper
//     const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';
//     const dispositivoID = '43';

//     beforeEach(() => {
//         enzymeWrapper = mount(<SignupConnected onAuth={jest.fn()} />)
//     })

//     it('+++ render the DUMB component', () => {
//         chaiExpect(enzymeWrapper.length).equal(1)
//     });

//     it('+++ contains Variables Title', () => {
//         // expect(wrapper.find('input#dynamic_form_item_timestampColumnName.ant.input').prop('value')).toEqual(output)
//         chaiExpect(enzymeWrapper.contains(<Title level={4}>Variables</Title>)).to.be.equal(true)
//     });

// });


const mockStore = configureMockStore([thunk])
const mockReduxState = {
    authentication: {
        loading: false,
        error: "Fucking error"
    },
}

// // https://redux.js.org/recipes/writing-tests/
// // https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
// jest.unmock('axios');
describe('<SignupConnected/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {



    // const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';

    // const props = {
    //     form: {
    //         validateFieldsAndScroll: jest.fn(opts => c => c),
    //         getFieldDecorator: jest.fn(opts => c => c),
    //         handleConfirmBlur: jest.fn(opts => c => c),
    //         validateToNextPassword: jest.fn(opts => c => c),
    //         getFieldValue: jest.fn(opts => c => c)
    //     }
    // }


    // const props = {
    //     form: {
    //         getFieldValue: jest.fn(opts => c => c)
    //     }
    // }


    let store = mockStore(mockReduxState)

    // afterEach(() => {
    //     store.clearActions()
    // })

    // let enzymeWrapper

    let enzymeWrapper = mount(
        <Provider store={store}>
            <SignupConnected onAuth={jest.fn()} _eventListeners={jest.fn()} />
        </Provider>
    )

    it('+++ render the DUMB component', () => {
        //console.log(enzymeWrapper.debug())
        chaiExpect(enzymeWrapper.length).equal(1)
    });

    it('+++ render the DUMB component correctly', () => {
        //   console.log(enzymeWrapper.debug())
        // const newFunction = jest.fn();


        let wrapper = mount(
            <Provider store={store}>
                <SignupConnected onAuth={jest.fn()} _eventListeners={jest.fn()} />
            </Provider>
        )
        wrapper.find('#pass1').simulate('change', { target: { value: 'pass' } });
        wrapper.find('#pass2').simulate('change', { target: { value: 'pass' } });

        wrapper.find("#signupBtn", { preventDefault() { } }).at(0).simulate('submit');
        expect(wrapper).toMatchSnapshot();


    });
});

