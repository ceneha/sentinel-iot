
import React from 'react'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { Typography } from 'antd'

import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureMockStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import { expect as chaiExpect } from 'chai'
import sinon from 'sinon';

import DispositivosListConnected, { DispositivosList } from '../../containers/DispositivosListView'
// import { fetchVariables } from '../../store/actions/variables'
// import { reducer } from '../../store/reducers/variables'

import * as mocks from '../../mocks/dispositivosMocks';
import { doesNotReject } from 'assert'
import { ServerDomain } from '../../routes';

jest.unmock('axios');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


Enzyme.configure({ adapter: new Adapter() })


const mockStore = configureMockStore([thunk])
const mockState = {
    authentication: {
        token: 'Token 5skdhai89399129bdakjsd91fdgasj84r'
    },
    entities: {
        dispositivos: {
            data: mocks.DispositivosArrayOfObjectsMock
        }
    },
    permissions: ["admin"]
}


// https://binarapps.com/blog/test-ajax-calls-in-react-component-lifecycle/
// https://github.com/ctimmerm/axios-mock-adapter/issues/58
// https://stackoverflow.com/questions/49413937/why-typeerror-axios-create-is-not-a-function-when-testing-axios-get



describe('>>> DispositivosListView  -- ISOLATED COMPONENT - Shallow Render REACT COMPONENTS', () => {
    let mockAdapter = new MockAdapter(axios);

    const response = {
        data: [
            {
                id: 1,
                username: 'username1'
            },
            {
                id: 2,
                username: 'username2'
            }]
    }
    mockAdapter.onGet(`${ServerDomain}/api/users/`).reply(200, response);

    let enzymeWrapper
    const token = '5skdhai89399129bdakjsd91fdgasj84r';

    it('test if dispatch axios request of users list', async (done) => {

        enzymeWrapper = shallow(<DispositivosList token={token} permissions='admin' dispatchFetchDispositivos={jest.fn()} />)

        enzymeWrapper.instance().componentDidMount().then(() => {

            expect(axios.defaults.headers.Authorization).toBe('Token ' + token)
            // Falta comprobar el estado del componente
            expect(enzymeWrapper.state('users')).toEqual(response);
            done();
        })
    });

    it('+++capturing Snapshot of DispositivosList', async (done) => {

        enzymeWrapper = shallow(<DispositivosList token={token} permissions='admin' dispatchFetchDispositivos={jest.fn()} />)

        enzymeWrapper.instance().componentDidMount().then(() => {

            expect(enzymeWrapper).toMatchSnapshot();

            done();
        })
    });
});

// https://redux.js.org/recipes/writing-tests/
// https://www.breadoliveoilsalt.com/coding/2019/07/11/testing-redux-connected-components.html
describe('<DispositivosListView/>  -- CONNECTED COMPONENT --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {
    describe("<DispositivosListView />'s Dispatching Props:", () => {
        let enzymeWrapper
        const token = 'Token 5skdhai89399129bdakjsd91fdgasj84r';


        let store = mockStore(mockState)

        // beforeEach(() => {
        //     enzymeWrapper = mount(
        //         <Provider store={store}>
        //             <DispositivosListConnected token={token} />
        //         </Provider>
        //     )
        // })

        // afterEach(() => {
        //     store.clearActions()
        // })

        describe("#this.props.dispatchFetchDispositivos", () => {
            it("dispatches the #FETCH_DISPOSITIVOS action creator to the Redux store", async (done) => {

                let mockAdapter = new MockAdapter(axios);

                const response = {
                    data: [
                        {
                            id: 1,
                            username: 'username1'
                        },
                        {
                            id: 2,
                            username: 'username2'
                        }]
                }
                mockAdapter.onGet(`${ServerDomain}/api/users/`).reply(200, response.data);

                let enzymeWrapper = mount(
                    <Provider store={store}>
                        <DispositivosListConnected token={token} permissions='admin' dispatchFetchDispositivos={jest.fn()} />
                    </Provider>
                )
                // enzymeWrapper.find(VariablesList).instance().props.dispatchFetchVariables()
                enzymeWrapper.find(DispositivosList).instance().componentDidMount().then(() => {
                    // console.log(enzymeWrapper.debug())

                    const expectedActions = [{ "type": "FETCH_DISPOSITIVOS_STARTED" }, { "type": "FETCH_DISPOSITIVOS_STARTED" }];
                    // chaiExpect(store.getActions()).to.have.deep.members(expectedActions)
                    expect(store.getActions()).toEqual(expectedActions)
                    done();
                })
            })
        })

    });
});

