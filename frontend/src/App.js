import React from 'react';
import { connect } from 'react-redux';

import BaseRouter from './routes';
import * as actions from './store/actions/auth';
import CustomLayout from './containers/Layout';

import 'antd/dist/antd.css';

// Es necesario conectar el componente App con el State de la app
export class App extends React.Component {

  //Cada vez que se monta el componente, se ejecuta el metodo onTryAutoSignUp(),
  //que fue mappeado como se encuentra mapeado como una propiedad.
  //El onTryAutoSignUp() a su vez ejecuta un action: authCheckState, que verifica
  //si el usuario se encuentra logueado o no.
  componentDidMount() {

    document.title = "Sentinel";
    this.props.onTryAutoSignUp();
  }

  //Cada vez que la app se renderiza, se chequeará automaticamente si el usuario se encuentra logueado o no.
  // Para esto se ejecuta el authCheckState() automaticamente cada vez que el componente se monta
  render() {
    return (
      <div>

        {/* Pasamos dinamicamente hacia "abajo" el isAuthenticated para que pueda ser manipulado
        por el CustomLayout */}
        <CustomLayout {...this.props}>
          <BaseRouter />
        </CustomLayout>
      </div>
    );
  }
}

//Mapea el State de la app para que pueda ser utilizado como una propiedad: isAuthenticated.
// Ahora podria llamar a CustomLayout y pasarle como parametro el "isAuthenticated"
const mapStateToProps = state => {
  return {
    isAuthenticated: state.authentication && state.authentication.token !== null
  }
}

//Mapea un método para que pueda ser utilizado como una propiedad: onTryAutoSignUp 
const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignUp: () => dispatch(actions.authCheckState())
  }
}


//Se conectan las nuevas propiedades mapeadas mediante mapStateToProps y mapDispatchToProps
// a la App.
//Esto toma el Store previamente creado y permite acceder al estado de dicho store.
export default connect(mapStateToProps, mapDispatchToProps)(App);
