export const dispositivosListMock = [
    {
        "id": 45,
        "etiqueta": "dev1",
        "descripcion": "asd",
        "estado": true,
        "visibilidad": false,
        "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
        "ultima_actividad": "2006-10-25T14:30:59-03:00",
        "latitud": 5.3,
        "longitud": 2.4,
        "users_enabled": [
            7
        ],
        "url": "http://127.0.0.1:8000/api/dispositivos/45/"
    },
    {
        "id": 46,
        "etiqueta": "dev1",
        "descripcion": "test46",
        "estado": true,
        "visibilidad": false,
        "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
        "ultima_actividad": "2006-10-25T14:30:59-03:00",
        "latitud": 80,
        "longitud": 25,
        "users_enabled": [
            5
        ],
        "url": "http://127.0.0.1:8000/api/dispositivos/46/"
    }
];

export const dispositivoMock = {
    "id": 45,
    "etiqueta": "dev1",
    "descripcion": "updateDev",
    "estado": true,
    "visibilidad": false,
    "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
    "ultima_actividad": "2006-10-25T14:30:59-03:00",
    "latitud": 5.3,
    "longitud": 2.4,
    "users_enabled": [
        7
    ],
    "url": "http://127.0.0.1:8000/api/dispositivos/45/"
};

export const dispositivoCreatedMock = [{
    "id": 45,
    "etiqueta": "dev1",
    "descripcion": "updateDev",
    "estado": true,
    "visibilidad": false,
    "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
    "ultima_actividad": "2006-10-25T14:30:59-03:00",
    "latitud": 5.3,
    "longitud": 2.4,
    "users_enabled": [
        7
    ],
    "url": "http://127.0.0.1:8000/api/dispositivos/45/"
}
];

export const dispositivosListDeletedDispositivoMock = [
    {
        "id": 46,
        "etiqueta": "dev1",
        "descripcion": "test46",
        "estado": true,
        "visibilidad": false,
        "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
        "ultima_actividad": "2006-10-25T14:30:59-03:00",
        "latitud": 80,
        "longitud": 25,
        "users_enabled": [
            5
        ],
        "url": "http://127.0.0.1:8000/api/dispositivos/46/"
    }
];

export const fetchedDispositivoSuccedeedMock = {
    "id": 45,
    "etiqueta": "devFetched",
    "descripcion": "devFetched",
    "estado": true,
    "visibilidad": false,
    "fecha_creacion": "2019-11-29T01:47:22.908155-03:00",
    "ultima_actividad": "2006-10-25T14:30:59-03:00",
    "latitud": 5.3,
    "longitud": 2.4,
    "users_enabled": [
        7
    ],
    "url": "http://127.0.0.1:8000/api/dispositivos/45/"
};