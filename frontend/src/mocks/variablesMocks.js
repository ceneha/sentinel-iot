export const variablesListMock = [
    {
        "id": 63,
        "nombre": "VAR1",
        "descripcion": "tu variable",
        "etiqueta": "var1",
        "unidad": "farenheti",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
    },
    {
        "id": 70,
        "nombre": "VAR70",
        "descripcion": "tu variable70",
        "etiqueta": "var1",
        "unidad": "Celsius",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/70/"
    }
];

export const variableMock = {
    "id": 63,
    "nombre": "VAR1_UPDATED",
    "descripcion": "updatedVariable",
    "etiqueta": "updated",
    "unidad": "farenheit",
    "valor_min_permitido": "-5.000",
    "valor_max_permitido": "98.000",
    "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
    "dispositivo": 45,
    "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
};



export const variablesListDeletedDispositivoMock = [
    {
        "id": 70,
        "nombre": "VAR70",
        "descripcion": "tu variable70",
        "etiqueta": "var1",
        "unidad": "Celsius",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/70/"
    }
];


export const fetchedVariableSuccedeedMock = {
    "id": 63,
    "nombre": "FETCHED_VAR",
    "descripcion": "FETCHED_VAR",
    "etiqueta": "fetched",
    "unidad": "farenheit",
    "valor_min_permitido": "-5.000",
    "valor_max_permitido": "98.000",
    "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
    "dispositivo": 45,
    "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
};

export const VariablesListOfObjectsMock = {
    0: {
        "id": 63,
        "nombre": "FETCHED_VAR",
        "descripcion": "FETCHED_VAR",
        "etiqueta": "fetched",
        "unidad": "farenheit",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
    },
    1: {
        "id": 64,
        "nombre": "FETCHED_VAR",
        "descripcion": "FETCHED_VAR",
        "etiqueta": "fetched",
        "unidad": "farenheit",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
    }
}

export const VariablesArrayOfObjectsMock = [
    {
        "id": 63,
        "nombre": "FETCHED_VAR",
        "descripcion": "FETCHED_VAR",
        "etiqueta": "fetched",
        "unidad": "farenheit",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
    },
    {
        "id": 64,
        "nombre": "FETCHED_VAR",
        "descripcion": "FETCHED_VAR",
        "etiqueta": "fetched",
        "unidad": "farenheit",
        "valor_min_permitido": "-5.000",
        "valor_max_permitido": "98.000",
        "fecha_creacion": "2019-11-30T10:42:56.474513-03:00",
        "dispositivo": 45,
        "url": "http://127.0.0.1:8000/api/dispositivos/45/variables/63/"
    }
]