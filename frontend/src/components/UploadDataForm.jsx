import React from 'react';
import axios from 'axios';
import { Button, Form, Input, Icon, Divider, Alert, message } from 'antd';

import { ServerDomain } from '../routes';

let id = 0;

class UploadData extends React.Component {

    state = {
        alertMessage: null,
        visible: true,
        confirmLoading: false,
        fieldsNames: [],
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = () => {
        this.setState({
            visible: false,
            confirmLoading: false,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    };

    handleCancel = () => {
        this.props.unmountMe();
        // console.log('Clicked cancel button');
        // this.setState({
        //     visible: false,
        // });
    };


    remove = k => {
        const { form } = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        // We need at least one passenger
        if (keys.length === 1) {
            return;
        }

        // can use data-binding to set
        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    };

    add = () => {
        const { form } = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(id++);
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
            keys: nextKeys,
        });
    };


    /**
     * Prepare data for POST Request.
     * 
     * @param  data  [{"timestamp": timestamp, "id_variable": 58.5 }]
     *         Example: [ 
     *          {"timestamp": "2019-10-08T12:10:03Z", "62": 58.5 },
     *          {"timestamp": "2019-10-08T12:10:03Z", "54": 60.3 }      
     *        ]
     * 
     * @param timestampColumnName Name of timestamp's column from the file
     * 
     * @param variablesColumnsNames List of variables IDs (extracteds from the file) wich will be recorded 
     *                              on the server
     *          Example: ['55', '1', '5']
     *
     */
    prepareData = (data, timestampColumnName, variablesColumnsNames) => {

        let newData = [];
        for (let i = 0; i < variablesColumnsNames.length; i++) {
            data.map((item, index) => {
                if (item[variablesColumnsNames[i]] !== null && item[variablesColumnsNames[i]] !== undefined)
                    newData.push({
                        timestamp: item[timestampColumnName],
                        variable: variablesColumnsNames[i],
                        valor: item[variablesColumnsNames[i]]
                    })
            })
        }

        return newData;
    }



    handleUpload(data) {

        if (data && this.props) {

            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
            axios.defaults.xsrfCookieName = "csrftoken";
            axios.defaults.headers = {
                "Content-Type": "application/json",
                "Authorization": `Token ${this.props.token}`,
            }

            axios.post(`${ServerDomain}/api/datos/`, data)
                .then(response => {
                    if (response.status === 201 || response.status === 200) { //CREATED
                        //this.handleOk();
                        message.success("Datos guardados correctamente");
                        this.props.unmountMe();
                    }
                })
                .catch(err => {

                    if (err.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx

                        //  message.error("Archivo con formato inválido");
                        const errorResponse = err.response.data;
                        let errMessage = "Errores: ";
                        console.log("error", errorResponse)
                        errorResponse.map((item, index) => {
                            if (Object.keys(item).length !== 0) {
                                Object.keys(item).forEach(function (key) {
                                    errMessage = errMessage + "(Linea " + index + ") " + item[key] + " // ";
                                })
                            }
                        }
                        )

                        this.setError("error", "Error al intentar cargar el archivo", errMessage);
                    } else {
                        this.setError("error", "Error al intentar cargar el archivo", "Archivo con formato inválido.-");
                        // message.error("Archivo con formato inválido");
                    }
                });
        }

    }

    validateColumnNames = (uploadedData, timestampColumnName, variablesColumnsNames) => {

        //First row of file: contains the columns's names. Example:  (timestamp, 45, 28, TMP, HUM)
        const firstObj = uploadedData[1];

        //check existing key in object
        if (!(timestampColumnName in firstObj)) {
            return false;
        }

        //check existing keys in object
        for (let i = 0; i < variablesColumnsNames.length; i++) {
            if (!(variablesColumnsNames[i] in firstObj)) {
                return false;
            }
        }

        return true;
    }

    //Return names of columns from the updated data (updated file)
    getColumnsNamesFromUpdatedData = () => {
        if (this.props.uploadedData && this.props.uploadedData.length > 0) {
            const firstLine = this.props.uploadedData[0];

            return Object.keys(firstLine);
        }
    }


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                // const { keys, names } = values;

                const uploadedData = this.props.uploadedData;
                if (uploadedData.length > 1) {

                    const variablesColumnsNames = values.names;
                    const timestampColumnName = values.timestampColumnName;

                    if (this.validateColumnNames(uploadedData, timestampColumnName, variablesColumnsNames)) {

                        const data = this.prepareData(uploadedData, timestampColumnName, variablesColumnsNames);

                        this.handleUpload(data);

                    } else {
                        this.setError("warning", "Error al validar datos ingresados", "Columnas inválidas");
                    }


                } else {

                    console.log("Archivo sin datos registrados");
                }

            }
        });
    };

    setError = (type, message, description) => {
        this.setState({
            alertMessage: {
                type: type,
                message: message,
                description: description
            }
        })
    }

    alertMessage = () => {
        return <div>
            <br />
            <Alert
                type={this.state.alertMessage.type}
                message={this.state.alertMessage.message}
                description={this.state.alertMessage.description}
            />
            <br />
        </div>
    }


    render() {

        // const { visible, confirmLoading } = this.state;
        const { getFieldDecorator, getFieldValue } = this.props.form;


        getFieldDecorator('keys', { initialValue: [] });
        const keys = getFieldValue('keys');
        const formItems = keys.map((k, index) => (


            <Form.Item
                // label={index === 0 ? 'Columna' : ''}
                required={false}
                key={k}
            >
                {getFieldDecorator(`names[${k}]`, {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                        {
                            required: true,
                            whitespace: true,
                            message: "Por favor ingrese los identificadores de las columnas que representan los valores registrados por cada variable",
                        },
                    ],
                })(<Input placeholder="Identificador de la variable" style={{ width: '50%', marginRight: 8 }} />)}
                {keys.length > 1 ? (
                    <Icon
                        className="dynamic-delete-button"
                        type="minus-circle-o"
                        onClick={() => this.remove(k)}
                    />
                ) : null}
            </Form.Item>
        ));

        return (
            <div  >
                <Divider />
                {this.state.alertMessage ? this.alertMessage() : null}

                <h3> <b>Configuración del archivo de carga</b></h3>
                <br />
                <Form onSubmit={this.handleSubmit}>

                    {this.getColumnsNamesFromUpdatedData() ?
                        <Alert
                            message="Nombres de columnas existentes en archivo de datos:"
                            description={this.getColumnsNamesFromUpdatedData().toString()}
                            type="info"
                            showIcon
                        />
                        :
                        null
                    }
                    <br />


                    <Form.Item label="Nombre del campo 'timestamp'">
                        {getFieldDecorator('timestampColumnName', {
                            initialValue: "timestamp",
                            rules: [{ required: false, message: "Por favor ingrese el nombre de la columna que representa el 'timestamp' del dato registrado." }],
                        })(
                            <Input name="timestampColumnName" style={{ width: '50%' }} placeholder="Nombre de la columna que contiene el 'timestamp' " />,
                        )}
                    </Form.Item>

                    <p>Columnas del archivo a cargar</p>
                    {formItems}
                    <Form.Item >
                        <Button type="dashed" onClick={this.add} style={{ width: '50%' }}>
                            <Icon type="plus" /> Agregar columna
                            </Button>
                    </Form.Item>
                    <Form.Item >
                        <Button id="btnAceptarUploadData" type="primary" htmlType="submit">
                            Aceptar
                        </Button>

                        <Button type="primary" style={{ marginLeft: 12 }} onClick={this.handleCancel}>
                            Cancelar
                        </Button>
                    </Form.Item>
                </Form>

            </div>
        );
    }
}


const UploadDataForm = Form.create({ name: 'dynamic_form_item' })(UploadData);

export default UploadDataForm;