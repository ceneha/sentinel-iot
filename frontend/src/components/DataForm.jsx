import React from 'react';
import { connect } from 'react-redux';
import { Form, DatePicker, Button } from 'antd';

import CsvDownloadDatos from './CsvDownloadDatos';
import * as actions from '../store/actions/datos';


const { RangePicker } = DatePicker;


export class DataFormView extends React.Component {

    constructor(props) {
        super(props);
      }


    handleSubmit = (event) => {
        event.preventDefault();

        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }

            // Should format date value before submit.
            const rangeTimeValue = fieldsValue['range-time-picker'];

            const parameters = {
                variableID: this.props.match.params.variableID,
                start_date: rangeTimeValue[0].format('YYYY-MM-DD HH:mm:ss'),
                end_date: rangeTimeValue[1].format('YYYY-MM-DD HH:mm:ss')
            }

            this.props.onSubmit(this.props.token, parameters);
        });
    };


    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };

        const rangeConfig = {
            rules: [{ type: 'array', required: true, message: 'Please select time!' }],
        };
        return (
            <Form {...formItemLayout} onSubmit={(event) => this.handleSubmit(event)}>

                <Form.Item label="Rango de fechas">
                    {getFieldDecorator('range-time-picker', rangeConfig)(
                        <RangePicker showTime format="YYYY-MM-DD HH:mm:ss" placeholder={["Fecha de inicio", "Fecha de fin"]} />,
                    )}
                    <Button id="btnFiltrar" type="primary" htmlType="submit" style={{ margin: '20px' }}>
                        Filtrar
                    </Button>

                    <CsvDownloadDatos />
                </Form.Item>
            </Form>
        );
    }
}


// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
export const mapStateToProps = state => {
    return {
        token: state.authentication.token,
        // datos_historicos: state.entities.datos.historicos.data,
    }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (token, parameters) => dispatch(actions.fetchDatosHistoricos(token, parameters)),
    }
}


const DataForm = Form.create({ name: 'time_related_controls' })(DataFormView);

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DataForm));
export default connect(mapStateToProps, mapDispatchToProps)(DataForm);