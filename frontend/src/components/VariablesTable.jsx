import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router'
import { Button, Table, Divider, Popconfirm, message } from 'antd';
import Permissions from "react-redux-permissions"

import * as actions from '../store/actions/variables';

export class VariablesTable extends React.Component {

    constructor(props) {
        super(props);

        this.confirmDelete = this.confirmDelete.bind(this);
        this.cancelDelete = this.cancelDelete.bind(this);
    }

    confirmDelete(dispositivoID, item) {
        this.props.onDelete(this.props.token, dispositivoID, item.id);
        message.success('Variable eliminada');
    }

    cancelDelete(e) {
        message.error('Eliminación cancelada');
    }


    columns = [
        {
            title: 'N°',
            dataIndex: 'id',
            key: 'id'
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre'
        },
        {
            title: 'Etiqueta',
            dataIndex: 'etiqueta',
            key: 'etiqueta'
        },
        {
            title: 'Unidad',
            dataIndex: 'unidad',
            key: 'unidad',
        },
        {
            title: 'Valor Mín. Permitido',
            key: 'valor_min_permitido',
            dataIndex: 'valor_min_permitido',
        },
        {
            title: 'Valor Máx. Permitido',
            key: 'valor_max_permitido',
            dataIndex: 'valor_max_permitido',
        },
        {
            title: 'Fecha Creación',
            dataIndex: 'fecha_creacion',
            key: 'fecha_creacion',
        },
        {
            title: 'Acción    ',
            key: 'action',
            columnWidth: '20%',
            render: (text, item) => (
                <div>
                    <Button id="ver-variable-btn" type="primary" onClick={() => this.props.onRedirect(`/dispositivos/${this.props.dispositivoID}/variables/${item.id}/`)} >Ver</Button>
                    <Permissions allowed={["admin"]}>
                        <Divider type="vertical" />

                        <Popconfirm
                            id="popconfirmVariableBtn"
                            title="Está seguro que desea eliminar esta variable y todos sus datos asociados?"
                            onConfirm={(event) => this.confirmDelete(this.props.dispositivoID, item)}
                            onCancel={(event) => this.cancelDelete()}
                            okText="Yes"
                            cancelText="No"
                        >
                            {/* <Button type="danger" onClick={(event) => this.props.onDelete(event, this.props.dispositivoID, item.id)}> Eliminar </Button> */}
                            <Button id="eliminar-variable-btn" type="danger" > Eliminar </Button>
                        </Popconfirm>
                    </Permissions>
                </div>
            ),
        },
    ];

    componentWillReceiveProps(newProps) {
        if (newProps.token && newProps.token !== this.props.token) {
            this.props.onFetchVariables();
        }
    }

    render() {
        return (
            <div>
                <Table rowKey="id" columns={this.columns} dataSource={this.props.variables} />
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onDelete: (token, dispositivoID, variableID) => dispatch(actions.handleDeleteVariable(token, dispositivoID, variableID)),
        onRedirect: (url) => dispatch(push(url)),
    }
}

export default connect(null, mapDispatchToProps)(VariablesTable);

