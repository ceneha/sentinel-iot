import React from 'react';
import { Alert } from 'antd';

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.state = {
            hasError: false,
            error: null,
            info: null
        };
    }

    componentDidCatch(error, info) {
        this.setState({
            hasError: true,
            error: error,
            info: info
        });
    }


    render() {
        if (this.state.hasError) {
            return (
                <div>
                    <Alert message={this.state.error.toString()} description={this.state.info.componentStack} type="error" />
                    {/* <h1>Oops!!! Something went wrong</h1>
                    <p>The error: {this.state.error.toString()}</p>
                    <p>Where it occured: {this.state.info.componentStack}</p> */}
                </div>
            );
        } else {
            return this.props.children;
        }
    }
}


export default ErrorBoundary;