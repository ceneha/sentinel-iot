import React from 'react';
import Permissions from "react-redux-permissions"

import Img from 'react-image'


// import Background1 from '../images/Background1.png'
// import Background4 from '../images/Background4.png'
// import Background5 from '../images/Background5.png'
// import Background6 from '../images/Background6.jpg'
// import Background7 from '../images/Background7.jpg'
// import Background8 from '../images/Background8.jpg'
// import Background9 from '../images/Background9.png'
// import Background10 from '../images/Background10.jpg'
// import Background11 from '../images/Background11.png'
import Background12 from '../images/Background12.png'
// import Background13 from '../images/Background13.jpg'




export default class WelcomeImage extends React.Component {

    render() {
        return (
            <div>
                <Permissions except={["admin", "visitor"]}>
                    <Img src={Background12} style={{
                        maxWidth: "90%",
                        maxHeight: "30%"
                    }} />
                </Permissions>
            </div>

        );
    }

}