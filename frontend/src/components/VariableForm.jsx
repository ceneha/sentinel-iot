import React from 'react';
import { connect } from 'react-redux';

import { Drawer, Form, Button, Col, Row, Input, Icon } from 'antd';

import * as actions from '../store/actions/variables';

export class VariableFormView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            estado: 'True',
            visible: false,
        };

        this.handleEstadoChange = this.handleEstadoChange.bind(this);
    }

    //Despliega el Drawer del formulario
    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    //Cierra el Drawer del formulario
    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    handleEstadoChange(estadoValue) {
        this.setState({ estado: estadoValue });
    }

    handleSubmit = (event) => {
        //event.preventDefault();
        // const dispositivo = this.props.dispositivo;
        // const variableID = this.props.variableID;

        //Prepara el objeto variable con los datos extraidos del formulario
        const postObj = {
            nombre: event.target.elements.nombre.value,
            etiqueta: event.target.elements.etiqueta.value,
            descripcion: event.target.elements.descripcion.value,
            unidad: event.target.elements.unidad.value,
            valor_min_permitido: event.target.elements.valMin.value,
            valor_max_permitido: event.target.elements.valMax.value,
            // dispositivo: dispositivo.id,
            dispositivo: this.props.dispositivoID,
        }

        const data = {
            requestType: this.props.requestType,
            dispositivoID: this.props.dispositivoID,
            variableID: this.props.variableID,
            postObj: postObj
        }

        event.preventDefault();
        this.props.onSubmit(this.props.token, data);
        this.onClose();
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Button id="btnShowDrawer" type="primary" onClick={this.showDrawer} style={{ float: 'right' }} >
                    <Icon type="plus" /> {this.props.btnText}
                </Button>

                <Drawer
                    title="Variable"
                    width={720}
                    onClose={this.onClose}
                    visible={this.state.visible}
                >
                    <Form layout="vertical" hideRequiredMark
                        onSubmit={(event) => this.handleSubmit(event)}>
                        {/* onSubmit={(event) => this.handleSubmit(
                             event,
                             this.props.requestType,
                             this.props.dispositivo,
                             this.props.variable)}> */}


                        <Row gutter={16}>

                            <Col span={12}>
                                <Form.Item label="Nombre">
                                    {getFieldDecorator('nombre', {
                                        initialValue: this.props.variableObj ? this.props.variableObj.nombre : "",
                                        rules: [{ required: true, message: "Por favor ingrese nombre de la variable" }],
                                    })(<Input name="nombre" placeholder="Ingrese nombre de la variable" />)}
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item label="Etiqueta">
                                    {getFieldDecorator('etiqueta', {
                                        initialValue: this.props.variableObj ? this.props.variableObj.etiqueta : "",
                                        rules: [{ required: true, message: 'Por favor ingrese identificador de la variable' }],
                                    })(
                                        <Input
                                            name="etiqueta"
                                            style={{ width: '100%' }}
                                            placeholder="Ingrese identificador de la variable"
                                        />,
                                    )}
                                </Form.Item>
                            </Col>

                        </Row>

                        <Row gutter={16}>

                            <Col span={12}>
                                <Form.Item label="Unidad">
                                    {getFieldDecorator('unidad', {
                                        initialValue: this.props.variableObj ? this.props.variableObj.unidad : "",
                                        rules: [{ required: true, message: "Por favor ingrese unidad de medición" }],
                                    })(<Input name="unidad" placeholder="Ingrese unidad de medición de la variable" />)}
                                </Form.Item>
                            </Col>

                        </Row>

                        <Row gutter={16}>

                            <Col span={12}>
                                <Form.Item label="Valor Mínimo">
                                    {getFieldDecorator('valMin', {
                                        initialValue: this.props.variableObj ? this.props.variableObj.valor_min_permitido : "",
                                        rules: [{ required: false, message: "Por favor ingrese valor mínimo tolerado" }],
                                    })(<Input name="valMin" placeholder="Ingrese valor mínimo tolerado" />)}
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item label="Valor máximo">
                                    {getFieldDecorator('valMax', {
                                        initialValue: this.props.variableObj ? this.props.variableObj.valor_max_permitido : "",
                                        rules: [{ required: false, message: "Por favor ingrese valor mínimo tolerado" }],
                                    })(
                                        <Input
                                            name="valMax"
                                            style={{ width: '100%' }}
                                            placeholder="Ingrese valor máximo tolerado"
                                        />,
                                    )}
                                </Form.Item>
                            </Col>

                        </Row>


                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item label="Descripcion">
                                    {getFieldDecorator('descripcion', {
                                        initialValue: this.props.variableObj ? this.props.variableObj.descripcion : "",
                                        rules: [
                                            {
                                                required: false,
                                                message: 'Por favor ingrese descripción',
                                            },
                                        ],
                                    })(<Input.TextArea rows={4} placeholder="Ingrese descripción" />)}
                                </Form.Item>
                            </Col>
                        </Row>

                        <div
                            style={{
                                position: 'absolute',
                                left: 0,
                                bottom: 0,
                                width: '100%',
                                borderTop: '1px solid #e9e9e9',
                                padding: '10px 16px',
                                background: '#fff',
                                textAlign: 'right',
                            }}
                        >

                            <Button onClick={this.onClose} style={{ marginRight: 8 }}> Cancelar </Button>
                            <Button id="submitVariableBtn" type="primary" htmlType="submit"> Guardar </Button>

                        </div>
                    </Form>
                </Drawer >
            </div >
        );
    }
}


// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
export const mapStateToProps = state => {
    return {
        token: state.authentication.token,
    }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (token, data) => dispatch(actions.handleSubmit(token, data)),
    }
}



const VariableForm = Form.create()(VariableFormView);

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VariableForm));
export default connect(mapStateToProps, mapDispatchToProps)(VariableForm);