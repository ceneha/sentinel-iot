
// Copyright (c) 2016 - 2017 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React from 'react';
import axios from 'axios';
import { Typography } from 'antd';
import { LineMarkSeries, XAxis, YAxis, Crosshair, HorizontalGridLines, VerticalGridLines, FlexibleWidthXYPlot } from 'react-vis';
import { ServerDomain } from '../routes';
const { Title } = Typography;
//Aca importaba unos Estiloss CSS !! Chequear si se rompio algo !!
//import '../../../node_modules/react-vis/dist';
//import '../../../node_modules/react-vis/dist/style.css';



export default class VariableRealTimeChart2 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            variableName: this.props.variableNombre,
            crosshairValues: [],
            data: [],
            value: false,
        };
    }


    /**
     * Plotea en la gráfica un arreglo inicial de datos registrados 
     * que se obtienen tras el primer fetch al servidor.
     */
    async plotData(datas) {

        const data = [...datas.map((item, index) => {
            return {
                // x0: new Date(Date.parse(item.timestamp) - 2000),
                x: new Date(Date.parse(item.timestamp)),
                y: parseFloat(item.valor)
            };
        })];

        await this.setState({
            data: data,
        });
    }

    /**
     * Cada 3000 ms se dispara una consulta al servidor, que devuelve un array con
     * los ultimos ¿50? datos registrados paginados de la variable específicada. 
     * 
     * En el primer disparo:
     *          - Se setea el initialState con los primeros 49 registros de esa ultima página 
     *            y se pasan al componente que grafica.
     * 
     * En cada disparo siguiente:
     *          - Se extrae el último elemento del array y se setea en el state del componente
     *            state.data = Ultimo nuevo dato
     *          - Una vez seteado el nuevo dato, se pasa como una prop al componente <RTChart />
     *            que se encarga de agregar dicho dato en la gráfica.
     */

    async UNSAFE_componentWillMount() {

        try {
            this.myInterval = setInterval(async () => {
                const token = this.props.token;
                const variableID = this.props.variableID;

                if (token && variableID) {
                    axios.defaults.headers = { "Content-Type": "application/json", "Authorization": `Token ${token}` }

                    await axios.get(`${ServerDomain}/api/datos/?page=last&variable=${variableID}`)
                        .then(response => {
                            this.plotData(response.data.results);
                        })
                        .catch(error => {
                            console.log(error)
                        })
                }
            }
                , 3000);

        } catch (error) {
            console.log("error VariableRealTimeChart", error);
        }
    }

    /** 
     * Unmount the component and clear the interval to stop executing the fetching periodically when
     * changing route in the browser
    */
    UNSAFE_componentWillUnmount() {
        clearInterval(this.myInterval);
    }


    render() {

        const {
            data,
            value
        } = this.state;


        const lineSeriesProps = {
            animation: true,
            // sizeRange: [5, 15],
            color: '#0D676C',
            opacityType: 'literal',
            curve: 'curveMonotoneX',
            data,
            onNearestX: d => this.setState({ value: d })
        };



        return (
            <div className='forms'>

                <Title level={4}>Datos registrados en tiempo real</Title>

                <FlexibleWidthXYPlot
                    xType="time"
                    height={250}
                    onMouseLeave={() => this.setState({ value: false })}
                    animation={true}

                >
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis title='Línea de tiempo' />
                    <YAxis title={this.state.variableName} />

                    {/* <LineSeries
                                data={this.state.data}
                                onNearestX={(value, { index }) =>
                                    this.setState({ crosshairValues: this.state.data.map(d => d[index]) })}
                            // curve={'curveMonotoneX'}
                            /> */}

                    {<LineMarkSeries {...lineSeriesProps} />}
                    {value && <Crosshair values={[value]} />}

                </FlexibleWidthXYPlot>




                {/* {this.state.data ? 
                <div>
                     {console.log("Hay DATA", this.state.data)} 
                    <FlexibleWidthXYPlot
                        // yDomain={[-50, 100]}
                        //xDomain={[this.state.data[0].x, this.state.data[this.state.data.length - 1].x]}
                        xType="time"
                        height={300}
                    >
                        <VerticalGridLines />
                        <HorizontalGridLines />
                        <XAxis />
                        <YAxis />

                        {
                            this.state.data ?
                                <VerticalRectSeries data={this.state.data} style={{ stroke: '#fff' }} />
                                :
                                <VerticalRectSeries data={[{ x0: 0, x: 0, y: 0 }]} style={{ stroke: '#fff' }} />
                        }
                    </FlexibleWidthXYPlot>

                </div >
                    */}

            </div>
        );
    }
}

