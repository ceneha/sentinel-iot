// Copyright (c) 2016 - 2017 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React from 'react';
import { connect } from 'react-redux';
import { XAxis, YAxis, HorizontalGridLines, VerticalGridLines, LineMarkSeries, Crosshair, FlexibleWidthXYPlot } from 'react-vis';
import * as variablesSelectors from '../store/selectors/variables';

/**
 * Para customizar los gráficos consultar:
 *     React-vis:     https://uber.github.io/react-vis/documentation/welcome-to-react-vis
 *                    https://uber.github.io/react-vis/examples/showcases/misc
 */

class DatosHistoricosChart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            crosshairValues: [],
            data: [],
            value: false,
        };
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.currentVariable) {
            this.setState({
                variableName: nextProps.currentVariable.nombre
            })
        }

        if (nextProps.datosHistoricos && Object.keys(nextProps.datosHistoricos).length > 0) {
            this.plotData(nextProps.datosHistoricos);
        }
    }


    /**
     * Plotea en la gráfica un arreglo inicial de datos registrados que se obtienen tras el primer fetch al servidor
     */
    async plotData(datos) {

        const data = [...datos.map((item, index) => {
            return {
                x: new Date(Date.parse(item.timestamp)),
                y: parseFloat(item.valor)
            };
        })];

        await this.setState({
            data: data,
        });
    }


    render() {

        const {
            data,
            value
        } = this.state;


        const lineSeriesProps = {
            animation: true,
            color: '#0D676C',
            opacityType: 'literal',
            curve: 'curveMonotoneX',
            data,
            onNearestX: d => this.setState({ value: d })
        };


        return (
            <div>
                <FlexibleWidthXYPlot
                    xType="time"
                    height={250}
                    onMouseLeave={() => this.setState({ value: false })}
                    animation={true}
                >
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis title='Línea de tiempo' />
                    <YAxis title={this.props.variableNombre} />


                    {<LineMarkSeries {...lineSeriesProps} />}
                    {value && <Crosshair values={[value]} />}

                </FlexibleWidthXYPlot>
            </div>
        );
    }
}

// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
export const mapStateToProps = state => {
    // console.log(datosHistoricos);
    return {
        token: state.authentication.token,
        datosHistoricos: state.entities.datos.historicos.data,
        currentVariable: variablesSelectors.selectCurrentVariable(state.entities.variables.data, state.entities.variables.currentId)
    }
}


export default connect(mapStateToProps, null)(DatosHistoricosChart);