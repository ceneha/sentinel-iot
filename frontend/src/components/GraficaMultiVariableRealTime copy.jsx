// Copyright (c) 2016 - 2017 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React from 'react';

import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries
} from 'react-vis';

const MSEC_DAILY = 86400000;

export default function Example(props) {
//   const timestamp = new Date('September 9 2017').getTime(); 
  const timestamp1 = new Date(Date.parse('2021-08-03T14:35:59Z')).getTime();
  const timestamp2 = new Date(Date.parse('2021-08-03T14:36:03Z')).getTime();
  const timestamp3 = new Date(Date.parse('2021-08-03T14:36:10Z')).getTime();
  const timestamp4 = new Date(Date.parse('2021-08-03T14:36:28Z')).getTime();

  const timestamp11 = new Date(Date.parse('2021-08-03T14:34:28Z')).getTime();
  const timestamp22 = new Date(Date.parse('2021-08-03T14:37:03Z')).getTime();
  const timestamp33 = new Date(Date.parse('2021-08-03T14:37:22Z')).getTime();
  const timestamp44 = new Date(Date.parse('2021-08-03T14:37:33Z')).getTime();
  const timestamp55 = new Date(Date.parse('2021-08-03T14:38:01Z')).getTime();
  const timestamp66 = new Date(Date.parse('2021-08-03T14:38:15Z')).getTime();

//   console.log("timestampp", timestamp);
  return (
    <XYPlot xType="time" width={600} height={300}>
      <HorizontalGridLines />
      <VerticalGridLines />
      <XAxis title="X Axis" />
      <YAxis title="Y Axis" />
      <LineSeries
        data={[
          {x: timestamp1, y: 3},
          {x: timestamp2, y: 5},
          {x: timestamp3, y: 15},
          {x: timestamp4, y: 12}
        ]}
      />
      <LineSeries data={null} />
      <LineSeries
        data={[
          {x: timestamp11, y: 10},
          {x: timestamp22, y: 4},
          {x: timestamp33, y: 2},
          {x: timestamp44, y: 15},
          {x: timestamp55, y: 12},
          {x: timestamp66, y: 22}
        ]}
      />
    </XYPlot>
  );
}