/**
 * Componente que permite cargar al sistema datos registrados por los dispositivos desde archivos
 * con extensión .csv.
 */

import React from 'react'
import { connect } from 'react-redux';
import { message, } from 'antd';
import CSVReader from 'react-csv-reader'
import Title from 'antd/lib/typography/Title';
import UploadDataForm from './UploadDataForm';


class CsvUploadDatos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: null,
            uploadDataFormVisibility: false,
            uploadedData: []
        }

        this.handleChildUnmount = this.handleChildUnmount.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
    }

    handleChildUnmount() {
        this.setState({
            uploadDataFormVisibility: false,
            uploadedData: []
        });
    }

    success = (msg) => {
        message.success(msg);
    };

    error = (errorMsg) => {
        message.success(errorMsg);
    };

    handleUpload(uploadedData) {
        this.setState({
            uploadDataFormVisibility: true,
            uploadedData: uploadedData
        });
    }

    render() {

        const { uploadDataFormVisibility } = this.state;

        const papaparseOptions = {
            header: true,
            dynamicTyping: true,
            skipEmptyLines: true,
            transformHeader: header =>
                header
                    .toLowerCase()
                    .replace(/\W/g, '_')
        }

        return (

            <div className="forms" >
                <p><b>Carga de archivos de datos</b></p>
                < CSVReader
                    cssClass="csv-reader-input "
                    //cssInputClass="button ant-btn ant-btn-primary"
                    label="Seleccione un archivo CSV para cargar datos registrados al sistema  "
                    onFileLoaded={this.handleUpload}
                    onError={this.handleError}
                    inputId="csvReaderInput"
                    inputStyle={{ color: 'blue' }}
                    parserOptions={papaparseOptions}
                />
                {uploadDataFormVisibility ?
                    <div>
                        <UploadDataForm token={this.props.token} uploadedData={this.state.uploadedData} unmountMe={this.handleChildUnmount} />
                    </div>
                    :
                    null
                }

            </div >

        )
    }
}

export const mapStateToProps = state => {
    return {
        token: state.authentication.token,
    }
}


export default connect(mapStateToProps, null)(CsvUploadDatos);