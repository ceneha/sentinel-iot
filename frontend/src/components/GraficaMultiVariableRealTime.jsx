// Copyright (c) 2016 - 2017 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import * as selectors from '../store/selectors/dispositivos';


import { ServerDomain } from '../routes';
import { Typography } from 'antd';
import { XAxis, YAxis, HorizontalGridLines, VerticalGridLines, LineMarkSeries, LineSeries, FlexibleWidthXYPlot, Hint,DiscreteColorLegend } from 'react-vis';
import { format } from "date-fns";



export class GraficaMultiVariableRealTime extends React.Component {

  constructor(props) {
    super(props);

    const lineSeriesData = <LineSeries data={[ ]}/>;

    this.state = {
        series: [],
        value: false,
        crosshairValues: [],
        hintData : {},
        hintHover : false,
        showComponent: true
    };

    this.tickFormat = this.tickFormat.bind(this);
    this.getHintSection = this.getHintSection.bind(this);
    this.mouseOver = this.mouseOver.bind(this);
    this.mouseOut = this.mouseOut.bind(this);    
  }

  /**
   * Plotea en la gráfica un arreglo inicial de datos registrados que se obtienen tras el primer fetch al servidor
   */
    async plotData(datax) {

    var showComponent = false;  
    const lineSeriesData = [...datax.map((variable) => {

      const myArrMadeFromForEach = [];

      variable.data.forEach((d)=> myArrMadeFromForEach.push(
        {
          x: new Date(Date.parse(d.timestamp)).getTime(),
          y: parseFloat(d.valor)
        }

      ));

      if(myArrMadeFromForEach.length != 0) {
        showComponent = true;
      }  

      return  {
        title: variable.id + " - " +variable.nombre,
        data: myArrMadeFromForEach
      }  


    }
  )];

    

  await this.setState({
    series: lineSeriesData,
    showComponent: showComponent
    });

  }


  // Carga los datos de la grafica instantaneamente en el primer renderizado
  componentDidMount() {
    try {
          this.fetchData();
    } catch (error) {
        console.log("error GraficaMultiVariableRealTime", error);
    }
  }


  // CActualiza los datos de la grafica tras 10 seg de delay
  async UNSAFE_componentWillMount() {

    try {
        this.myInterval = setInterval(async () => {
          await this.fetchData();
        }
            , 5000);

    } catch (error) {
        console.log("error GraficaMultiVariableRealTime", error);
    }
}



fetchData(){
  const token = this.props.token;
  const dispositivoID = this.props.dispositivoID;

  if (token) {
      axios.defaults.headers = { "Content-Type": "application/json", "Authorization": `Token ${token}` }

       axios.get(`${ServerDomain}/api/get-datos-multisensores/dispositivo/${dispositivoID}`)
          .then(response => {
              this.plotData(response.data);
          })
          .catch(error => {
              console.log(error)
          })
  }
}


tickFormat(key) {
  const splitString = "-";
  const splittedKey = key.split(splitString);
  const week = splittedKey[1];
  return week;
}

getHintSection(isHintVisible) {
  return isHintVisible ?
    <Hint value={this.state.hintData}>
        <div style={{background: 'grey'}}>
              {format(new Date(this.state.hintData.x), 'M/d/yyyy H:mm:ss')} <br/>
              {this.state.hintData.y}
        </div>
    </Hint> : null;
}

mouseOver(datapoint) {
  this.setState({hintData : datapoint, hintHover :true});
}

mouseOut(datapoint) {
  this.setState({hintData : datapoint, hintHover :false});
}


render() {

    const {
       value,
    } = this.state;


    return (

      <div className='forms '>
          <p><b>Variables del dispositivo en tiempo real</b></p>
      <div className="inline-elements">

          {this.state.showComponent ?
          <FlexibleWidthXYPlot
              xType="time"
              height={250}
              animation={true}
          >
            <HorizontalGridLines />
            <VerticalGridLines />
            <XAxis  />
            <YAxis title="Valor registrado" />

            {
                this.state.series.map((serie, index) => (
                  [ 
                    <LineMarkSeries 
                             getNull={(d) => d.y !== null}
                              data={serie.data} 
                              animation={true} 
                              curve={'curveMonotoneX'}    
                              size={1}
                              onValueMouseOver={this.mouseOver}
                              onValueMouseOut={this.mouseOut}
                              />,
                    this.getHintSection(this.state.hintHover)
                  ]
                ))
            }

          <DiscreteColorLegend 
                    className="float-right"
                    items={this.state.series}
                    orientation={'horizontal'}
                  />

      </FlexibleWidthXYPlot>  
      :
       null
       
    }
      </div>
      {this.state.showComponent ?
        <small>* Las variables del dispositivo que no están en la gráfica no han registrado datos durante la última hora.</small>
        :
        <small>* No se han registrado datos para ninguna variable del dispositivo durante la última hora.</small>
      }
    </div>      

    );  
  }
}


const mapStateToProps = state => {
  // console.log("DetailStateToProps", state.authentication.token);

  return {
       variables: selectors.objectToArray(state.entities.variables.data),
  }
}

export default connect(mapStateToProps, null)(GraficaMultiVariableRealTime);