import React from 'react';
import { connect } from 'react-redux';
import CsvDownload from 'react-json-to-csv';

import * as dispositivosSelectors from '../store/selectors/dispositivos';
import * as variablesSelectors from '../store/selectors/variables'


class CsvDownloadDatos extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            filename: 'datos_registrados.csv',
            btnDisabled: true
        }

    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.datosHistoricos && Object.keys(nextProps.datosHistoricos).length > 0) {
            this.setState({
                btnDisabled: false
            })
        }

        if (nextProps.currentDispositivo && nextProps.currentVariable) {
            this.setState({
                filename: nextProps.currentDispositivo.etiqueta + '_' + nextProps.currentVariable.etiqueta + '.csv'
            })
        }

    }


    render() {

        return (
            <CsvDownload data={this.props.datosHistoricos}
                filename={this.state.filename}
                className='button ant-btn ant-btn-primary'
                // style={{ marginLeft: '20px' }}
                disabled={this.state.btnDisabled}
            >Descargar datos
            </CsvDownload>
        );
    }

}

export const mapStateToProps = state => {
    return {
        token: state.authentication.token,
        datosHistoricos: state.entities.datos.historicos.data,
        currentDispositivo: dispositivosSelectors.selectCurrentDispositivo(state.entities.dispositivos.data, state.entities.dispositivos.currentId),
        currentVariable: variablesSelectors.selectCurrentVariable(state.entities.variables.data, state.entities.variables.currentId)
    }
}


export default connect(mapStateToProps, null)(CsvDownloadDatos);