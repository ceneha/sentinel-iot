import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router'
import Permissions from "react-redux-permissions"
import { Button, Table, Divider, Tag, Popconfirm, message } from 'antd';

import moment from 'moment';
import 'moment/min/locales';

import * as actions from '../store/actions/dispositivos';



class DispositivosTable extends React.Component {

  constructor(props) {
    super(props);

    moment.locale('es-es');

    this.confirmDelete = this.confirmDelete.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
  }

  confirmDelete(item) {
    this.props.onDeleteDispositivo(this.props.token, item.id);
    message.success('Dispositivo eliminado');
  }

  cancelDelete(e) {
    message.error('Eliminación cancelada');
  }



  columns = [
    {
      title: 'N°',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Estacion',
      dataIndex: 'etiqueta',
      key: 'etiqueta'
    },
    {
      title: 'Descripcion',
      dataIndex: 'descripcion',
      key: 'descripcion',
    },
    {
      title: 'Fecha Creación',
      dataIndex: 'fecha_creacion',
      key: 'fecha_creacion',
      render: (fecha_creacion) => {
        var fecha = fecha_creacion ? moment(fecha_creacion).format('L') : '-';
        return (fecha)
      }
    },
    {
      title: 'Estado',
      key: 'estado',
      dataIndex: 'estado',
      render: (estado) => {
        let color = estado === true ? 'green' : 'volcano';
        let texto = estado === true ? 'Habilitado' : 'No Habilitado';
        return (
          <Tag color={color} key={estado}>
            {texto}
          </Tag>
        );
      },
    },
    {
      title: 'Ultima Actividad',
      dataIndex: 'ultima_actividad',
      key: 'ultima_actividad',
      render: (ultima_actividad) => {
        var fecha = ultima_actividad ? moment(ultima_actividad).calendar() : '-';
        return (fecha)
      }
    },
    {
      title: 'Acción    ',
      key: 'action',
      columnWidth: '20%',
      render: (text, item) => (
        <div>
          <Button id="ver-dispositivo-btn" type="primary" onClick={() => this.props.onRedirect(`/dispositivos/${item.id}/`)} >Ver</Button>
          <Permissions allowed={["admin"]}>
            <Divider type="vertical" />
            <Popconfirm
              title="Está seguro que desea eliminar éste dispositivo y todos sus datos asociados?"
              onConfirm={() => this.confirmDelete(item)}
              onCancel={() => this.cancelDelete()}
              okText="Si"
              cancelText="No"
            >
              <Button type="danger"> Eliminar</Button>
            </Popconfirm>
          </Permissions>
        </div>
      ),
    },
  ];

  componentWillReceiveProps(newProps) {
    if (newProps.token && newProps.token !== this.props.token) {
      this.props.onFetchDispositivos();
    }
  }

  render() {
    return (
      < Table id="asdasd" rowKey="id" columns={this.columns} dataSource={this.props.dispositivos} />
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDeleteDispositivo: (token, dispositivoID) => dispatch(actions.handleDeleteDispositivo(token, dispositivoID)),
    onRedirect: (url) => dispatch(push(url)),
  }
}

export default connect(null, mapDispatchToProps)(DispositivosTable);

