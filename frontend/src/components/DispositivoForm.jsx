import React from 'react';
import { connect } from 'react-redux';
// import { push } from 'connected-react-router'

import { Drawer, Form, Button, Col, Row, Input, Icon, Select, Switch } from 'antd';

import * as actions from '../store/actions/dispositivos';

const { Option } = Select;

export class DispositivoFormView extends React.Component {

  constructor(props) {
    super(props);

    // const userEnabled = this.props.requestType === "put" && this.props.dispositivo && this.props.dispositivo.users_enabled ? this.props.dispositivo.users_enabled.map(String) : [];

    this.state = {
      estado: true,
      // visibilidad: 'False',
      visible: false,

      users: [], //Todos los usuarios obtenidos tras fetch al servidor en formato [{key => value}]
      listUsers: [], // lista de objetos tipo <Option> con los usuarios obtenidos

      // Usuarios seleccionados para ser habilitados al dispositivo
      usersEnabled: this.props.requestType === "put" && this.props.dispositivo && this.props.dispositivo.users_enabled ? this.props.dispositivo.users_enabled.map(String) : []
    };

    this.handleEstadoChange = this.handleEstadoChange.bind(this);
  }


  componentWillReceiveProps(newProps) {
    if (newProps.users !== null && this.props.users !== newProps.users) {

      if (Array.isArray(newProps.users)) {
        let listUsers = newProps.users.map((user) =>
          <Option key={user.id}>{user.username}</Option>
        );
        this.setState({ listUsers: listUsers });
      }

    }
  }


  //Despliega el Drawer del formulario
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  //Cierra el Drawer del formulario
  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  /**
 *  Modifica con el Switch en el formulario la visibilidad del dispositivo 
 *  segun usuarios administradores / no adminstradores 
 */
  handleEstadoChange = (checked) => {
    this.setState({
      estado: checked
    });
  }



  handleUsersEnabledChange = (value) => {

    this.setState({
      usersEnabled: value
    });
  }


  handleSubmit = (event) => {

    //Prepara el objeto Dispositivo con los datos extraidos del formulario
    const postObj = {
      etiqueta: event.target.elements.etiqueta.value,
      descripcion: event.target.elements.descripcion.value,
      estado: this.state.estado,
      latitud: event.target.elements.latitud.value,
      longitud: event.target.elements.longitud.value,
      users_enabled: this.state.usersEnabled
    }

    const dispositivo = this.props.dispositivo;
    event.preventDefault();
    this.props.onSubmit(this.props.token, postObj, this.props.requestType, dispositivo);
    this.onClose();
  }




  render() {

    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Button id="btnShowDrawer" type="primary" onClick={this.showDrawer} style={{ float: 'right' }} >
          <Icon type="plus" /> {this.props.btnText}
        </Button>

        <Drawer

          title="Dispositivo"
          width={600}
          onClose={this.onClose}
          visible={this.state.visible}
        >

          <Form layout="vertical" hideRequiredMark
            onSubmit={(event) => this.handleSubmit(event)}>

            <Row gutter={10}>
              <Col span={12}>
                <Form.Item label="Identificador">
                  {getFieldDecorator('etiqueta', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.etiqueta : "",
                    rules: [{ required: true, message: "Por favor un identificador del dispositivo" }],
                  })(<Input name="etiqueta" placeholder="Ingrese identificador del dispositivo" />)}
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item label="Estado">
                  {getFieldDecorator('estado', {
                    defaultChecked: this.props.dispositivo && this.props.dispositivo.estado === true ? true : false,
                    rules: [{ required: false, message: 'Por favor elija un estado!' }],
                  })(
                    <Switch checkedChildren={"Habilitado"} unCheckedChildren={"No habilitado"} onChange={this.handleEstadoChange} />
                  )}
                </Form.Item>
              </Col>

            </Row>

            <Row gutter={10}>
              <Col span={8}>
                <Form.Item label="Latitud">
                  {getFieldDecorator('latitud', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.latitud : "",
                    rules: [{ required: true, message: "Por favor ingrese latitud de la ubicación del dispositivo" }],
                  })(<Input name="latitud" placeholder="Ingrese longitud de la ubicación del dispositivo" />)}
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item label="Longitud">
                  {getFieldDecorator('longitud', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.longitud : "",
                    rules: [{ required: true, message: "Por favor ingrese longitud de la ubicación del dispositivo" }],
                  })(
                    <Input name="longitud" style={{ width: '100%' }} placeholder="Ingrese longitud de la ubicación del dispositivo" />,
                  )}
                </Form.Item>
              </Col>

            </Row>


            <Row gutter={10}>
              <Col span={16}>
                <Form.Item label="Usuarios habilitados para acceder al dispositivo">
                  {getFieldDecorator('users_enabled', {
                    initialValue: this.props.requestType === "put" && this.props.dispositivo && this.props.dispositivo.users_enabled ? this.props.dispositivo.users_enabled.map(String) : [],
                    rules: [{ required: true, message: 'Por favor elija usuarios habilitados para consultar el dispositivo' }],
                  })(
                    <Select
                      name="users_enabled"
                      mode="multiple"
                      style={{ width: '100%' }}
                      placeholder="Seleccione usuarios"
                      onChange={this.handleUsersEnabledChange}
                    >
                      {this.state.listUsers}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>




            <Row gutter={10}>
              <Col span={24}>
                <Form.Item label="Descripcion">
                  {getFieldDecorator('descripcion', {
                    initialValue: this.props.dispositivo ? this.props.dispositivo.descripcion : "",
                    rules: [
                      {
                        required: false,
                        message: 'Por favor ingrese descripción',
                      },
                    ],
                  })(<Input.TextArea rows={4} placeholder="Ingrese descripción" />)}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: 'absolute',
                left: 0,
                bottom: 0,
                width: '100%',
                borderTop: '1px solid #e9e9e9',
                padding: '10px 16px',
                background: '#fff',
                textAlign: 'right',
              }}
            >

              <Button onClick={this.onClose} style={{ marginRight: 8 }}> Cancelar </Button>
              <Button id="submitDispositivoBtn" type="primary" htmlType="submit"> Guardar </Button>

            </div>
          </Form>
        </Drawer>

      </div >
    );
  }
}

// Mapea el token almacenado en el state del Store de la app para que pueda ser utilizado
// como una propiedad en el componente 'DispositivoDetail'.
// Es ahora en el componente puedo acceder al valor del token a través de: this.props.token
export const mapStateToProps = state => {
  return {
    token: state.authentication.token
  }
}

// Mapea el método "handleRedirect" como una propiedad del componente. Siendo handleDelete la Key del objeto
const mapDispatchToProps = dispatch => {
  return {
    onSubmit: (token, postObj, requestType, dispositivo) => dispatch(actions.handleSubmit(token, postObj, requestType, dispositivo)),
  }
}

const DispositivoForm = Form.create()(DispositivoFormView);

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DispositivoForm));
export default connect(mapStateToProps, mapDispatchToProps)(DispositivoForm);