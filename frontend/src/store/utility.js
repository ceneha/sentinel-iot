// Actualiza las propiedades de los objetos.
// Reemplaza los valores de las claves de oldObject con los valores de
// las claves exitentes en updatedProperties
// Retorna un objeto actualizado
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    }
}

//Actualiza un objeto (newItem) de un listado en forma de array
//Input: data= [{id:1, etiqueta: 'DEV100'}, {id:2, etiqueta: 'DEV200'}]  , newItem = {id:1, etiqueta: 'DEV999'} 
//Output data= [{id:1, etiqueta: 'DEV999'}, {id:2, etiqueta: 'DEV200'}]
export const updateList = (data, newItem) => {

    let updated = false;
    const datax = data.map((item, index) => {
        // Find the item with the matching id
        if (item.id === newItem.id) {
            updated = true;
            return { ...newItem } // replace the item            
        }
        // Leave every other item unchanged
        return item;
    });

    //Si  se modificó algun elemento del listado entonces se se retorna la lista actualizada
    // de caso contrario se inserta un nuevo elemento al final
    return updated ? [...datax] : [...datax, newItem];
}


// Recibe un array asociativo de objectos con keys incrementales 
// Input:  [ 0 : {id:8 , ...},  1 : {id:25 , ...}]
// Retorna un objeto  que tiene sub-objetos con ID como claves
// Output: { 8: {id:8 , ...},  25: {id:25 , ...}}
export const arrayToObjectWithId = (arr) => {
    return arr.reduce((obj, item) => {
        obj[item.id] = item
        return obj
    }, {})
}

// Recibe un array asociativo de objectos con keys incrementales 
// Input:  [ 0 : {id:8 , ...},  1 : {id:25 , ...}]
// Retorna un objeto 
// Output: {id:8 , ...}
export const arrayToObject = (arr) => {
    return arr.reduce((obj, item) => {
        return item
    }, {})
}


/// Recibe un objeto con que tiene sub-objetos con ID como claves
// Input:  { 8: {id:8 , ...},  25: {id:25 , ...}} 
// Retorna un array de objectos con keys incrementales
// Output: [ 0 : {id:8 , ...},  1 : {id:25 , ...}]
export const objectToArray = (obj) => {
    return Object.values(obj);
}