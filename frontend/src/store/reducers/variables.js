/**
 * This is a reducer, a pure function with (state, action) => state signature.
 * It describes how an action transforms the state into the next state.
 *
 * The shape of the state is up to you: it can be a primitive, an array, an object,
 * or even an Immutable.js data structure. The only important part is that you should
 * not mutate the state object, but return a new object if the state changes.
 *
 * Here, we use a 'switch' statement and strings, but you can use a helper that
 * follows a different convention (such as function maps) if it makes sense for your
 * project.
 */

import * as actionTypes from '../actions/actionTypes';
import { updateList } from '../utility';
// import { updateList, arrayToObject } from '../utility';
// import { updateVariablesList } from '../actions/variables';


const initialState = {
    data: [],     // Lista de variables
    currentId: null,    // Id de una variable
    loading: false,
    error: false,
}

export const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.FETCH_VARIABLES_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_VARIABLES_SUCCEEDED:

            state = {
                ...state,
                //data: arrayToObject([...action.payload]),
                data: [...action.payload],
                currentId: null,
                loading: false,
                error: false
            };
            break;

        case actionTypes.FETCH_VARIABLES_FAILED:
            state = {
                ...state,
                data: [],
                currentId: null,
                error: action.error,
                loading: false
            };
            break;


        case actionTypes.UPDATE_VARIABLE_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.UPDATE_VARIABLE_SUCCEEDED:

            state = {
                ...state,
                data: updateList(state.data, action.payload),
                currentItem: { ...action.payload.id },
                loading: false
            };
            break;

        case actionTypes.UPDATE_VARIABLE_FAILED:
            state = {
                ...state,
                error: action.error,
                loading: false
            };
            break;

        case actionTypes.CREATE_VARIABLE_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.CREATE_VARIABLE_SUCCEEDED:
            state = {
                ...state,
                data: updateList(state.data, action.payload),
                loading: true,
            };
            break;


        case actionTypes.CREATE_VARIABLE_FAILED:
            state = {
                ...state,
                // data: null,
                error: action.error,
                loading: false
            };
            break;


        case actionTypes.DELETE_VARIABLE_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        //Se recibe VariableID en action.payload y se elimina elimina del VARIABLE del listado del state
        case actionTypes.DELETE_VARIABLE_SUCCEEDED:
            state = {
                ...state,
                data: state.data.filter(item => item.id !== action.payload),
                loading: false
            };
            break;


        case actionTypes.DELETE_VARIABLE_FAILED:
            state = {
                ...state,
                error: action.error,
                loading: false
            };
            break;



        case actionTypes.FETCH_VARIABLE_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_VARIABLE_SUCCEEDED:

            state = {
                ...state,
                //data: updateList(...state.data, ...action.payload),
                data: updateList(state.data, action.payload),
                currentId: action.payload.id,
                loading: false
            };
            break;

        case actionTypes.FETCH_VARIABLE_FAILED:
            state = {
                ...state,
                data: null,
                error: action.error,
                loading: false
            };
            break;

        case actionTypes.RESET:
            state = { ...initialState };
            break;

        default:
            return state;
    }

    return state;
}
