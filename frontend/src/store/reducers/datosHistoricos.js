import * as actionTypes from '../actions/actionTypes';


/**
 * Reducer de datos historicos
 *  
 */

const initialState = {
    data: [],     // Lista de variables
    loading: false,
    error: false,
}

export const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.FETCH_DATOS_HISTORICOS_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_DATOS_HISTORICOS_SUCCEEDED:
            state = {
                ...state,
                data: [...action.payload],
                loading: false
            };
            break;

        case actionTypes.FETCH_DATOS_HISTORICOS_FAILED:
            state = {
                ...state,
                data: [],
                error: action.error,
                loading: false
            };
            break;

        case actionTypes.RESET_DATOS_HISTORICOS:
            state = { ...initialState };
            break;

        default:
            return state;
    }

    return state;
}                