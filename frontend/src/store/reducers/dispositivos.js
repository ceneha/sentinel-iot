/**
 * This is a reducer, a pure function with (state, action) => state signature.
 * It describes how an action transforms the state into the next state.
 *
 * The shape of the state is up to you: it can be a primitive, an array, an object,
 * or even an Immutable.js data structure. The only important part is that you should
 * not mutate the state object, but return a new object if the state changes.
 *
 * Here, we use a 'switch' statement and strings, but you can use a helper that
 * follows a different convention (such as function maps) if it makes sense for your
 * project.
 */

import * as actionTypes from '../actions/actionTypes';
import { updateList } from '../utility';


const initialState = {
    data: [],     // Lista de dispositivos
    currentId: null,    // ID de UN dispositivo
    loading: false,
    error: false,
}

export const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.FETCH_DISPOSITIVOS_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_DISPOSITIVOS_SUCCEEDED:

            state = {
                ...state,
                //data: arrayToObject([...action.payload]),
                data: [...action.payload],
                currentId: null,
                loading: false
            };
            break;

        case actionTypes.FETCH_DISPOSITIVOS_FAILED:
            state = {
                ...state,
                data: [],
                currentId: null,
                error: action.error,
                loading: false
            };
            break;


        case actionTypes.UPDATE_DISPOSITIVO_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.UPDATE_DISPOSITIVO_SUCCEEDED:
            state = {
                ...state,
                //data: { ...action.payload },
                // data: [...updateList(state.data, action.payload)],
                data: updateList(state.data, action.payload),
                currentItem: { ...action.payload.id },
                loading: false
            };
            break;

        case actionTypes.UPDATE_DISPOSITIVO_FAILED:
            state = {
                ...state,
                error: action.error,
                loading: false
            };
            break;

        case actionTypes.CREATE_DISPOSITIVO_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.CREATE_DISPOSITIVO_SUCCEEDED:
            state = {
                ...state,
                // data: [...state.data, action.payload],
                data: updateList(state.data, action.payload),
                loading: true,
            };
            break;


        case actionTypes.CREATE_DISPOSITIVO_FAILED:
            state = {
                ...state,
                // data: null,
                error: action.error,
                loading: false
            };
            break;


        case actionTypes.DELETE_DISPOSITIVO_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        //Se recibe dispositivoID en action.payload y se elimina elimina del dispositivo del listado del state
        case actionTypes.DELETE_DISPOSITIVO_SUCCEEDED:
            state = {
                ...state,
                data: state.data.filter(item => item.id !== action.payload),
                loading: false
            };
            break;


        case actionTypes.DELETE_DISPOSITIVO_FAILED:
            state = {
                ...state,
                error: action.error,
                loading: false
            };
            break;



        case actionTypes.FETCH_DISPOSITIVO_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_DISPOSITIVO_SUCCEEDED:

            state = {
                ...state,
                //data: updateList(...state.data, ...action.payload),
                data: updateList(state.data, action.payload),
                currentId: action.payload.id,
                loading: false
            };
            break;

        case actionTypes.FETCH_DISPOSITIVO_FAILED:
            state = {
                ...state,
                data: [],
                error: action.error,
                loading: false
            };
            break;

        case actionTypes.RESET:
            state = { ...initialState };
            break;

        default:
            return state;
    }

    return state;
}
