/**
 * This is a reducer, a pure function with (state, action) => state signature.
 * It describes how an action transforms the state into the next state.
 *
 * The shape of the state is up to you: it can be a primitive, an array, an object,
 * or even an Immutable.js data structure. The only important part is that you should
 * not mutate the state object, but return a new object if the state changes.
 *
 * Here, we use a 'switch' statement and strings, but you can use a helper that
 * follows a different convention (such as function maps) if it makes sense for your
 * project.
 */

import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

//Estado inicial de la aplicación 
const initialState = {
    token: null,
    error: null,
    loading: false,
    user: null
}

//Función auxiliar del reducer que actualiza el estado de la aplicación.
//Se invoca cuando un action si el realiza un dispatch con action.type == actionTypes.AUTH_START
//Retorna un Objeto del estado de la aplicación, tras actualizarse el estado.
//Nota: Cuando loading es True => Se muestra un Spining en pantalla
const authStart = (state, action) => {
    return updateObject(state, {
        error: null,
        loading: true
    });
}

//Función auxiliar del reducer que actualiza el estado de la aplicación.
//Se invoca cuando un action si el realiza un dispatch con action.type == actionTypes.AUTH_SUCCESS
//Retorna un Objeto del estado de la aplicación, tras actualizarse el estado.
//Nota: Luego del dispatch del action "authSucess" se obtiene un nuevo token por lo que es necesario
//actualizar el token del estado de la app
const authSuccess = (state, action) => {
    return updateObject(state, {
        token: action.token,
        user: action.user,
        error: null,
        loading: false
    })
}

//Luego del dispatch del action "authFail" se obtiene un codigo de error por lo que es necesario
//actualizar el codigo de error del estado de la app
const authFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
}

//Luego del dispatch del action "logout" es necesario
//setear en null el token del estado de la app
const authLogout = (state, action) => {
    return updateObject(state, {
        token: null,
        user: null,
    });
}


//Es necesario definir cuando se ejecuta cada reducer.
//Se ejecutan los actions antes definidos dependiendo el actionType recibido.
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
        default:
            return state;
    }
}

// export const reducer;