import * as actionTypes from '../actions/actionTypes';
// import { updateList, arrayToObject } from '../utility';



/**
 * Reducer de Datos que provienen de fetch de los Ultimos Datos registrado.
 * Utilizados para plottear datos en tiempo real.
 */
const initialState = {
    data: [],     // Lista de ultimos datos registrados
    loading: false,
    error: false,
}

export const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.FETCH_DATOS_STARTED:
            state = {
                ...state,
                loading: true,
            };
            break;

        case actionTypes.FETCH_DATOS_SUCCEEDED:

            const newData = action.payload.map((item, index) => {
                return {
                    date: new Date(Date.parse(item.timestamp)),
                    Variable: item.valor
                };
            });


            state = {
                ...state,
                data: [...newData],
                loading: false
            };
            break;

        case actionTypes.FETCH_DATOS_FAILED:
            state = {
                ...state,
                data: [],
                error: action.error,
                loading: false
            };
            break;

        default:
            return state;
    }

    return state;
}                