import { combineReducers } from 'redux';

import { connectRouter } from 'connected-react-router'

import { reducer as permissions } from "react-redux-permissions"
import { reducer as authentication } from './auth';
import { reducer as dispositivos } from './dispositivos';
import { reducer as variables } from './variables';
import { reducer as datos } from './datos';
import { reducer as historicos } from './datosHistoricos';




//Combinación de reducers utilizados por la app
const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    authentication,
    permissions,
    entities: combineReducers({ dispositivos, variables, datos: combineReducers({ datos, historicos }) })
})

export default createRootReducer;