import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import createRootReducer from './reducers/rootReducers'

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

export const history = createBrowserHistory();


const composeEnhaces = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
    key: 'root',
    storage,
}

const initialState = {};

const persistedReducer = persistReducer(persistConfig, createRootReducer(history));


const store = createStore(
    persistedReducer,  // root reducer with router state    
    initialState, // Estado inicial del store
    composeEnhaces(
        applyMiddleware(
            thunk,
            routerMiddleware(history), // for dispatching history actions
            // ... other middlewares ...
        ),
    )
);


export const persistor = persistStore(store);

export default store;

