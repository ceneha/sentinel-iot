import { arrayToObject } from '../utility';


// Selector de dispositivo:
// Como en el state se guarda un listado de dispositivos en forma de array y un id de UN dispositivo
// seleccionado esta funcion selecciona un dispositivo del listado en funcion del "currentId"

//Input:  data = [{id: 1, etiqueta:"string", ... }, ] , currentId = '1'
//Output:  dispositivo = {id: 1, etiqueta:"string", ... }

export function selectCurrentDispositivo(data, currentId) {

    if (data && data.length > 0 && currentId) {
        const currentItemArray = data.filter((item) => {
            return item.id === currentId
        });

        return arrayToObject(currentItemArray);
    }

    return null;
}


/// Recibe un objeto con que tiene sub-objetos con ID como claves
// Input:  { 8: {id:8 , ...},  25: {id:25 , ...}} 
// Retorna un array de objectos con keys incrementales
// Output: [{id:8 , ...},  {id:25 , ...}]
export const objectToArray = (obj) => {
    return obj ? Object.values(obj) : obj;
    // return Object.values(obj);
}