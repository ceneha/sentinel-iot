import { arrayToObject } from '../utility';
// Selector de variable:
// Como en el state se guarda un listado de variabless en forma de array y un id de UNA variable
// seleccionado esta funcion selecciona una variable del listado en funcion del "currentId"

//Input:  data = [{id: 1, etiqueta:"string", ... }, ] , currentId = '1'
//Output:  variable = {id: 1, etiqueta:"string", ... }

export function selectCurrentVariable(data, currentId) {

    if (data && data.length > 0 && currentId) {
        const currentItemArray = data.filter((item) => {
            return item.id === currentId
        });

        return arrayToObject(currentItemArray);
    }

    return null;
}


/// Recibe un objeto con que tiene sub-objetos con ID como claves
// Input:  { 8: {id:8 , ...},  25: {id:25 , ...}} 
// Retorna un array de objectos 
// Output: [ {id:8 , ...},   {id:25 , ...}]
export const objectToArray = (obj) => {
    return obj ? Object.values(obj) : obj;
    //return Object.values(obj);
}