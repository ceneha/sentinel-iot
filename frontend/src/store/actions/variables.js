// En este  archivo se define un metodo por cada tipo de action declarado en './actionTypes.js'
import * as actionTypes from './actionTypes';
import axios from 'axios';

import { ServerDomain } from '../../routes';


// FETCH LIST OF VARIABLES
export const fetchVariablesStarted = () => ({
    type: actionTypes.FETCH_VARIABLES_STARTED
});

export const fetchVariablesSucceeded = (variables) => ({
    type: actionTypes.FETCH_VARIABLES_SUCCEEDED,
    payload: [...variables]
});

export const fetchVariablesFailed = (error) => ({
    type: actionTypes.FETCH_VARIABLES_FAILED,
    error: error
});

//  DELETE VARIABLE DE LISTADO
export const deleteVariableStarted = () => ({
    type: actionTypes.DELETE_VARIABLE_STARTED
});

export const deleteVariableSucceeded = (variableID) => ({
    type: actionTypes.DELETE_VARIABLE_SUCCEEDED,
    payload: variableID
});

export const deleteVariableFailed = (error) => ({
    type: actionTypes.DELETE_VARIABLE_FAILED,
    error: error
});



//  CREATE VARIABLE - ADD TO VARIABLE'S LIST
export const createVariableStarted = () => ({
    type: actionTypes.CREATE_VARIABLE_STARTED
});

export const createVariableSucceeded = (variable) => ({
    type: actionTypes.CREATE_VARIABLE_SUCCEEDED,
    payload: { ...variable }
});

export const createVariableFailed = (error) => ({
    type: actionTypes.CREATE_VARIABLE_FAILED,
    error: error
});

//   UPDATE VARIABLE - MODIFY VARIABLE'S LIST
export const updateVariableStarted = () => ({
    type: actionTypes.UPDATE_VARIABLE_STARTED
});

export const updateVariableSucceeded = (variable) => ({
    type: actionTypes.UPDATE_VARIABLE_SUCCEEDED,
    payload: { ...variable },
});

export const updateVariableFailed = (error) => ({
    type: actionTypes.UPDATE_VARIABLE_FAILED,
    error: error
});

// Fetch UN Variable - Detail

export const fetchVariableStarted = () => ({
    type: actionTypes.FETCH_VARIABLE_STARTED
});

export const fetchVariableSucceeded = (variable) => ({
    type: actionTypes.FETCH_VARIABLE_SUCCEEDED,
    payload: { ...variable }
});

export const fetchVariableFailed = (error) => ({
    type: actionTypes.FETCH_VARIABLE_FAILED,
    error: error
});




export const fetchVariables = (token, dispositivoID) => {

    return dispatch => {

        dispatch(fetchVariablesStarted());
        axios.defaults.headers = {
            "Authorization": `Token ${token}`,
        }

        return axios.get(`${ServerDomain}/api/dispositivos/${dispositivoID}/variables/`)
            .then(response => {
                if (response.status === 200) {
                    dispatch(fetchVariablesSucceeded(response.data.results));
                }
            })
            .catch(error => { dispatch(fetchVariablesFailed(error.message)) });
    }
}



export const fetchVariable = (token, dispositivoID, variableID) => {

    return dispatch => {
        axios.defaults.headers = {
            "Content-Type": "application/json",
            "Authorization": `Token ${token}`,
        }
        dispatch(fetchVariableStarted());

        return axios.get(`${ServerDomain}/api/dispositivos/${dispositivoID}/variables/${variableID}/`)
            .then(response => {
                dispatch(fetchVariableSucceeded(response.data))
            })
            .catch(error => {
                dispatch(fetchVariableFailed(error.message));
            });
    }
}



export const handleSubmit = (token, data) => {
    return dispatch => {

        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
            "Content-Type": "application/json",
            "Authorization": `Token ${token}`,
        }

        switch (data.requestType) {
            case 'post':
                dispatch(createVariableStarted());
                if (data.dispositivoID) {
                    return axios.post(`${ServerDomain}/api/dispositivos/${data.dispositivoID}/variables/`, data.postObj)
                        .then(response => {
                            if (response.status === 201) { //CREATED
                                dispatch(createVariableSucceeded(response.data)); //actualiza state
                            }
                        })
                        .catch(error => {
                            dispatch(createVariableFailed(error.message))
                        });
                }
                return dispatch(createVariableFailed('Error al procesar la solicitud- El dispositivo no pudo ser creado: dispositivoID inválido.'))

            case 'put':
                dispatch(updateVariableStarted());
                if (data.dispositivoID && data.variableID) {
                    return axios.put(`${ServerDomain}/api/dispositivos/${data.dispositivoID}/variables/${data.variableID}/`, data.postObj)
                        .then(response => {
                            if (response.status === 200) {// Se confirmaron los cambios en el backend
                                dispatch(updateVariableSucceeded(response.data)); // Modifico el state del frontend
                            }
                        })
                        .catch(error => {
                            dispatch(updateVariableFailed(error.message));
                        });
                }
                return dispatch(updateVariableFailed('Error al procesar la solicitud- El dispositivo no pudo ser actualizado: dispositivoID inválido.'));

            default:
                return dispatch(createVariableFailed("Error: La solicitud no se pudo procesar. Tipo: submit variable"));
        }

    }
}



//Método que se pasa como una prop al componente hijo: "Variables", para que cada "hijo" 
// autogestione su eliminación pero que a la vez permite mantener el State actualizado 
// (listado en este caso) del elemento padre "VariablesListView"
//   Secuencia:
//    0- El usuario accede a la url del listado.
//    1- Se monta el componente padre "VariablesListView" y se obtienen todos los variables de 
//      la BD mediante una llamada a la API.
//    2- Se setea en el state del componente dicho arreglo de variables. 
//    3- Se carga una tabla de componentes hijos "Variables".
//    4- El usuario Selecciona "Eliminar" en el componente hijo "Variables"
//    5- Se elimina el dispositivo seleccionado de la BD mediante una llamada a la API.
//    6- Se elimina del arreglo de variables del state, dicho dispositivo.
//    7- Se actualiza el state y Reactjs actualiza de forma automatica la tabla. 
export const handleDeleteVariable = (token, dispositivoID, variableID) => {
    return dispatch => {

        dispatch(deleteVariableStarted());
        if (dispositivoID && variableID) {
            axios.defaults.headers = {
                "Authorization": `Token ${token}`,
            }
            return axios.delete(`${ServerDomain}/api/dispositivos/${dispositivoID}/variables/${variableID}/`)
                .then(res => {
                    if (res.status === 204) {
                        dispatch(deleteVariableSucceeded(variableID));
                    }
                }).catch(error => {
                    dispatch(deleteVariableFailed(error.message));
                })
        } else {
            return dispatch(deleteVariableFailed('Error al procesar la solicitud: La variable no pudo ser eliminada: dispositivoID o variableID inválidos.'));
        }
    }
}
