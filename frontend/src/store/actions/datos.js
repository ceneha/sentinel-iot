// En este  archivo se define un metodo por cada tipo de action declarado en './actionTypes.js'
import * as actionTypes from './actionTypes';
import axios from 'axios';
import { ServerDomain } from '../../routes';

// FETCH DATOS FROM SERVER

// Call reducers to modify the app state


export const fetchDatosStarted = () => ({
    type: actionTypes.FETCH_DATOS_STARTED
});

export const fetchDatosSucceeded = (datos) => (
    {
        type: actionTypes.FETCH_DATOS_SUCCEEDED,
        payload: [...datos]
    });

export const fetchDatosFailed = (error) => ({
    type: actionTypes.FETCH_DATOS_FAILED,
    error: error
});

export const fetchDatosHistoricosStarted = () => ({
    type: actionTypes.FETCH_DATOS_HISTORICOS_STARTED
});

export const fetchDatosHistoricosSucceeded = (data) => ({
    type: actionTypes.FETCH_DATOS_HISTORICOS_SUCCEEDED,
    payload: [...data]
});

export const fetchDatosHistoricosFailed = (error) => ({
    type: actionTypes.FETCH_DATOS_HISTORICOS_FAILED,
    error: error
});

export const resetDatosHistoricos = () => ({
    type: actionTypes.RESET_DATOS_HISTORICOS
});

// export const uploadDatosHistoricos = () => ({
//     type: actionTypes.RESET_DATOS_HISTORICOS
// });


/**
 *  Fetch last page registered data: used for real time chart rendering
 */
export const fetchDatos = (token, variableID) => {
    return dispatch => {
        // const token = store.getState().authentication.token;

        dispatch(fetchDatosStarted());
        if (token) {

            axios.defaults.headers = {
                "Content-Type": "application/json",
                "Authorization": `Token ${token}`,
            }


            return axios.get(`${ServerDomain}/api/datos/`)
                .then(response => {
                    dispatch(fetchDatosSucceeded(response.data.results));
                })
                .catch(error => {
                    dispatch(fetchDatosFailed(error.message))
                    // dispatch(fetchVariableFailed(error.message));
                });
        } else {
            dispatch(fetchDatosFailed("Empty Token when trying to fetch Datos."));
        }
    }
}


/**
 *  Fetch historic registered data: used for historical's chart rendering & download  data on .csv file
 */
export const fetchDatosHistoricos = (token, parameters) => {
    return dispatch => {
        // const token = store.getState().authentication.token;

        dispatch(fetchDatosHistoricosStarted());
        if (token) {
            axios.defaults.headers = {
                "Content-Type": "application/json",
                "Authorization": `Token ${token}`,
            }

            return axios.get(`${ServerDomain}/api/datos/?variable=${parameters.variableID}&start_date=${parameters.start_date}&end_date=${parameters.end_date}`)
                .then(response => {
                    dispatch(fetchDatosHistoricosSucceeded(response.data));
                })
                .catch(error => {
                    dispatch(fetchDatosHistoricosFailed(error.message))
                });
        } else {
            dispatch(fetchDatosHistoricosFailed("Empty Token when trying to fetch Datos Historicos."));
        }
    }
}