// En este  archivo se define un metodo por cada tipo de action declarado en './actionTypes.js'
import * as actionTypes from './actionTypes';
import axios from 'axios';
// import store from '../configureStore';

import { ServerDomain } from '../../routes';

// FETCH LIST OF DISPOSITIVOS
export const fetchDispositivosStarted = () => ({
    type: actionTypes.FETCH_DISPOSITIVOS_STARTED
});

export const fetchDispositivosSucceeded = (dispositivos) => ({
    type: actionTypes.FETCH_DISPOSITIVOS_SUCCEEDED,
    payload: [...dispositivos]
});

export const fetchDispositivosFailed = (error) => ({
    type: actionTypes.FETCH_DISPOSITIVOS_FAILED,
    error: error
});

//  DELETE DISPOSITIVO DE LISTADO
export const deleteDispositivoStarted = () => ({
    type: actionTypes.DELETE_DISPOSITIVO_STARTED
});

export const deleteDispositivoSucceeded = (dispositivoID) => ({
    type: actionTypes.DELETE_DISPOSITIVO_SUCCEEDED,
    payload: dispositivoID
});

export const deleteDispositivoFailed = (error) => ({
    type: actionTypes.DELETE_DISPOSITIVO_FAILED,
    error: error
});



//  CREATE DISPOSITIVO - ADD TO DISPOSITIVO'S LIST
export const createDispositivoStarted = () => ({
    type: actionTypes.CREATE_DISPOSITIVO_STARTED
});

export const createDispositivoSucceeded = (dispositivo) => ({
    type: actionTypes.CREATE_DISPOSITIVO_SUCCEEDED,
    payload: { ...dispositivo }
});

export const createDispositivoFailed = (error) => ({
    type: actionTypes.CREATE_DISPOSITIVO_FAILED,
    error: error
});

//   UPDATE DISPOSITIVO - MODIFY DISPOSITIVO'S LIST
export const updateDispositivoStarted = () => ({
    type: actionTypes.UPDATE_DISPOSITIVO_STARTED
});

export const updateDispositivoSucceeded = (dispositivo) => ({
    type: actionTypes.UPDATE_DISPOSITIVO_SUCCEEDED,
    payload: { ...dispositivo },
});

export const updateDispositivoFailed = (error) => ({
    type: actionTypes.UPDATE_DISPOSITIVO_FAILED,
    error: error
});

// Fetch UN Dispositivo - Detail

export const fetchDispositivoStarted = () => ({
    type: actionTypes.FETCH_DISPOSITIVO_STARTED
});

export const fetchDispositivoSucceeded = (dispositivo) => ({
    type: actionTypes.FETCH_DISPOSITIVO_SUCCEEDED,
    payload: { ...dispositivo }
});

export const fetchDispositivoFailed = (error) => ({
    type: actionTypes.FETCH_DISPOSITIVO_FAILED,
    error: error
});






export const fetchDispositivos = (token) => {

    return dispatch => {

        dispatch(fetchDispositivosStarted());

        axios.defaults.headers = {
            "Content-Type": "application/json",
            "Authorization": `Token ${token}`,
        }
        return axios.get(`${ServerDomain}/api/dispositivos/`)
            .then(response => {
                dispatch(fetchDispositivosSucceeded(response.data.results));
            })
            .catch(error => {
                dispatch(fetchDispositivosFailed(error.message));
            });
    }
}


export const fetchDispositivo = (token, dispositivoID) => {

    return dispatch => {

        dispatch(fetchDispositivoStarted());

        axios.defaults.headers = {
            "Authorization": `Token ${token}`,
        }

        return axios.get(`${ServerDomain}/api/dispositivos/${dispositivoID}/`)
            .then(response => {
                dispatch(fetchDispositivoSucceeded(response.data));
            })
            .catch(error => {
                dispatch(fetchDispositivoFailed(error.message))
            });
    }
}



export const handleSubmit = (token, postObj, requestType, dispositivo) => {
    return dispatch => {

        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
            "Content-Type": "application/json",
            "Authorization": `Token ${token}`,
        }

        switch (requestType) {

            case 'post':
                dispatch(createDispositivoStarted());
                return axios.post(`${ServerDomain}/api/dispositivos/`, postObj)
                    .then(response => {
                        if (response.status === 201) { //CREATED
                            dispatch(createDispositivoSucceeded(response.data));
                        }
                    })
                    .catch(error => {
                        dispatch(createDispositivoFailed(error.message));
                    });

            case 'put':
                dispatch(updateDispositivoStarted());
                return axios.put(`${ServerDomain}/api/dispositivos/${dispositivo.id}/`, postObj)
                    .then(response => {
                        if (response.status === 200) {
                            dispatch(updateDispositivoSucceeded(response.data));
                        }
                    }).catch(error => {
                        dispatch(updateDispositivoFailed(error.message));
                    });

            default:
                return dispatch(updateDispositivoFailed("Error al procesar la solicitud - Metodo de la Request inválido"));
        }
    }
}



//Método que se pasa como una prop al componente hijo: "Dispositivos", para que cada "hijo" 
// autogestione su eliminación pero que a la vez permite mantener el State actualizado 
// (listado en este caso) del elemento padre "DispositivosListView"
//   Secuencia:
//    0- El usuario accede a la url del listado.
//    1- Se monta el componente padre "DispositivosListView" y se obtienen todos los dispositivos de 
//      la BD mediante una llamada a la API.
//    2- Se setea en el state del componente dicho arreglo de dispositivos. 
//    3- Se carga una tabla de componentes hijos "Dispositivos".
//    4- El usuario Selecciona "Eliminar" en el componente hijo "Dispositivos"
//    5- Se elimina el dispositivo seleccionado de la BD mediante una llamada a la API.
//    6- Se elimina del arreglo de dispositivos del state, dicho dispositivo.
//    7- Se actualiza el state y Reactjs actualiza de forma automatica la tabla. 
export const handleDeleteDispositivo = (token, dispositivoID) => {
    return dispatch => {

        dispatch(deleteDispositivoStarted());

        axios.defaults.headers = {
            "Authorization": `Token ${token}`,
        }

        return axios.delete(`${ServerDomain}/api/dispositivos/${dispositivoID}/`)
            .then(response => {
                if (response.status === 204) { // Si se eliminó de la BD
                    dispatch(deleteDispositivoSucceeded(dispositivoID));
                }
            })
            .catch(error => {
                dispatch(deleteDispositivoFailed(error.message));
            });
    }

}
