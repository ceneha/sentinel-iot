// En este  archivo se define un metodo por cada tipo de action declarado en './actionTypes.js'
// siempre que estén vinculados a la autenticación del usuario
import * as actionTypes from './actionTypes';
import axios from 'axios';
import { add, clear } from "react-redux-permissions"
import { push } from 'connected-react-router'

import { ServerDomain } from '../../routes';

// const handleResponesErrors = (data) => {
//     var errors = [];
//     Object.keys(data).forEach(function (key) {
//         errors.push(data[key]);
//     });

// }


export const authStart = () => {
    // Todas las actions deben retornar un tipo de action: mensaje que se recibe cuando se ejecuta authStart()
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (user, token) => {
    // Todas las actions deben retornar un tipo de action: mensaje que se recibe cuando se ejecuta el metodo
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        user: user
    }
}

export const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const authLogout = () => {
    return {
        type: actionTypes.AUTH_LOGOUT,
    }
}

export const reset = () => {
    return {
        type: actionTypes.RESET,
    }
}


export const logout = () => {
    return dispatch => {
        //Remueve las credenciales de la sesión de usuario en el navegador
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        localStorage.removeItem('expirationDate');
        //localStorage.removeItem('entities');

        // Dispatchs de los actions que invocan a los reducers que modifican el estado de la app. 
        dispatch(authLogout());
        dispatch(reset());
        dispatch(clear());
        dispatch(push('/'));
    }
}

//Ejecuta el logout despues la cantidad de tiempo seteada en el parametro expirationDate
export const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000)
    }
}

//Setea en el State el rol del usuario logueado
export const setRole = user => {
    return dispatch => {
        if (user.is_staff) {
            dispatch(add("admin"));
        } else {
            dispatch(add("visitor"));
        }
    }
}

//Para realizar el Login primero hay que iniciar la autenticación
//Luego loguear al usuario a través de la API.
export const authLogin = (username, password) => {

    return dispatch => {
        dispatch(authStart()); // Inicia la autenticación

        // console.log("x",username, password);

        axios.defaults.headers = {
            "Content-Type": "application/json",
            "Connection": "keep-alive"
        }
  console.log("server-username-pass",  ServerDomain, username, password);
        return axios.post(`${ServerDomain}/rest-auth/login/`, {
            // return axios.post(`http://localhost:8000/rest-auth/login/`, {
            username: username,
            password: password
        }).then(res => {

            console.log("ok-login", res);
            //En caso de login exitoso, la API retorna:
            //  - Key (o token) que se utiliza para hacer requests.
            //  - Fecha de expiración del token (1 hora)
            const token = res.data.key;
            const user = res.data.user;
            const expirationDate = new Date(new Date().getTime() + 3600 * 1000);

            //Se guarda el token y la fecha de expiración en el localStorage del navegador
            //Esta es la lógica que se requiere para persistir la sesión del usuario en la aplicacion.
            //No se almacenan estos datos en el State porque una vez que el usuario recargue el navegador,
            //los datos se pierden
            localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(user));
            localStorage.setItem('expirationDate', expirationDate);

            dispatch(authSuccess(user, token));
            dispatch(checkAuthTimeout(3600));
            dispatch(setRole(user));

            dispatch(push('/'));
        }).catch(err => {

            if (err.response) {

                console.log("errorr1-login:", err);
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                var errors = [];
                Object.keys(err.response.data).forEach(function (key) {
                    errors.push(err.response.data[key] + ' - ');
                });
                errors = errors.join('\n');

                dispatch(authFail(errors));
            } else {  
                console.log("errorr2-login:", err);
                dispatch(authFail("Error al intentar iniciar sesión. "));
            }
        })

    }
}


// Registro de usuarios
export const authSignup = (username, email, password1, password2) => {

    return dispatch => {
        dispatch(authStart()); // Se inicia la autenticación

        return axios.post(`${ServerDomain}/rest-auth/registration/`, {
            username: username,
            email: email,
            password1: password1,
            password2: password2
        })
            .then(res => {
                //En caso de login exitoso, la API retorna:
                //  - Key (o token) que se utiliza para hacer requests.
                //  - Fecha de expiración del token (1 hora)
                const token = res.data.key;
                const user = res.data.user;
                const expirationDate = new Date(new Date().getTime() + 3600 * 1000);

                localStorage.setItem('token', token);
                localStorage.setItem('user', JSON.stringify(user));
                localStorage.setItem('expirationDate', expirationDate);
                //Se guarda el token y la fecha de expiración en el localStorage del navegador
                //Esta es la lógica que se requiere para persistir la sesión del usuario en la aplicacion.
                //No se almacenan estos datos en el State porque una vez que el usuario recargue el navegador,
                //los datos se pierden


                dispatch(authSuccess(user, token));
                dispatch(checkAuthTimeout(3600));
                dispatch(setRole(user));

                dispatch(push('/'));
            })
            .catch(err => {
                if (err.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx

                    var errors = [];
                    Object.keys(err.response.data).forEach(function (key) {
                        errors.push(err.response.data[key]);
                    });
                    errors = errors.join('\n');
                    dispatch(authFail(errors));

                } else {
                    dispatch(authFail(err));
                }
            })
    }
}

//Cada vez que se renderiza el Layout se ejecuta una comprobación del estado de autenticación
//del usuario en la aplicación.
//      -Si el token es nulo, se dispara el action de logout.
//      -Si el token ya expiró, se dispara el action de logout.
//      -Si el token es valido y aun no expiró, se autentica nuevamente mediante el action de login
//       y se actualiza el tiempo que falte para ejecutar el logout automático (checkAuthTimeout).

//Nota 1: El estado (o state) global de autenticación, contenidas en el store, se modifica
//        mediante actions que a su vez disparan reducers que son los que realmente
//        efectúan los cambios de estado del store.

//Nota 2: De esta forma el token persiste en el State de la aplicación independientemente
//de que se actualice la app desde el navegador (con F5) y por consiguiente todos los componentes.
export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        const user = JSON.parse(localStorage.getItem('user'))
        if (token === undefined || user === undefined) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                dispatch(authSuccess(user, token));
                dispatch(setRole(user));

                // Dispara el logout automático si la app ha estado inactiva mas de 1 hora, 
                //de caso contrario actualiza la hora de expiracion
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
}