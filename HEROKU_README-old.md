# Despliegue del sistema

El sistema se encuentra desplegado sobre la plataforma de Herokku, el cual tiene su propio repositorio de git oculto.
La forma rápida y momentánea para poder desplegar consiste en:

1. Abrir el siguiente archivo `/src/routes.js` y descomentar la linea de ServerDomain según el host que se vaya a utilizar:

Para desarrollo: Local host -> `http://localhost:300`
Para producción: Heroku host-> `https://iot-sentinel.herokuapp.com`

ToDo: Investigar como configurar ésto para poder switchear el host automáticamente segun se este trabajando en development o production.

2. Posicionarse sobre la rama master.

`git checkout master`

3. Ingresar a Heroku desde el navegador, ir a Settings y copiar la API Key.

4. Reemplazar HEROKU_API_KEY con la Api Key copiada y ejecutar en consola:

`git push https://heroku:$HEROKU_API_KEY@git.heroku.com/iot-sentinel.git $(LOCAL_BRANCH_TO_PUSH)`


Trabajos futuros, implementar integración continua: Permitirá desplegar el sitema con un único push a la rama master del repo de GitLab, a través de la creación de un pipeline directo con el repo de heroku. 

Ver:
- https://blog.thecodewhisperer.com/permalink/deploying-jekyll-to-heroku-using-gitlab-ci

# Heroku

Para poder acceder a una terminal de heroku desde nuestra pc (previa instalación y configuración de heroku):

1. Abrir una terminal y ejecutar:

    `heroku login`

2. Ejecutar:

    `heroku run bash --app iot-sentinel`